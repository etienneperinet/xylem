﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Xylem
{
    public class FormulaController
    {

        /// <summary>
        /// Calculates the Pearson product moment correlation coefficient (R^2)
        /// </summary>
        /// <param name="prUnits">The average pressure units of the microcontroller by height</param>
        /// <param name="bar">The average pressure in bar by height</param>
        /// <returns>The square of the Pearson product moment correlation coefficient</returns>
        public static double GetSquaredPearson(List<double> prUnits, List<double> bar)
        {
            double pearson = MathNet.Numerics.Statistics.Correlation.Pearson(prUnits, bar);
            return Math.Pow(pearson, 2);
        }

        /// <summary>
        /// Calculates the slope of the linear regression
        /// </summary>
        /// <param name="prUnits">The average pressure units of the microcontroller by height</param>
        /// <param name="bar">The average pressure in bar by height</param>
        /// <returns>The slope of the linear regression</returns>
        public static double GetSlope(List<double> prUnits, List<double> bar)
        {
            Tuple <double, double> fit = MathNet.Numerics.LinearRegression.SimpleRegression.Fit(prUnits.ToArray(), bar.ToArray());
            double slope = fit.Item2;
            return slope;
        }

        /// <summary>
        /// Calculates the intercept of the linear regression
        /// </summary>
        /// <param name="prUnits">The average pressure units of the microcontroller by height</param>
        /// <param name="bar">The average pressure in bar by height</param>
        /// <returns>The intercept of the linear regression</returns>
        public static double GetIntercept(List<double> prUnits, List<double> bar)
        {
            Tuple <double, double> fit = MathNet.Numerics.LinearRegression.SimpleRegression.Fit(prUnits.ToArray(), bar.ToArray());
            double intercept = fit.Item1;
            return intercept;
        }

        public static List<int> GetSensor1PressureAveragePerStep(List<ArduinoData> arduinoData)
        {
            List<int> pressureAveragePerStep = new List<int>();
            for (int step = 1; step <= 10; step++)
            {
                IEnumerable<ArduinoData> stepArduinoData = arduinoData.Where(ad => ad.Step == step);
                if (stepArduinoData != null && stepArduinoData.Any())
                {
                    int stepPressureAverage = Convert.ToInt32(stepArduinoData.Average(ad => ad.Sensor1.Pressure));
                    pressureAveragePerStep.Add(stepPressureAverage);
                }
            }
            return pressureAveragePerStep;
        }

        public static List<int> GetSensor2PressureAveragePerStep(List<ArduinoData> arduinoData)
        {
            List<int> pressureAveragePerStep = new List<int>();
            for (int step = 1; step <= 10; step++)
            {
                IEnumerable<ArduinoData> stepArduinoData = arduinoData.Where(ad => ad.Step == step);
                if (stepArduinoData != null && stepArduinoData.Any())
                {
                    int stepPressureAverage = Convert.ToInt32(stepArduinoData.Average(ad => ad.Sensor2.Pressure));
                    pressureAveragePerStep.Add(stepPressureAverage);
                }
            }
            return pressureAveragePerStep;
        }

        public static List<int> GetHeights(List<ArduinoData> arduinoData, int heightIncrementation)
        {
            List<int> steps = arduinoData.GroupBy(b => b.Step).Select(b => b.First()).OrderBy(b => b.Step).Select(b => b.Step).ToList();
            List<int> heights = new List<int>();
            for (int index = 0; index < steps.Count(); index++)
            {
                heights.Add(steps[index] * heightIncrementation);
            }
            return heights;
        }

        public static List<double> GetBars(List<int> heights)
        {
            List<double> bars = new List<double>();
            for (int index = 0; index < heights.Count(); index++)
            {
                bars.Add(Convert.ToDouble(heights[index]) / 1000);
            }
            return bars;
        }

        public static List<ArduinoData> GetStepArduinoData(List<ArduinoData> arduinoData, int step)
		{
            return arduinoData.Where(ad => ad.Step == step).ToList();
		}

        /// <summary>
        /// (D6)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <param name="minBar"></param>
        /// <param name="maxBar"></param>
        /// <returns></returns>
        public static bool OptimalPeekTubeChoice(List<ArduinoData> arduinoData, double slopeSensor1, double interceptSensor1, double slopeSensor2, double interceptSensor2, double minBar, double maxBar)
		{
            bool optimal = false;
            arduinoData = GetStepArduinoData(arduinoData, 2);
            arduinoData = GetArduinoDataLast300Seconds(arduinoData);
            double averageBar1 = GetSensor1AverageArduinoDataBar(arduinoData, slopeSensor1, interceptSensor1);
            double averageBar2 = GetSensor2AverageArduinoDataBar(arduinoData, slopeSensor2, interceptSensor2);
            double averageBarRatio = averageBar2 / averageBar1;

            if (averageBarRatio > minBar && averageBarRatio < maxBar)
			{
                optimal = true;
			}

            return optimal;
		}

        /// <summary>
        /// Indicates if the sensors are calibrated (C7)
        /// </summary>
        /// <param name="arduinoData">The ArduinoData of the first step</param>
        /// <param name="slope">The result slope of the calibration</param>
        /// <param name="intercept">The result intercept of the calibration</param>
        /// <returns></returns>
        public static bool CalibratedPressureSensor(List<ArduinoData> arduinoData, double slopeSensor1, double interceptSensor1, double slopeSensor2, double interceptSensor2)
		{
            arduinoData = GetStepArduinoData(arduinoData, 1);
            double sensor1BarAverage = GetSensor1AverageArduinoDataBar(arduinoData, slopeSensor1, interceptSensor1);
            double sensor2BarAverage = GetSensor2AverageArduinoDataBar(arduinoData, slopeSensor2, interceptSensor2);
            bool calibrated = false;

            if (Math.Abs(sensor1BarAverage - sensor2BarAverage) < 0.001)
			{
                calibrated = true;
			}

            return calibrated;
        }

        /// <summary>
        /// (Q8)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static double GetSensor1AverageArduinoDataBar(List<ArduinoData> arduinoData, double slope, double intercept)
		{
            double average = -1;
            if (arduinoData.Any())
			{
                average = arduinoData.Average(ad => GetArduinoDataBar(ad.Sensor1.Pressure, slope, intercept));

            }
            return average;
        }

        /// <summary>
        /// (R8)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static double GetSensor2AverageArduinoDataBar(List<ArduinoData> arduinoData, double slope, double intercept)
        {
            double average = -1;
            if (arduinoData.Any())
            {
                average = arduinoData.Average(ad => GetArduinoDataBar(ad.Sensor2.Pressure, slope, intercept));

            }
            return average;
        }

        /// <summary>
        /// (AG8)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static double GetSensor2MinimumArduinoDataBar(List<ArduinoData> arduinoData, double slope, double intercept)
        {
            double min = -1;
            if (arduinoData.Any())
			{
                min = arduinoData.Min(ad => GetArduinoDataBar(ad.Sensor2.Pressure, slope, intercept));

            }
            return min;
        }

        /// <summary>
        /// (Q12-Q21)
        /// </summary>
        /// <param name="pressure"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static double GetArduinoDataBar(int pressure, double slope, double intercept)
		{
            return (pressure * slope) + intercept;
		}

        /// <summary>
        /// Checks if it has been recording for a long enough period of time (D8)
        /// </summary>
        /// <param name="arduinoData">The ArduinoData of the second step</param>
        /// <returns></returns>
        public static bool SufficientTime(List<ArduinoData> arduinoData)
		{
            bool sufficientTime = false;

            arduinoData = GetStepArduinoData(arduinoData, 2);
            List<int> timeToMax = GetArduinoDataLast300Seconds(arduinoData).Select(ad => ad.Time).ToList();
            int maxTimeToMax = -1;

            if (timeToMax.Any())
			{
                maxTimeToMax = timeToMax.Max();
            }

            if (maxTimeToMax >= 294)
			{
                sufficientTime = true;
			}

            return sufficientTime;
        }

        public static List<ArduinoData> GetArduinoDataLast300Seconds(List<ArduinoData> arduinoData)
		{
            int maxTime = GetMaximumTime(arduinoData);
            arduinoData = arduinoData.Where(ad => (maxTime - ad.Time) < 300).ToList();
            return arduinoData;
        }

        /// <summary>
        /// Returns the minimum time in the ArduinoData list (Y6)
        /// </summary>
        /// <param name="arduinoData">The ArduinoData of the second step</param>
        /// <returns></returns>
        public static int GetMinimumTime(List<ArduinoData> arduinoData)
		{
            return arduinoData.Min(ad => ad.Time);
		}

        /// <summary>
        /// Returns the maximum time in the ArduinoData list (Y7)
        /// </summary>
        /// <param name="arduinoData">The ArduinoData of the second step</param>
        /// <returns></returns>
        public static int GetMaximumTime(List<ArduinoData> arduinoData)
		{
            int max = -1;
            if (arduinoData.Any())
			{
                max = arduinoData.Max(ad => ad.Time);
            }
            return max;
		}

        /// <summary>
        /// (D9)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="peekTubeResistance"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static bool ValidStability(List<ArduinoData> arduinoData, double peekTubeResistance, double slope, double intercept)
		{
            arduinoData = GetStepArduinoData(arduinoData, 2);
            List<double> k300Rough = GetKRoughLast300Seconds(arduinoData, peekTubeResistance, slope, intercept);
            double averageK300Rough = -1;
            if (k300Rough.Any())
			{
                averageK300Rough = k300Rough.Average();
            } 
            double standardDeviationK300Rough = MathNet.Numerics.Statistics.Statistics.StandardDeviation(k300Rough);
            bool stable = false;
            
            if (standardDeviationK300Rough / averageK300Rough < 0.05)
			{
                stable = true;
			}

            return stable;
		}

        /// <summary>
        /// (D24)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static double GetAverageStep2Sensor1BarLast300Seconds(List<ArduinoData> arduinoData, double slope, double intercept)
        {
            arduinoData = GetStepArduinoData(arduinoData, 2);
            arduinoData = GetArduinoDataLast300Seconds(arduinoData);
            return GetSensor1AverageArduinoDataBar(arduinoData, slope, intercept);
        }

        /// <summary>
        /// (D25)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static double GetAverageStep2Sensor2BarLast300Seconds(List<ArduinoData> arduinoData, double slope, double intercept)
        {
            arduinoData = GetStepArduinoData(arduinoData, 2);
            arduinoData = GetArduinoDataLast300Seconds(arduinoData);
            return GetSensor2AverageArduinoDataBar(arduinoData, slope, intercept);
        }

        /// <summary>
        /// (C26)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="peekTubeResistance"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static double GetMmolKRoughLast300Seconds(List<ArduinoData> arduinoData, double peekTubeResistance, double slopeSensor1, double interceptSensor1, double slopeSensor2, double interceptSensor2)
		{
            arduinoData = GetStepArduinoData(arduinoData, 2);
            arduinoData = GetArduinoDataLast300Seconds(arduinoData);
            double averageBar1 = GetSensor1AverageArduinoDataBar(arduinoData, slopeSensor1, interceptSensor1);
            double averageBar2 = GetSensor2AverageArduinoDataBar(arduinoData, slopeSensor2, interceptSensor2);
            return GetKRough(averageBar1, averageBar2, peekTubeResistance);
		}

        /// <summary>
        /// (D27)
        /// </summary>
        /// <param name="mmolKRough"></param>
        /// <returns></returns>
        public static double GetKGKRough(double mmolKRough)
		{
            return mmolKRough * 18 / 1000000;
		}

        /// <summary>
        /// (AE66-AE120)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="peekTubeResistance"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static List<double> GetKRoughLast300Seconds(List<ArduinoData> arduinoData, double peekTubeResistance, double slope, double intercept)
		{
            arduinoData = GetArduinoDataLast300Seconds(arduinoData);
		    List<double> k300Rough = arduinoData.Select(ad => GetKRough(GetArduinoDataBar(ad.Sensor1.Pressure, slope, intercept), GetArduinoDataBar(ad.Sensor2.Pressure, slope, intercept), peekTubeResistance)).ToList();
            return k300Rough;
        }

        public static double GetKRough(double bar1, double bar2, double peekTubeResistance)
		{
            return ((bar1 - bar2) / peekTubeResistance) / bar2;
		}

        /// <summary>
        /// (D28)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="peekTubeResistance"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static double GetKCoefficientCorrelation(List<ArduinoData> arduinoData, double peekTubeResistance, double slopeSensor1, double interceptSensor1, double slopeSensor2, double interceptSensor2)
        {
            arduinoData = GetStepArduinoData(arduinoData, 2);
            arduinoData = GetArduinoDataLast300Seconds(arduinoData);
            List<double> kRough = arduinoData.Select(ad => GetKRough(GetArduinoDataBar(ad.Sensor1.Pressure, slopeSensor1, interceptSensor1), GetArduinoDataBar(ad.Sensor2.Pressure, slopeSensor2, interceptSensor2), peekTubeResistance)).ToList();
            double averageKRough = -1;
            if (kRough.Any())
			{
                averageKRough = kRough.Average();
            }
            double standardDeviationKRough = MathNet.Numerics.Statistics.Statistics.StandardDeviation(kRough);

            return standardDeviationKRough / averageKRough;
        }

        /// <summary>
        /// (D30)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="slope"></param>
        /// <param name="intercept"></param>
        /// <returns></returns>
        public static double GetMinimumStep3Sensor2Bar(List<ArduinoData> arduinoData, double slope, double intercept)
		{
            arduinoData = GetStepArduinoData(arduinoData, 3);
            return GetSensor2MinimumArduinoDataBar(arduinoData, slope, intercept);
		}

        /// <summary>
        /// (D31)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <returns></returns>
        public static double GetAverageStep3Sensor1Temperature(List<ArduinoData> arduinoData)
        {
            arduinoData = GetStepArduinoData(arduinoData, 3);
            double average = -1;
            if (arduinoData.Any())
			{
                average = arduinoData.Average(ad => ad.Sensor1.Temperature);
            }
            return average;
        }

        /// <summary>
        /// (D32)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <returns></returns>
        public static double GetAverageSensor2Step3Temperature(List<ArduinoData> arduinoData)
        {
            arduinoData = GetStepArduinoData(arduinoData, 3);
            double average = -1;
            if (arduinoData.Any())
			{
                average = arduinoData.Average(ad => ad.Sensor2.Temperature);
            }
            return average;
        }

        /// <summary>
        /// (D33)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <returns></returns>
        public static double GetAverageStep3Temperature(List<ArduinoData> arduinoData)
		{
            return (GetAverageStep3Sensor1Temperature(arduinoData) + GetAverageSensor2Step3Temperature(arduinoData)) / 2;
		}

        /// <summary>
        /// (D34)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="peekTubeResistance"></param>
        /// <returns></returns>
        public static double GetPeektubeResistanceActualTemperature(List<ArduinoData> arduinoData, double peekTubeResistance)
		{
            double averageTemperature = GetAverageStep3Temperature(arduinoData);
            return Math.Pow(10, (1.3272 * (20 - averageTemperature) - 0.001053 * Math.Pow((20 - averageTemperature), 2)) / (averageTemperature + 105)) * (1.002 / 0.8904) * peekTubeResistance;
		}

        /// <summary>
        /// (D36)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <returns></returns>
        public static double GetAverageEndTemperatureStep3(List<ArduinoData> arduinoData)
		{
            arduinoData = GetStepArduinoData(arduinoData, 3);
            double average = -1;
            if (arduinoData.Any())
			{
                average = arduinoData.Average(ad => ad.EndTemperature);
            }
            return average;
		}

        /// <summary>
        /// (D38)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="peekTubeResistance"></param>
        /// <param name="slopeSensor1"></param>
        /// <param name="interceptSensor1"></param>
        /// <returns></returns>
        public static double GetMeasurementTemperatureCorrectedP3K(List<ArduinoData> arduinoData, double peekTubeResistance, double slopeSensor1, double interceptSensor1, double slopeSensor2, double interceptSensor2)
		{
            double averageSensor1Bar = GetAverageStep2Sensor1BarLast300Seconds(arduinoData, slopeSensor1, interceptSensor1);
            double averageSensor2Bar = GetAverageStep2Sensor2BarLast300Seconds(arduinoData, slopeSensor2, interceptSensor2);
            double averageSensor3Bar = GetMinimumStep3Sensor2Bar(arduinoData, slopeSensor2, interceptSensor2);
            double actualTemperatureResistance = GetPeektubeResistanceActualTemperature(arduinoData, peekTubeResistance);
            return (averageSensor1Bar - averageSensor2Bar) / actualTemperatureResistance / (averageSensor2Bar - averageSensor3Bar) * 18 / 1000000;
        }

        /// <summary>
        /// (D39)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="peekTubeResistance"></param>
        /// <param name="slopeSensor1"></param>
        /// <param name="interceptSensor1"></param>
        /// <returns></returns>
        public static double Get25oCCorrectedP3K(List<ArduinoData> arduinoData, double peekTubeResistance, double slopeSensor1, double interceptSensor1, double slopeSensor2, double interceptSensor2)
        {
            double averageEndTemperature = GetAverageEndTemperatureStep3(arduinoData);
            double temperatureCorrected = GetMeasurementTemperatureCorrectedP3K(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2);
            return temperatureCorrected / (0.88862 * (1 / Math.Pow(10, (1.3272 * (20 - averageEndTemperature) - 0.001053 * Math.Pow((averageEndTemperature - 20), 2)) / (averageEndTemperature + 105))));
        }

        /// <summary>
        /// (D43)
        /// </summary>
        /// <param name="stemDiameter"></param>
        /// <param name="pithDiameter"></param>
        /// <returns></returns>
        public static double GetStemCrossSectionalArea(double stemDiameter, double pithDiameter)
		{
            return Math.Pow(((stemDiameter / 1000) / 2), 2) * Math.PI - Math.Pow(((pithDiameter / 1000) / 2), 2) * Math.PI;
		}

        /// <summary>
        /// (D47)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="peekTubeResistance"></param>
        /// <param name="slopeSensor1"></param>
        /// <param name="interceptSensor1"></param>
        /// <param name="stemLength"></param>
        /// <returns></returns>

        public static double GetConductivity(List<ArduinoData> arduinoData, double peekTubeResistance, double slopeSensor1, double interceptSensor1, double slopeSensor2, double interceptSensor2, double stemLength)
		{
            double correctedP3K = Get25oCCorrectedP3K(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2);
            return correctedP3K * (stemLength / 1000);
        }

        /// <summary>
        /// (D48)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="peekTubeResistance"></param>
        /// <param name="slopeSensor1"></param>
        /// <param name="interceptSensor1"></param>
        /// <param name="stemLength"></param>
        /// <param name="stemDiameter"></param>
        /// <param name="pithDiameter"></param>
        /// <returns></returns>
        public static double GetStemAreaSpecificConductivity(List<ArduinoData> arduinoData, double peekTubeResistance, double slopeSensor1, double interceptSensor1, double slopeSensor2, double interceptSensor2, double stemLength, double stemDiameter, double pithDiameter)
		{
            double conductivity = GetConductivity(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2, stemLength);
            double stemCrossSectional = GetStemCrossSectionalArea(stemDiameter, pithDiameter);
            return conductivity / stemCrossSectional;
		}

        /// <summary>
        /// (D49)
        /// </summary>
        /// <param name="stemDiameter"></param>
        /// <param name="leafArea"></param>
        /// <returns></returns>
        public static double GetHuberValue(double stemDiameter, double leafArea)
		{
            return (Math.Pow(((stemDiameter / 1000) / 2), 2) * Math.PI) / (leafArea / 10000);
		}

        /// <summary>
        /// (D50)
        /// </summary>
        /// <param name="arduinoData"></param>
        /// <param name="peekTubeResistance"></param>
        /// <param name="slopeSensor1"></param>
        /// <param name="interceptSensor1"></param>
        /// <param name="stemLength"></param>
        /// <param name="leafArea"></param>
        /// <returns></returns>
        public static double GetLeafAreaSpecificConductivity(List<ArduinoData> arduinoData, double peekTubeResistance, double slopeSensor1, double interceptSensor1, double slopeSensor2, double interceptSensor2, double stemLength, double leafArea)
        {
            double conductivity = GetConductivity(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2, stemLength);
            return conductivity / (leafArea / 10000);
        }

        public static List<double> GetSensor1BarChart(List<ArduinoData> arduinoData, double slope, double intercept)
		{
            return arduinoData.Select(ad => ad.Step == 2 ? GetArduinoDataBar(ad.Sensor1.Pressure, slope, intercept) : 0).ToList();
		}

        public static List<double> GetSensor2BarChart(List<ArduinoData> arduinoData, double slope, double intercept)
		{
            return arduinoData.Select(ad => ad.Step == 2 ? GetArduinoDataBar(ad.Sensor2.Pressure, slope, intercept) : 0).ToList();
		}

        public static List<int> GetArduinoDataTime(List<ArduinoData> arduinoData)
		{
            return arduinoData.Select(ad => ad.Time).ToList();
		}

    }
}