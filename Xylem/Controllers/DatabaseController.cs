﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;



namespace Xylem
{
    class DatabaseController
    {
        const string databaseFileName = "XylemData.db";
        SQLiteConnection connection;

        #region DatabaseCreation

        public DatabaseController()
        {
            CreateConnection();
            CreateDatabase();
        }

        public DatabaseController(string path)
        {
            CreateDatabase();
            connection = new SQLiteConnection(path);
        }

        private void CreateConnection()
        {
            string connectionString = "URI=file:" + GetPathToProgram() + databaseFileName;

            connection = new SQLiteConnection(connectionString);
        }

        public void CreateSessionTest()
        {
            Computer computerTest = new Computer(2, 1);
            Operator operatorTest = new Operator("Étienne", "Périnet", "peret3");
            int arduinoTest = 3;
            if (!ArduinoExists(arduinoTest))
            {
                computerTest.ID = AddComputer(computerTest);
                AddOperator(operatorTest);
                AddArduino(arduinoTest);
            }
            Session.SetInstance(arduinoTest, computerTest, operatorTest);
        }

        public SessionRecord CreateSessionRecordTest()
        {
            CreateSessionTest();
            SessionRecord sessionRecordTest = new SessionRecord(Session.GetInstance());
            sessionRecordTest.ID = AddSessionRecord(sessionRecordTest);
            return sessionRecordTest;
        }

        public SessionRecord CreateSessionRecordTest(Session sessionTest)
        {
            SessionRecord sessionRecordTest = new SessionRecord(sessionTest);
            sessionRecordTest.ID = AddSessionRecord(sessionRecordTest);
            return sessionRecordTest;
        }

        private void CreateDatabase()
        {
            if (!File.Exists(GetPathToProgram() + databaseFileName))
            {
                string strCommand = File.ReadAllText(GetPathToProgram() + "XylemScript.sql"); // .sql file path
                using (SQLiteCommand objCommand = connection.CreateCommand())
                {
                    connection.Open();
                    objCommand.CommandText = strCommand;
                    objCommand.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        #endregion

        public string GetPathToProgram()
        {
            Process currentProcess = Process.GetCurrentProcess();
            ProcessModule myProcessModule = currentProcess.MainModule;

            return Path.GetDirectoryName(myProcessModule.FileName).Replace("Xylem.exe", "") + "\\";
        }

        #region Add

        public void AddOperator(Operator @operator)
        {
            if (!OperatorExists(@operator.EmployeeCode))
            {
                string sqlCommand = "INSERT INTO operator " +
                    "(employee_code, first_name, last_name)" +
                    "VALUES ('" +
                    @operator.EmployeeCode + "', '" +
                    @operator.FirstName + "', '" +
                    @operator.LastName + "')";

                ExecuteNonQuerry(sqlCommand);
            }
        }
        
        public void AddCalibrationResult(CalibrationResult calibrationResult)
        {
            string sqlCommand = "INSERT INTO calibration_result " +
                "(slope1, slope2, intercept1, intercept2, pearson1, pearson2, session_record_id, note)" +
                "VALUES ('" +
                calibrationResult.Slope1 + "', '" +
                calibrationResult.Slope2 + "', '" +
                calibrationResult.Intercept1 + "', '" +
                calibrationResult.Intercept2 + "', '" +
                calibrationResult.Pearson1 + "', '" +
                calibrationResult.Pearson2 + "', '" +
                calibrationResult.SessionRecord.ID + "', '" +
                calibrationResult.Note + "')";

            ExecuteNonQuerry(sqlCommand);
        }

        public void AddPeekTube(PeekTube peekTube)
        {
            if (!PeekTubeExists(peekTube.Code))
            {
                string sqlCommand = "INSERT INTO peektube " +
                    "(code, color, r_peektube_25, year, month, day, actif, employee_code)" +
                    "VALUES ('" +
                    peekTube.Code + "', '" +
                    peekTube.Color + "', '" +
                    peekTube.R_PeekTube_25 + "', '" +
                    peekTube.Year + "', '" +
                    peekTube.Month + "', '" +
                    peekTube.Day + "', '" +
                    Convert.ToInt32(peekTube.Actif) + "', '" +
                    peekTube.OperatorCreatorCode + "')";

                ExecuteNonQuerry(sqlCommand);
            }
        }

        public int AddKI_KMAX(KI_KMAX ki_kmax)
        {
            string sqlCommand = "INSERT INTO ki_kmax " +
                "(kmax_id, measurement_result_id, sample_id)" +
                "VALUES ('"+
                ki_kmax.KMax.ID + "', '" +
                ki_kmax.MeasurementResult.ID + "', '" +
                ki_kmax.Sample.ID + "')";

            return ExecuteInsertInto(sqlCommand);
        }

        public int AddKMAXMethod(KMAXMethod method)
        {
            string sqlCommand = "INSERT INTO kmax_method " +
                "(name)" +
                "VALUES ('"+
                method.Name + "')";

            return ExecuteInsertInto(sqlCommand);
        }
        

        public int AddKMAX(KMAX kmax)
        {
            string sqlCommand = "INSERT INTO kmax " +
                "(kmax_duration_min, kmax_iteration, kmax_pression, kmax_method_id)" +
                "VALUES ('"+
                kmax.KMaxDurationMin + "', '" +
                kmax.KMaxIteration + "', '" +
                kmax.KMaxPression + "', '" +
                kmax.KMAXMethode.ID + "')";

            return ExecuteInsertInto(sqlCommand);
        }

        public int AddOrgan(Organ organ)
        {
            string sqlCommand = "INSERT INTO organ " +
                "(name)" +
                "VALUES ('" + organ.Name + "')";

            return ExecuteInsertInto(sqlCommand);
        }
        
        public int AddStress(Stress stress)
        {
            string sqlCommand = "INSERT INTO stress " +
                "(name)" +
                "VALUES ('" + stress.Name + "')";

            return ExecuteInsertInto(sqlCommand);
        }
        
        public int AddIndividual(Individual individual)
        {
            int individualCode = -1;

            if (!IndividualExists(individual.Code))
            {
                string sqlCommand = "INSERT INTO individual " +
                    "(code, experience_id)" +
                    "VALUES ('" +
                    individual.Code + "', '" +
                    individual.Experience.ID + "')";

                individualCode = ExecuteInsertInto(sqlCommand);
            }

            return individualCode;
        }
        
        public int AddExperience(Experience experience)
        {
            string sqlCommand = "INSERT INTO experience " +
                "(name)" +
                "VALUES ('" + experience.Name + "')";

            return ExecuteInsertInto(sqlCommand);
        }
        
        public int AddPerfusionSolution(PerfusionSolution perfusionSolution)
        {
            string sqlCommand = "INSERT INTO perfusion_solution " +
                "(name)" +
                "VALUES ('" + perfusionSolution.Name + "')";

            return ExecuteInsertInto(sqlCommand);
        }
        
        public int AddMeasurementResult(MeasurementResult measurementResult)
        {
            string sqlCommand = "INSERT INTO measurement_result " +
                "(water_height, perfusion_solution_id, valid_peektube, sufficient_time, stable, valid_sensor_pressure" +
                ", p1, p2, mmol_k_rough, kg_k_rough, cv_k, p3, t1, t_av, r_peektube_t, t3, k_t, k_25" +
                ", kh, ks, hv, kl, gamme_peektube, diameter1, diameter2, length," +
                " peektube_code, experience_id, session_record_id, note)" +
                "VALUES ('" +
                measurementResult.WaterHeight + "', '" +
                measurementResult.SolutionPerfusion.ID + "', '" +
                Convert.ToInt32(measurementResult.ValidPeekTube) + "', '" +
                Convert.ToInt32(measurementResult.SufficientTime) + "', '" +
                Convert.ToInt32(measurementResult.Stable) + "', '" +
                Convert.ToInt32(measurementResult.ValidSensorPressure) + "', '" +
                measurementResult.P1 + "', '" +
                measurementResult.P2 + "', '" +
                measurementResult.Mmol_k_rough + "', '" +
                measurementResult.Kg_k_rough + "', '" +
                measurementResult.CV_K + "', '" +
                measurementResult.P3 + "', '" +
                measurementResult.T1 + "', '" +
                measurementResult.T_AV + "', '" +
                measurementResult.R_peektube_t + "', '" +
                measurementResult.T3 + "', '" +
                measurementResult.K_T + "', '" +
                measurementResult.K_25 + "', '" +
                measurementResult.KH + "', '" +
                measurementResult.KS + "', '" +
                measurementResult.HV + "', '" +
                measurementResult.KL + "', '" +
                measurementResult.GammePeekTube + "', '" +
                measurementResult.PeekTube.Code + "', '" +
                measurementResult.SessionRecord.ID + "', '" +
                measurementResult.Diameter1 + "', '" +
                measurementResult.Diameter2 + "', '" +
                measurementResult.Lenght + "', '" +
                measurementResult.Note + "')";

            return ExecuteInsertInto(sqlCommand);
        }

        private Sample GetLastSample()
        {
            string sqlCommand = "SELECT * FROM sample ORDER BY id DESC LIMIT 1 ";
            Sample objectToReturn = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectToReturn = GetSampleFromReader(reader);
                    }
                }
            }
            if (objectToReturn == null)
            {
                objectToReturn = new Sample();
                objectToReturn.ID = 0;
            }
            
            return objectToReturn;
        }


        public int AddSample(Sample sample)
        {
            //sample.ID = GetLastSample().ID + 1;
            string sqlCommand = "INSERT INTO sample " +
                "(code, stage, physiological_age, water_potential," +
                " organ_id, stress_id, individual_code)" +
                "VALUES ('" +
                sample.Code + "', '" +
                sample.Stage + "', '" +
                sample.PhysiologicalAge + "', '" +
                sample.WaterPotential + "', '" +
                sample.Organ.ID + "', '" +
                sample.Stress.ID + "', '" +
                sample.Individual.Code + "')";


            return ExecuteInsertInto(sqlCommand);
        }

        public long AddArduinoData(ArduinoData arduinoData)
        {

            arduinoData.Sensor1.ID = AddSensor(arduinoData.Sensor1);
            arduinoData.Sensor2.ID = AddSensor(arduinoData.Sensor2);

            string sqlCommand = "INSERT INTO arduino_data " +
                "(time, step, end_temperature, session_id, sensor1_id, sensor2_id)" +
                "VALUES ('" +
                arduinoData.Time + "', '" +
                arduinoData.Step + "', '" +
                arduinoData.EndTemperature + "', '" +
                arduinoData.CurrentSessionRecord.ID + "', '" +
                arduinoData.Sensor1.ID + "', '" +
                arduinoData.Sensor2.ID + "')";

            return ExecuteInsertIntoReturnLong(sqlCommand);
        }

        private int AddSensor(Sensor sensor)
        {
            string sqlCommand = "INSERT INTO sensor " +
                "(pressure, temperature)" +
                "VALUES ('" +
                sensor.Pressure + "', '" +
                sensor.Temperature + "')";

            return ExecuteInsertInto(sqlCommand);
        }

        public int AddSession(Session session)
        {
            string sqlCommand = "INSERT INTO session " +
                "(computer_id, arduino_id, employee_code, year, month, day)" +
                "VALUES ('" +
                session.CurrentComputer.ID + "', '" +
                session.ArduinoNumber + "', '" +
                session.CurrentOperator.EmployeeCode + "', '" +
                session.Year + "', '" +
                session.Month + "', '" +
                session.Day + "')";

            return ExecuteInsertInto(sqlCommand);
        }

        public int AddSessionRecord(SessionRecord sessionRecord)
        {
            string sqlCommand = "INSERT INTO session_record " +
                "(hour, minute, session_id)" +
                "VALUES ('" +
                sessionRecord.Hour + "', '" +
                sessionRecord.Minute + "', '" +
                sessionRecord.Session.ID + "')";

            return ExecuteInsertInto(sqlCommand);
        }

        public int AddArduino(int arduinoNumber)
        {
            string sqlCommand = "INSERT INTO arduino " +
                "(id)" +
                "VALUES ('" + arduinoNumber + "')";

            return ExecuteInsertInto(sqlCommand);
        }

        public int AddComputer(Computer computer)
        {
            int computerID = -1;

            if (!ComputerExists(computer.ID))
            {
                string sqlCommand = "INSERT INTO computer " +
                    "(id, computer_type_id)" +
                    "VALUES ('" +
                    computer.ID + "', '" +
                    computer.GetStandardizedID() + "')";

                computerID = ExecuteInsertInto(sqlCommand);
            }

            return computerID;
        }

        private int ExecuteInsertInto(string sqlCommand)
        {
            int id = -1;
            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    cmd.ExecuteNonQuery();
                    id = (int) c.LastInsertRowId;
                }
            }
            return id;
        }

        private long ExecuteInsertIntoReturnLong(string sqlCommand)
        {
            long id = -1;
            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    cmd.ExecuteNonQuery();
                    id = (long) c.LastInsertRowId;
                }
            }
            return id;
        }
        
        private void ExecuteNonQuerry(string sqlCommand)
        {
            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Select
        public Operator GetOperator(string employeeCode)
        {
            string sqlCommand = "SELECT * FROM operator WHERE employee_code = \"" + employeeCode + "\"";
            Operator operatorToReturn;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                         operatorToReturn = GetOperatorFromReader(reader);
                    }
                }
            }

            return operatorToReturn;
        }
        

        private Operator GetOperatorFromReader(SQLiteDataReader reader)
        {
            Operator objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new Operator(
                reader["first_name"].ToString(),
                reader["last_name"].ToString(),
                reader["employee_code"].ToString());
            }

            return objectFromBD;
        }
        public Session GetSession(int sessionID)
        {
            string sqlCommand = "SELECT * FROM session WHERE id = " + sessionID;
            Session sessionToReturn = null;
            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        sessionToReturn = GetSessionFromReader(reader);
                    }
                }
            }
            return sessionToReturn;
        }

        private Session GetSessionFromReader(SQLiteDataReader reader)
        {
            Session objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new Session(
                Convert.ToInt32(reader["id"]),
                Convert.ToInt32(reader["arduino_id"]),
                Convert.ToInt32(reader["year"]),
                Convert.ToInt32(reader["month"]),
                Convert.ToInt32(reader["day"]),
                GetComputer(Convert.ToInt32(reader["computer_id"])),
                GetOperator(Convert.ToString(reader["employee_code"])));
            }

            return objectFromBD;
        }

        public CalibrationResult GetCalibrationResult(int id)
        {
            string sqlCommand = "SELECT * FROM calibration_result WHERE id = " + id;
            CalibrationResult sessionToReturn = null;
            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        sessionToReturn = GetCalibrationResultFromReader(reader);
                    }
                }
            }
            return sessionToReturn;
        }

        private CalibrationResult GetCalibrationResultFromReader(SQLiteDataReader reader)
        {
            CalibrationResult objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new CalibrationResult(
                    (float)Convert.ToDouble(reader["intercept1"]),
                    (float)Convert.ToDouble(reader["intercept2"]),
                    (float) Convert.ToDouble(reader["slope1"]),
                    (float) Convert.ToDouble(reader["slope2"]),
                    (float) Convert.ToDouble(reader["pearson1"]),
                    (float) Convert.ToDouble(reader["pearson2"]),
                    GetSessionRecord(Convert.ToInt32(reader["session_record_id"])),
                    reader["note"].ToString());
            }

            return objectFromBD;
        }

        public SessionRecord GetSessionRecord(int id)
        {
            string sqlCommand = "SELECT * FROM session_record WHERE id = " + id;
            SessionRecord sessionToReturn = null;
            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        sessionToReturn = GetSessionRecordFromReader(reader);
                    }
                }
            }
            return sessionToReturn;
        }

        private SessionRecord GetSessionRecordFromReader(SQLiteDataReader reader)
        {
            SessionRecord objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new SessionRecord(
                    Convert.ToInt32(reader["id"]),
                    GetSession(Convert.ToInt32(reader["session_id"])),
                    Convert.ToInt32(reader["hour"]),
                    Convert.ToInt32(reader["minute"]));
            }

            return objectFromBD;
        }

        public Computer GetComputer(int computerID)
        {
            string sqlCommand = "SELECT * FROM computer WHERE id = " + computerID;
            Computer computerToReturn;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        computerToReturn = GetComputerFromReader(reader);
                    }
                }
            }

            return computerToReturn;
        }

        private Computer GetComputerFromReader(SQLiteDataReader reader)
        {
            Computer objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new Computer(
                Convert.ToInt32(reader["id"]),
                Convert.ToInt32(reader["computer_type_id"]));
            } 

            return objectFromBD;
        }

        public KI_KMAX GetKI_KMAX(int id)
        {
            string sqlCommand = "SELECT * FROM ki_kmax WHERE id = " + id;
            KI_KMAX computerToReturn;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        computerToReturn = GetKI_KMAXFromReader(reader);
                    }
                }
            }

            return computerToReturn;
        }

        private KI_KMAX GetKI_KMAXFromReader(SQLiteDataReader reader)
        {
            KI_KMAX objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new KI_KMAX(
                    Convert.ToInt32(reader["id"]),
                    //Return null if nothing is found.
                    GetKMAX(Convert.ToInt32(reader["kmax_id"])),
                    GetMeasurementResult(Convert.ToInt32(reader["measurement_result_id"])),
                    GetSample(Convert.ToInt32(reader["sample_id"])));
            }
            return objectFromBD;
        }

        //Return null if nothing is found.
        public KMAX GetKMAX(int id)
        {
            string sqlCommand = "SELECT * FROM kmax WHERE id = " + id;
            KMAX computerToReturn;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        computerToReturn = GetKMAXFromReader(reader);
                    }
                }
            }

            return computerToReturn;
        }

        private KMAX GetKMAXFromReader(SQLiteDataReader reader)
        {
            KMAX objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new KMAX(
                Convert.ToInt32(reader["id"]),
                Convert.ToInt32(reader["kmax_duration_min"]),
                Convert.ToInt32(reader["kmax_iteration"]),
                Convert.ToInt32(reader["kmax_pression"]),
                GetKMAXMethod(Convert.ToInt32(reader["kmax_method_id"])));
            } 

            return objectFromBD;
        }

        public KMAXMethod GetKMAXMethod(int id)
        {
            string sqlCommand = "SELECT * FROM kmax_method WHERE id = " + id;
            KMAXMethod computerToReturn;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        computerToReturn = GetKMAXMethodeFromReader(reader);
                    }
                }
            }

            return computerToReturn;
        }

        private KMAXMethod GetKMAXMethodeFromReader(SQLiteDataReader reader)
        {
            KMAXMethod objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new KMAXMethod(
                Convert.ToInt32(reader["id"]),
                reader["name"].ToString());
            } 

            return objectFromBD;
        }

        public List<KMAXMethod> GetAllKMAXMethod()
        {
            string sqlCommand = "SELECT * FROM kmax_method";
            List<KMAXMethod> listToReturn = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        listToReturn = GetAllKMAXMethodeFromReader(reader);
                    }
                }
            }

            return listToReturn;
        }

        private List<KMAXMethod> GetAllKMAXMethodeFromReader(SQLiteDataReader reader)
        {
            List<KMAXMethod> objectsFromBD = new List<KMAXMethod>();
            while (reader.Read())
            {
                objectsFromBD.Add( new KMAXMethod(
                Convert.ToInt32(reader["id"]),
                reader["name"].ToString()));
            } 

            return objectsFromBD;
        }

        
        public List<Stress> GetAllStress()
        {
            string sqlCommand = "SELECT * FROM stress";

            List<Stress> listToReturn = new List<Stress>();

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        listToReturn = GetAllStressFromReader(reader);
                    }
                }
            }

            return listToReturn;
        }

        private List<Stress> GetAllStressFromReader(SQLiteDataReader reader)
        {
            List<Stress> objectFromBD = new List<Stress>();
            while (reader.Read())
            {
                objectFromBD.Add( 
                    new Stress(
                        Convert.ToInt32(reader["id"]),
                        reader["name"].ToString())
                    );
            }

            return objectFromBD;
        }

        public MeasurementResult GetMeasurementResult(int id)
        {
            string sqlCommand = "SELECT * FROM measurement_result WHERE id = " + id;
            MeasurementResult computerToReturn;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        computerToReturn = GetMeasurementResultFromReader(reader);
                    }
                }
            }

            return computerToReturn;
        }

        private MeasurementResult GetMeasurementResultFromReader(SQLiteDataReader reader)
        {
            MeasurementResult objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new MeasurementResult(
                    Convert.ToInt32(reader["id"]),
                    Convert.ToInt32(reader["water_height"]),
                    Convert.ToBoolean(reader["valid_peektube"]),
                    Convert.ToBoolean(reader["stable"]),
                    Convert.ToBoolean(reader["valid_sensor_pressure"]),
                    Convert.ToBoolean(reader["sufficient_time"]),
                    (float) Convert.ToDouble(reader["p1"]),
                    (float) Convert.ToDouble(reader["p2"]),
                    (float) Convert.ToDouble(reader["mmol_k_rough"]),
                    (float) Convert.ToDouble(reader["kg_k_rough"]),
                    (float) Convert.ToDouble(reader["cv_k"]),
                    (float) Convert.ToDouble(reader["p3"]),
                    (float) Convert.ToDouble(reader["t1"]),
                    (float) Convert.ToDouble(reader["t_av"]),
                    (float) Convert.ToDouble(reader["r_peektube_t"]),
                    (float) Convert.ToDouble(reader["t3"]),
                    (float) Convert.ToDouble(reader["k_t"]),
                    (float) Convert.ToDouble(reader["k_25"]),
                    (float) Convert.ToDouble(reader["kh"]),
                    (float) Convert.ToDouble(reader["ks"]),
                    (float) Convert.ToDouble(reader["hv"]),
                    (float) Convert.ToDouble(reader["kl"]),
                    (float) Convert.ToDouble(reader["gamme_peektube"]),
                    (float) Convert.ToDouble(reader["diameter1"]),
                    (float) Convert.ToDouble(reader["diameter2"]),
                    (float) Convert.ToDouble(reader["length"]),
                    GetPerfusionSolution(Convert.ToInt32(reader["perfusion_solution_id"])),
                    GetPeekTube(reader["peektube_code"].ToString()),
                    GetSessionRecord(Convert.ToInt32(reader["session_record_id"])),
                    reader["note"].ToString());
            } 

            return objectFromBD;
        }


        public PeekTube GetPeekTube(string code)
        {
            string sqlCommand = "SELECT * FROM peektube WHERE code = \"" + code+"\"";

            PeekTube objectFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectFromBD = GetPeekTubeFromReader(reader);
                    }
                }
            }

            return objectFromBD;
        }

        private PeekTube GetPeekTubeFromReader(SQLiteDataReader reader)
        {
            PeekTube objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new PeekTube(
                    reader["code"].ToString(),
                    reader["color"].ToString(),
                    (float) Convert.ToDouble(reader["r_peektube_25"]),
                    reader["employee_code"].ToString(),
                    Convert.ToInt32(reader["year"]),
                    Convert.ToInt32(reader["month"]),
                    Convert.ToInt32(reader["day"]),
                    Convert.ToBoolean(reader["actif"]));
            }

            return objectFromBD;
        }
        

        public List<PeekTube> GetAllActifPeekTubes()
        {
            string sqlCommand = "SELECT * FROM peektube WHERE actif = 1";

            List<PeekTube> objectsFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectsFromBD = GetAllPeekTubesFromReader(reader);
                    }
                }
            }

            return objectsFromBD;
        }

        private List<PeekTube> GetAllPeekTubesFromReader(SQLiteDataReader reader)
        {
            List<PeekTube> objectsFromBD = new List<PeekTube>();
            while (reader.Read())
            {
                objectsFromBD.Add(new PeekTube(
                    reader["code"].ToString(),
                    reader["color"].ToString(),
                    (float) Convert.ToDouble(reader["r_peektube_25"]),
                    reader["employee_code"].ToString(),
                    Convert.ToInt32(reader["year"]),
                    Convert.ToInt32(reader["month"]),
                    Convert.ToInt32(reader["day"]),
                    Convert.ToBoolean(reader["actif"])));
            }

            return objectsFromBD;
        }

        public Sample GetSample(int id)
        {
            string sqlCommand = "SELECT * FROM sample WHERE id = " + id;

            Sample objectFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectFromBD = GetSampleFromReader(reader);
                    }
                }
            }

            return objectFromBD;
        }
        public Sample GetSample(string code)
        {
            string sqlCommand = "SELECT * FROM sample WHERE code = \"" + code + "\"";

            Sample objectFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectFromBD = GetSampleFromReader(reader);
                    }
                }
            }

            return objectFromBD;
        }
        private Sample GetSampleFromReader(SQLiteDataReader reader)
        {
            Sample objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new Sample(
                    Convert.ToInt32(reader["id"]),
                    reader["id"].ToString(),
                    GetStress(Convert.ToInt32(reader["stress_id"])),
                    GetOrgan(Convert.ToInt32(reader["organ_id"])),
                    GetIndividual(reader["individual_code"].ToString()),
                    reader["physiological_age"].ToString(),
                    reader["stage"].ToString(),
                    (float) Convert.ToDouble(reader["water_potential"]));
            }

            return objectFromBD;
        }

        public Stress GetStress(int id)
        {
            string sqlCommand = "SELECT * FROM stress WHERE id = " + id;

            Stress objectFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectFromBD = GetStressFromReader(reader);
                    }
                }
            }

            return objectFromBD;
        }


        private Stress GetStressFromReader(SQLiteDataReader reader)
        {
            Stress objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new Stress(
                    Convert.ToInt32(reader["id"]),
                    reader["name"].ToString());
            }

            return objectFromBD;
        }


        public PerfusionSolution GetPerfusionSolution(int id)
        {
            string sqlCommand = "SELECT * FROM perfusion_solution WHERE id = " + id;

            PerfusionSolution objectFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectFromBD = GetPerfusionSolutionFromReader(reader);
                    }
                }
            }

            return objectFromBD;
        }

        private PerfusionSolution GetPerfusionSolutionFromReader(SQLiteDataReader reader)
        {
            PerfusionSolution objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new PerfusionSolution(
                Convert.ToInt32(reader["id"]),
                reader["name"].ToString());
            }

            return objectFromBD;
        }

        public List<PerfusionSolution> GetAllPerfusionSolutions()
        {
            string sqlCommand = "SELECT * FROM perfusion_solution";

            List<PerfusionSolution> objectsFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectsFromBD = GetAllPerfusionSolutionsFromReader(reader);
                    }
                }
            }

            return objectsFromBD;
        }

        private List<PerfusionSolution> GetAllPerfusionSolutionsFromReader(SQLiteDataReader reader)
        {
            List<PerfusionSolution> objectsFromBD = new List<PerfusionSolution>();
            while (reader.Read())
            {
                objectsFromBD.Add(new PerfusionSolution(
                    Convert.ToInt32(reader["id"]),
                    reader["name"].ToString()));
            }

            return objectsFromBD;
        }

        public Experience GetExperience(int id)
        {
            string sqlCommand = "SELECT * FROM experience WHERE id = " + id;

            Experience objectFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectFromBD = GetExperienceFromReader(reader);
                    }
                }
            }

            return objectFromBD;
        }

        private Experience GetExperienceFromReader(SQLiteDataReader reader)
        {
            Experience objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new Experience(
                Convert.ToInt32(reader["id"]),
                reader["name"].ToString());
            }

            return objectFromBD;
        }
        
        public List<Experience> GetAllExperiences()
        {
            string sqlCommand = "SELECT * FROM experience";

            List<Experience> objectsFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectsFromBD = GetAllExperiencesFromReader(reader);
                    }
                }
            }

            return objectsFromBD;
        }

        private List<Experience> GetAllExperiencesFromReader(SQLiteDataReader reader)
        {
            List<Experience> objectsFromBD = new List<Experience>();
            while (reader.Read())
            {
                objectsFromBD.Add(new Experience(
                    Convert.ToInt32(reader["id"]),
                    reader["name"].ToString()));
            }

            return objectsFromBD;
        }

        public Organ GetOrgan(int id)
        {
            string sqlCommand = "SELECT * FROM organ WHERE id = " + id;

            Organ objectFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectFromBD = GetOrganFromReader(reader);
                    }
                }
            }

            return objectFromBD;
        }

        private Organ GetOrganFromReader(SQLiteDataReader reader)
        {
            Organ objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new Organ(
                Convert.ToInt32(reader["id"]),
                reader["name"].ToString());
            }

            return objectFromBD;
        }


        public List<Organ> GetAllOrgans()
        {
            string sqlCommand = "SELECT * FROM organ";

            List<Organ> objectFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectFromBD = GetAllOrgansFromReader(reader);
                    }
                }
            }

            return objectFromBD;
        }

        private List<Organ> GetAllOrgansFromReader(SQLiteDataReader reader)
        {
            List<Organ> objectsFromBD = new List<Organ>();
            while (reader.Read())
            {
                objectsFromBD.Add (new Organ(
                    Convert.ToInt32(reader["id"]),
                    reader["name"].ToString()));
            }

            return objectsFromBD;
        }


        public Individual GetIndividual(string code)
        {
            string sqlCommand = "SELECT * FROM individual WHERE code = \"" + code+"\"";

            Individual objectFromBD = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        objectFromBD = GetIndividualFromReader(reader);
                    }
                }
            }
            return objectFromBD;
        }

        private Individual GetIndividualFromReader(SQLiteDataReader reader)
        {
            Individual objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new Individual(
                    reader["code"].ToString(),
                    GetExperience(Convert.ToInt32(reader["experience_id"])));
            }
            
            return objectFromBD;
        }
        
        public List<Individual> GetAllIndividuals()
        {
            string sqlCommand = "SELECT * FROM individual ";

            List<Individual> listToReturn = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        listToReturn = GetAllIndividualsFromReader(reader);
                    }
                }
            }
            return listToReturn;
        }

        private List<Individual> GetAllIndividualsFromReader(SQLiteDataReader reader)
        {
            List<Individual> objectsFromBD = new List<Individual>();
            while (reader.Read())
            {
                objectsFromBD.Add(new Individual(
                        reader["code"].ToString(),
                        GetExperience(Convert.ToInt32(reader["experience_id"])))
                    );
            }
            
            return objectsFromBD;
        }


        public ArduinoData GetArduinoData(int arduinoDataID)
        {
            string sqlCommand = "SELECT * FROM arduino_data WHERE id = " + arduinoDataID;

            ArduinoData arduinoDataToReturn = null;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        arduinoDataToReturn = GetArduinoDataFromReader(reader);
                    }
                }
            }

            return arduinoDataToReturn;
        }

        private ArduinoData GetArduinoDataFromReader(SQLiteDataReader reader)
        {
            ArduinoData objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new ArduinoData(
                Convert.ToInt32(reader["id"]),
                Convert.ToInt32(reader["time"]),
                Convert.ToInt32(reader["step"]),
                GetSensor(Convert.ToInt32(reader["sensor1_id"])),
                GetSensor(Convert.ToInt32(reader["sensor2_id"])),
                (float) Convert.ToDouble(reader["end_temperature"]));
            }

            return objectFromBD;
        }

        private Sensor GetSensor(int sensorID)
        {
            string sqlCommand = "SELECT * FROM sensor WHERE id = " + sensorID;
            
            Sensor sensorToReturn = null;
            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        sensorToReturn = GetSensorFromReader(reader);
                    }
                }
            }
            return sensorToReturn;
        }

        private Sensor GetSensorFromReader(SQLiteDataReader reader)
        {
            Sensor objectFromBD = null;
            if (reader.Read())
            {
                objectFromBD = new Sensor(
                Convert.ToInt32(reader["id"]),
                Convert.ToInt32(reader["pressure"]),
                Convert.ToInt32(reader["temperature"]));
            }

            return objectFromBD;
        }

        public bool ArduinoExists(int arduinoID)
        {
            string sqlCommand = "SELECT * FROM arduino WHERE id = " + arduinoID;
            bool arduinoExists = false;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        arduinoExists = reader.Read();
                    }
                }
            }

            return arduinoExists;
        }

        public bool ComputerExists(int computerID)
        {
            string sqlCommand = "SELECT * FROM computer WHERE id = " + computerID;
            bool computerExists = false;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        computerExists = reader.Read();
                    }
                }
            }

            return computerExists;
        }

        public bool IndividualExists(string individualCode)
        {
            string sqlCommand = "SELECT * FROM individual WHERE code = \"" + individualCode+"\"";
            bool individualExists = false;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        individualExists = reader.Read();
                    }
                }
            }

            return individualExists;
        }

        public bool OperatorExists(string employee_code)
        {
            string sqlCommand = "SELECT * FROM operator WHERE employee_code =\" " + employee_code + "\"";
            bool operatorExists = false;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        operatorExists = reader.Read();
                    }
                }
            }

            return operatorExists;
        }

        public bool PeekTubeExists(string code)
        {
            string sqlCommand = "SELECT * FROM peektube WHERE code = \"" + code+ "\"";
            bool peekTubeExists = false;

            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        peekTubeExists = reader.Read();
                    }
                }
            }

            return peekTubeExists;
        }


        #endregion

        #region Update
        public void UpdateArduinoDataStep(ArduinoData arduinoData)
        {
            string sqlCommand = "UPDATE arduino_data SET step = " + arduinoData.Step + " WHERE id = " + arduinoData.ID + "";
            using (SQLiteConnection c = new SQLiteConnection(connection))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, c))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
		#endregion
		
        #region delete

        public void DeleteArduinoData(long id)
        {
            string sqlCommand = "DELETE FROM arduino_data WHERE id=" + id;

            ExecuteNonQuerry(sqlCommand);
        }

        #endregion
    }
}
