﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class TestController
    {

        #region values

        // Values found in 2019_files\Kstem_measurement_v0l_échantillon test2 2019-01-18 X2

        public static List<double> prUnitsSensor1 = new List<double>()
        {
            379,
            1163,
            1929,
            2670,
            3480,
            4226,
            5008,
            5765,
            6567,
            7342
        };

        public static List<double> prUnitsSensor2 = new List<double>()
        {
            659,
            1432,
            2191,
            2910,
            3699,
            4434,
            5215,
            5959,
            6760,
            7535
        };

        public static List<double> bar = new List<double>()
        {
            0.005,
            0.01,
            0.015,
            0.02,
            0.025,
            0.03,
            0.035,
            0.04,
            0.045,
            0.05
        };

        #endregion

        public static void TestSquaredPearson()
        {
            double squareSensor1 = FormulaController.GetSquaredPearson(prUnitsSensor1, bar);
            double squareSensor2 = FormulaController.GetSquaredPearson(prUnitsSensor2, bar);
        }

        public static void TestSlope()
        {
            double slopeSensor1 = FormulaController.GetSlope(prUnitsSensor1, bar);
            double slopeSensor2 = FormulaController.GetSlope(prUnitsSensor2, bar);
        }

        public static void TestIntercept()
        {
            double InterceptSensor1 = FormulaController.GetIntercept(prUnitsSensor1, bar);
            double InterceptSensor2 = FormulaController.GetIntercept(prUnitsSensor2, bar);
        }
    }
}
