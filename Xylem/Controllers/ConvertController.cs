﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
    class ConvertController
    {
        private const string errorMessageNotUsedDigitTextBox = "Vous devez mettre des chiffres dans les zone de textes affichées.";

        public static float ConvertStringToFloat(string value)
        {
            float valueToReturn = float.NaN;

            try
            {
                valueToReturn = (float)Convert.ToDouble(value);
            }
            catch (Exception)
            {
                try
                {
                    if (value.Contains(','))
                    {
                        valueToReturn = (float)Convert.ToDouble(value.Replace(',', '.'));
                    }
                    else
                    {
                        valueToReturn = (float)Convert.ToDouble(value.Replace('.', ','));
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(errorMessageNotUsedDigitTextBox);
                }

            }
            return valueToReturn;
        }
        public static bool CanUseKeyPressOnFloat(KeyPressEventArgs e, string FullNumber)
        {
            return FullNumber.Contains(',') || FullNumber.Contains('.')
                ? !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar)
                : !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !((e.KeyChar) == ',') && !((e.KeyChar) == '.');
        }
        public static bool CanUseKeyPressOnInt(KeyPressEventArgs e)
        {
            return !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
