﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            
            SerialPortCommunicator.Initialise();
            Application.Run(new FrmConnection());
            //Application.Run(new FrmRecordMeasurement());
            //Application.Run(new FrmRecordCalibration());

        }
    } 
}
