﻿
namespace Xylem
{
	partial class FrmRecordCalibration
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.srlprtArduino = new System.IO.Ports.SerialPort(this.components);
            this.lblHeightIncrementation = new System.Windows.Forms.Label();
            this.txtHeightIncrementation = new System.Windows.Forms.TextBox();
            this.txtPearsonSensor2 = new System.Windows.Forms.TextBox();
            this.txtPearsonSensor1 = new System.Windows.Forms.TextBox();
            this.chartSensor2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartSensor1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dgvCalibration = new System.Windows.Forms.DataGridView();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Step = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sensor1Pressure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sensor2Pressure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sensor1Temperature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sensor2Temperature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndTemperature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Show = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lblPearsonSensor2 = new System.Windows.Forms.Label();
            this.lblPearsonSensor1 = new System.Windows.Forms.Label();
            this.btnContinue = new System.Windows.Forms.Button();
            this.arduinoDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.chartSensor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartSensor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalibration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arduinoDataBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // srlprtArduino
            // 
            this.srlprtArduino.DtrEnable = true;
            this.srlprtArduino.PortName = "COM0";
            this.srlprtArduino.RtsEnable = true;
            this.srlprtArduino.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPortArduino_DataReceived);
            // 
            // lblHeightIncrementation
            // 
            this.lblHeightIncrementation.AutoSize = true;
            this.lblHeightIncrementation.Location = new System.Drawing.Point(61, 674);
            this.lblHeightIncrementation.Name = "lblHeightIncrementation";
            this.lblHeightIncrementation.Size = new System.Drawing.Size(210, 28);
            this.lblHeightIncrementation.TabIndex = 24;
            this.lblHeightIncrementation.Text = "Incrément d\'hauteur (cm)";
            // 
            // txtHeightIncrementation
            // 
            this.txtHeightIncrementation.Location = new System.Drawing.Point(327, 671);
            this.txtHeightIncrementation.Name = "txtHeightIncrementation";
            this.txtHeightIncrementation.Size = new System.Drawing.Size(159, 31);
            this.txtHeightIncrementation.TabIndex = 23;
            this.txtHeightIncrementation.Text = "5";
            // 
            // txtPearsonSensor2
            // 
            this.txtPearsonSensor2.Enabled = false;
            this.txtPearsonSensor2.Location = new System.Drawing.Point(66, 621);
            this.txtPearsonSensor2.Name = "txtPearsonSensor2";
            this.txtPearsonSensor2.Size = new System.Drawing.Size(159, 31);
            this.txtPearsonSensor2.TabIndex = 17;
            // 
            // txtPearsonSensor1
            // 
            this.txtPearsonSensor1.Enabled = false;
            this.txtPearsonSensor1.Location = new System.Drawing.Point(66, 307);
            this.txtPearsonSensor1.Name = "txtPearsonSensor1";
            this.txtPearsonSensor1.Size = new System.Drawing.Size(159, 31);
            this.txtPearsonSensor1.TabIndex = 16;
            // 
            // chartSensor2
            // 
            chartArea1.Name = "ChartArea1";
            this.chartSensor2.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartSensor2.Legends.Add(legend1);
            this.chartSensor2.Location = new System.Drawing.Point(1, 340);
            this.chartSensor2.Name = "chartSensor2";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series1.Legend = "Legend1";
            series1.Name = "Steps";
            series1.YValuesPerPoint = 4;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Regression";
            this.chartSensor2.Series.Add(series1);
            this.chartSensor2.Series.Add(series2);
            this.chartSensor2.Size = new System.Drawing.Size(670, 275);
            this.chartSensor2.TabIndex = 22;
            this.chartSensor2.Text = "chart1";
            // 
            // chartSensor1
            // 
            chartArea2.Name = "ChartArea1";
            this.chartSensor1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartSensor1.Legends.Add(legend2);
            this.chartSensor1.Location = new System.Drawing.Point(1, 28);
            this.chartSensor1.Name = "chartSensor1";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series3.Legend = "Legend1";
            series3.Name = "Steps";
            series3.YValuesPerPoint = 4;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "Legend1";
            series4.Name = "Regression";
            this.chartSensor1.Series.Add(series3);
            this.chartSensor1.Series.Add(series4);
            this.chartSensor1.Size = new System.Drawing.Size(670, 270);
            this.chartSensor1.TabIndex = 21;
            this.chartSensor1.Text = "chart1";
            // 
            // dgvCalibration
            // 
            this.dgvCalibration.AllowUserToAddRows = false;
            this.dgvCalibration.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCalibration.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Time,
            this.Step,
            this.Sensor1Pressure,
            this.Sensor2Pressure,
            this.Sensor1Temperature,
            this.Sensor2Temperature,
            this.EndTemperature,
            this.ID,
            this.Show});
            this.dgvCalibration.Location = new System.Drawing.Point(698, 12);
            this.dgvCalibration.Name = "dgvCalibration";
            this.dgvCalibration.RowHeadersWidth = 51;
            this.dgvCalibration.RowTemplate.Height = 42;
            this.dgvCalibration.Size = new System.Drawing.Size(829, 782);
            this.dgvCalibration.TabIndex = 20;
            this.dgvCalibration.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCalibration_CellValueChanged);
            this.dgvCalibration.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvCalibration_UserDeletingRow);
            // 
            // Time
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Time.DefaultCellStyle = dataGridViewCellStyle1;
            this.Time.HeaderText = "Time";
            this.Time.MinimumWidth = 6;
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.Width = 80;
            // 
            // Step
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.Step.DefaultCellStyle = dataGridViewCellStyle2;
            this.Step.HeaderText = "Step";
            this.Step.MinimumWidth = 6;
            this.Step.Name = "Step";
            this.Step.Width = 75;
            // 
            // Sensor1Pressure
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Sensor1Pressure.DefaultCellStyle = dataGridViewCellStyle3;
            this.Sensor1Pressure.HeaderText = "S1P";
            this.Sensor1Pressure.MinimumWidth = 6;
            this.Sensor1Pressure.Name = "Sensor1Pressure";
            this.Sensor1Pressure.ReadOnly = true;
            // 
            // Sensor2Pressure
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Sensor2Pressure.DefaultCellStyle = dataGridViewCellStyle4;
            this.Sensor2Pressure.HeaderText = "S2P";
            this.Sensor2Pressure.MinimumWidth = 6;
            this.Sensor2Pressure.Name = "Sensor2Pressure";
            this.Sensor2Pressure.ReadOnly = true;
            // 
            // Sensor1Temperature
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Sensor1Temperature.DefaultCellStyle = dataGridViewCellStyle5;
            this.Sensor1Temperature.HeaderText = "S1T";
            this.Sensor1Temperature.MinimumWidth = 6;
            this.Sensor1Temperature.Name = "Sensor1Temperature";
            this.Sensor1Temperature.ReadOnly = true;
            // 
            // Sensor2Temperature
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Sensor2Temperature.DefaultCellStyle = dataGridViewCellStyle6;
            this.Sensor2Temperature.HeaderText = "S2T";
            this.Sensor2Temperature.MinimumWidth = 6;
            this.Sensor2Temperature.Name = "Sensor2Temperature";
            this.Sensor2Temperature.ReadOnly = true;
            // 
            // EndTemperature
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.EndTemperature.DefaultCellStyle = dataGridViewCellStyle7;
            this.EndTemperature.HeaderText = "ET";
            this.EndTemperature.MinimumWidth = 6;
            this.EndTemperature.Name = "EndTemperature";
            this.EndTemperature.ReadOnly = true;
            // 
            // ID
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ID.DefaultCellStyle = dataGridViewCellStyle8;
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            this.ID.Visible = false;
            this.ID.Width = 125;
            // 
            // Show
            // 
            this.Show.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.NullValue = false;
            this.Show.DefaultCellStyle = dataGridViewCellStyle9;
            this.Show.HeaderText = "Afficher";
            this.Show.MinimumWidth = 6;
            this.Show.Name = "Show";
            this.Show.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Show.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Show.Width = 97;
            // 
            // lblPearsonSensor2
            // 
            this.lblPearsonSensor2.AutoSize = true;
            this.lblPearsonSensor2.Location = new System.Drawing.Point(23, 624);
            this.lblPearsonSensor2.Name = "lblPearsonSensor2";
            this.lblPearsonSensor2.Size = new System.Drawing.Size(30, 28);
            this.lblPearsonSensor2.TabIndex = 19;
            this.lblPearsonSensor2.Text = "R²";
            // 
            // lblPearsonSensor1
            // 
            this.lblPearsonSensor1.AutoSize = true;
            this.lblPearsonSensor1.Location = new System.Drawing.Point(23, 308);
            this.lblPearsonSensor1.Name = "lblPearsonSensor1";
            this.lblPearsonSensor1.Size = new System.Drawing.Size(30, 28);
            this.lblPearsonSensor1.TabIndex = 18;
            this.lblPearsonSensor1.Text = "R²";
            // 
            // btnContinue
            // 
            this.btnContinue.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btnContinue.FlatAppearance.BorderSize = 0;
            this.btnContinue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContinue.Location = new System.Drawing.Point(120, 733);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(240, 58);
            this.btnContinue.TabIndex = 25;
            this.btnContinue.Text = "Terminer la calibration";
            this.btnContinue.UseVisualStyleBackColor = false;
            // 
            // arduinoDataBindingSource
            // 
            this.arduinoDataBindingSource.DataSource = typeof(Xylem.ArduinoData);
            // 
            // FrmRecordCalibration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1539, 806);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.lblHeightIncrementation);
            this.Controls.Add(this.txtHeightIncrementation);
            this.Controls.Add(this.txtPearsonSensor2);
            this.Controls.Add(this.txtPearsonSensor1);
            this.Controls.Add(this.chartSensor2);
            this.Controls.Add(this.chartSensor1);
            this.Controls.Add(this.dgvCalibration);
            this.Controls.Add(this.lblPearsonSensor2);
            this.Controls.Add(this.lblPearsonSensor1);
            this.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "FrmRecordCalibration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enregistrement";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmRecord_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartSensor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartSensor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalibration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arduinoDataBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.IO.Ports.SerialPort srlprtArduino;
		private System.Windows.Forms.BindingSource arduinoDataBindingSource;
		private System.Windows.Forms.DataVisualization.Charting.Chart chartSensor1;
		private System.Windows.Forms.DataVisualization.Charting.Chart chartSensor2;
        private System.Windows.Forms.Label lblHeightIncrementation;
        private System.Windows.Forms.TextBox txtHeightIncrementation;
        private System.Windows.Forms.TextBox txtPearsonSensor2;
        private System.Windows.Forms.TextBox txtPearsonSensor1;
        private System.Windows.Forms.DataGridView dgvCalibration;
        private System.Windows.Forms.Label lblPearsonSensor2;
        private System.Windows.Forms.Label lblPearsonSensor1;
        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Step;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sensor1Pressure;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sensor2Pressure;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sensor1Temperature;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sensor2Temperature;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndTemperature;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Show;
    }
}