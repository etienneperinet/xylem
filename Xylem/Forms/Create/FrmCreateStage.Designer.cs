﻿
namespace Xylem
{
	partial class FrmCreateStage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCreate = new System.Windows.Forms.Button();
			this.pnlName = new System.Windows.Forms.Panel();
			this.txtName = new System.Windows.Forms.TextBox();
			this.lblName = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnCreate
			// 
			this.btnCreate.BackColor = System.Drawing.Color.LightSkyBlue;
			this.btnCreate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
			this.btnCreate.FlatAppearance.BorderSize = 0;
			this.btnCreate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue;
			this.btnCreate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
			this.btnCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCreate.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCreate.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.btnCreate.Location = new System.Drawing.Point(170, 164);
			this.btnCreate.Margin = new System.Windows.Forms.Padding(4);
			this.btnCreate.Name = "btnCreate";
			this.btnCreate.Size = new System.Drawing.Size(160, 58);
			this.btnCreate.TabIndex = 35;
			this.btnCreate.Text = "Ajouter";
			this.btnCreate.UseVisualStyleBackColor = false;
			// 
			// pnlName
			// 
			this.pnlName.BackColor = System.Drawing.Color.Black;
			this.pnlName.Location = new System.Drawing.Point(14, 93);
			this.pnlName.Margin = new System.Windows.Forms.Padding(4);
			this.pnlName.Name = "pnlName";
			this.pnlName.Size = new System.Drawing.Size(309, 1);
			this.pnlName.TabIndex = 34;
			// 
			// txtName
			// 
			this.txtName.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtName.Font = new System.Drawing.Font("Sitka Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtName.Location = new System.Drawing.Point(14, 61);
			this.txtName.Margin = new System.Windows.Forms.Padding(4);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(309, 26);
			this.txtName.TabIndex = 33;
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblName.Location = new System.Drawing.Point(12, 13);
			this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(61, 35);
			this.lblName.TabIndex = 32;
			this.lblName.Text = "Nom";
			// 
			// FrmCreateStage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.ClientSize = new System.Drawing.Size(342, 235);
			this.Controls.Add(this.btnCreate);
			this.Controls.Add(this.pnlName);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.lblName);
			this.Name = "FrmCreateStage";
			this.Text = "Ajout d\'un stade";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnCreate;
		private System.Windows.Forms.Panel pnlName;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Label lblName;
	}
}