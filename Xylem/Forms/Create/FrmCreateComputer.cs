﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
    public partial class FrmCreateComputer : Form
    {
        public Computer NewComputer { get; set; }
        private string computerIDStringFromOtherForm;

        public FrmCreateComputer()
        {
            InitializeComponent();
        }

        public FrmCreateComputer(string computerStringID)
        {
            InitializeComponent();
            computerIDStringFromOtherForm = computerStringID;
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            if (IsDigitsOnly(txtEmployeeCode.Text))
            {
                if (comboComputerType.SelectedIndex != -1)
                {
                    NewComputer = new Computer();
                    NewComputer.ID = Convert.ToInt32(txtEmployeeCode.Text);
                    NewComputer.Standardized = getStandardizedFromCombo();
                    DatabaseController database = new DatabaseController();
                    database.AddComputer(NewComputer);
                    this.DialogResult = DialogResult.OK;
                    MessageBox.Show("Ordinateur ajouté à la base de données.");
                    this.Close();
                } else
                {
                    MessageBox.Show("Vous devez sélectionner un type d'ordinateur.");
                }
            }
            else
            {
                MessageBox.Show("Veuillez entrer des chiffres dans la zone de texte.");
            }
        }


        bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        bool getStandardizedFromCombo()
        {
            bool idOfObjectSelected = false;
            if (comboComputerType.Text == "Standarisé par le SAC")
            {
                idOfObjectSelected = true;
            }
            return idOfObjectSelected;
        }

        private void FrmCreateComputer_Load(object sender, EventArgs e)
        {
            btnContinue.TabStop = false;
            AcceptButton = btnContinue;
            txtEmployeeCode.Text = computerIDStringFromOtherForm;
            comboComputerType.Items.Add("Standarisé par le SAC");
            comboComputerType.Items.Add("Non standarisé");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void FrmCreateComputer_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (NewComputer == null)
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }

        private void txtEmployeeCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
