﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
	public partial class FrmCreateStress : Form
	{
		DatabaseController databaseController = new DatabaseController();
		public Stress NewStress { get; set; }

		public FrmCreateStress()
		{
			InitializeComponent();
		}

		public FrmCreateStress(string name)
		{
			InitializeComponent();
			txtName.Text = name;
		}

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (txtName.Text != String.Empty)
            {
                NewStress = new Stress(txtName.Text);
                NewStress.ID = databaseController.AddStress(NewStress);
                this.DialogResult = DialogResult.OK;
                MessageBox.Show("Stress ajouté à la base de données.");
                this.Close();
            }
            else
            {
                MessageBox.Show("La zone de textes ne peut pas être vide.");
            }
        }

        private void FrmCreateStress_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (NewStress == null)
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }
    }
}
