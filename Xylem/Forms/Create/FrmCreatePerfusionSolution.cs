﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
	public partial class FrmCreatePerfusionSolution : Form
    {
        DatabaseController databaseController = new DatabaseController();
        public PerfusionSolution NewPerfusionSolution { get; set; }
        public FrmCreatePerfusionSolution()
		{
			InitializeComponent();
		}

        public FrmCreatePerfusionSolution(string name)
        {
            InitializeComponent();
            txtName.Text = name;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (txtName.Text != String.Empty)
            {
                NewPerfusionSolution = new PerfusionSolution(txtName.Text);
                NewPerfusionSolution.ID = databaseController.AddPerfusionSolution(NewPerfusionSolution);
                this.DialogResult = DialogResult.OK;
                MessageBox.Show("Solution de perfusion ajoutée à la base de données.");
                this.Close();
            }
            else
            {
                MessageBox.Show("La zone de textes ne peut pas être vide.");
            }
        }

        private void FrmCreatePerfusionSolution_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (NewPerfusionSolution == null)
            {
                this.DialogResult = DialogResult.Cancel;
            }
            else
            {
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
