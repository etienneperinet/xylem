﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
	public partial class FrmCreateIndividual : Form
	{
		DatabaseController databaseController = new DatabaseController();
		public Individual NewIndividual { get; set; }


		public FrmCreateIndividual(string code)
		{
			InitializeComponent();
			txtCode.Text = code;
		}

		private void FrmCreateIndividual_Load(object sender, EventArgs e)
		{
			cboExperienceName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			cboExperienceName.AutoCompleteSource = AutoCompleteSource.ListItems;
			cboExperienceName.DisplayMember = "Name";
			FillComboBox();
		}

		private void btnContinue_Click(object sender, EventArgs e)
		{
			if (cboExperienceName.SelectedIndex != -1)
			{
				if (txtCode.Text.Trim() != String.Empty)
				{
					NewIndividual = new Individual(txtCode.Text, (Experience)cboExperienceName.SelectedItem);
					databaseController.AddIndividual(NewIndividual);
					MessageBox.Show("L'individu a été ajouté à la base de données");
					this.DialogResult = DialogResult.OK;
					this.Close();
				} else
				{
					MessageBox.Show("Veuillez indiquer le code de l'individu");
				}
			} else
			{
				MessageBox.Show("Veuillez sélectionner une expérience dans la liste");
			}
		}

		private void cboExperienceName_Leave(object sender, EventArgs e)
		{
			if (cboExperienceName.SelectedIndex == -1 && cboExperienceName.Text.Trim() != String.Empty)
			{
				if (cboExperienceName.FindStringExact(cboExperienceName.Text) == -1)
				{
					using (var form = new FrmCreateExperience(cboExperienceName.Text))
					{
						if (form.ShowDialog() == DialogResult.OK)
						{
							FillComboBox();
							cboExperienceName.SelectedIndex = cboExperienceName.FindStringExact(form.NewExperience.Name);
						}
					}
				}
			}
		}

		private void FillComboBox()
		{
			cboExperienceName.Items.Clear();
			List<Experience> experiences = databaseController.GetAllExperiences();
			foreach (Experience experience in experiences)
			{
				cboExperienceName.Items.Add(experience);
			}
		}

        private void FrmCreateIndividual_FormClosed(object sender, FormClosedEventArgs e)
        {
			if (NewIndividual == null)
			{
				this.DialogResult = DialogResult.Cancel;
			}
		}
    }
}
