﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
    public partial class FrmCreatePeekTube : Form
    {
        private const string notReconnnaizedEmployeeMessage = "Ce code d'employé n'est pas reconnu. Voulez-vous l'ajouter à la base de données?";

        public PeekTube NewPeekTube { get; set; }
        DatabaseController databaseController = new DatabaseController();
        public FrmCreatePeekTube()
        {
            InitializeComponent();
            DatabaseController databaseController;
        }

        public FrmCreatePeekTube(string code)
        {
            InitializeComponent();
            DatabaseController databaseController;
            txtPeekTubeCode.Text = code;
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private bool CreateEmploye()
        {
            using (var form = new FrmCreateOperator(txtEmployeeCode.Text))
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtEmployeeCode.Text = form.NewOperator.EmployeeCode;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private bool CanEmployeBeUsedAddInDBIfNeeded()
        {
            Operator employee = databaseController.GetOperator(txtEmployeeCode.Text);
            if (employee == null)
            {
                DialogResult dialogResult = MessageBox.Show
                    (notReconnnaizedEmployeeMessage, "Information inconnu", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    return CreateEmploye();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            if (txtEmployeeCode.Text != String.Empty && txtPeekTubeColor.Text != String.Empty && txtPeekTubeCode.Text != String.Empty && txtR_PeekTube_25.Text != String.Empty)
            {
                if (CanEmployeBeUsedAddInDBIfNeeded())
                { 
                    NewPeekTube = new PeekTube(txtPeekTubeCode.Text, txtPeekTubeColor.Text, ConvertController.ConvertStringToFloat(txtR_PeekTube_25.Text), txtEmployeeCode.Text);
                    databaseController.AddPeekTube(NewPeekTube);
                    this.DialogResult = DialogResult.OK;
                    MessageBox.Show("Peek tube ajouté à la base de données.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Code d'employé inutilisable.");
                }
            }
            else
            {
                MessageBox.Show("Les informations ne peuvent pas être vide.");
            }
        }


        private void FrmCreatePeekTube_Load(object sender, EventArgs e)
        {
            /*
            btnCancel.TabStop = false;
            btnContinue.TabStop = false;
            AcceptButton = btnContinue;
            txtEmployeeCode.Text = employeeCodeFromOtherForm;
            */
        }

        private void FrmCreatePeekTube_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (NewPeekTube == null)
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }

        private void txtR_PeekTube_25_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !((e.KeyChar) == ',') && !((e.KeyChar) == '.'));
        }
    }
}
