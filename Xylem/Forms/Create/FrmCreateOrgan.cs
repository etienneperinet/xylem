﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
	public partial class FrmCreateOrgan : Form
	{
        DatabaseController databaseController = new DatabaseController();
        public Organ NewOrgan { get; set; }
        public FrmCreateOrgan()
		{
			InitializeComponent();
		}

        public FrmCreateOrgan(string name)
		{
			InitializeComponent();
            txtName.Text = name;
        }

        private void FrmCreateOrgan_Load(object sender, EventArgs e)
        {

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (txtName.Text != String.Empty)
            {
                NewOrgan = new Organ(txtName.Text);
                NewOrgan.ID = databaseController.AddOrgan(NewOrgan);
                this.DialogResult = DialogResult.OK;
                MessageBox.Show("Organe ajouté à la base de données.");
                this.Close();
            }
            else
            {
                MessageBox.Show("La zone de textes ne peut pas être vide.");
            }
        }

        private void FrmCreateOrgan_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (NewOrgan == null)
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }
    }
}
