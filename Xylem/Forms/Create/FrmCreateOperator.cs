﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
    public partial class FrmCreateOperator : Form
    {
        string employeeCodeFromOtherForm;
        public Operator NewOperator { get; set; }
        public FrmCreateOperator(string employeeCode)
        {
            InitializeComponent();
            this.employeeCodeFromOtherForm = employeeCode;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            if (txtEmployeeFirstName.Text != String.Empty && txtEmployeeLastName.Text != String.Empty && txtEmployeeCode.Text != String.Empty)
            {
                NewOperator = new Operator(
                    txtEmployeeFirstName.Text,
                    txtEmployeeLastName.Text,
                    txtEmployeeCode.Text);
                DatabaseController databaseController = new DatabaseController();
                databaseController.AddOperator(NewOperator);
                this.DialogResult = DialogResult.OK;
                MessageBox.Show("Opérateur ajouté à la base de données");
                this.Close();
            } else
            {
                MessageBox.Show("Les informations ne peuvent pas être vide");
            }
        }

        private void FrmCreateOperator_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(NewOperator == null)
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }

        private void FrmCreateOperator_Load(object sender, EventArgs e)
        {
            btnCancel.TabStop = false;
            btnContinue.TabStop = false;
            AcceptButton = btnContinue;
            txtEmployeeCode.Text = employeeCodeFromOtherForm;
        }
    }
}
