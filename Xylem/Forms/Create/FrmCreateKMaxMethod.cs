﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
	public partial class FrmCreateKMaxMethod : Form
	{
        DatabaseController databaseController = new DatabaseController();
        public KMAXMethod NewKMAXMethod { get; set; }

        public FrmCreateKMaxMethod()
		{
			InitializeComponent();
		}

        public FrmCreateKMaxMethod(string name)
        {
            InitializeComponent();
            txtName.Text = name;
        }
        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (txtName.Text != String.Empty)
            {
                NewKMAXMethod = new KMAXMethod(txtName.Text);
                NewKMAXMethod.ID = databaseController.AddKMAXMethod(NewKMAXMethod);
                this.DialogResult = DialogResult.OK;
                MessageBox.Show("Solution de perfusion ajoutée à la base de données.");
                this.Close();
            }
            else
            {
                MessageBox.Show("La zone de textes ne peut pas être vide.");
            }
        }

        private void FrmCreateKMaxMethod_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (NewKMAXMethod == null)
            {
                this.DialogResult = DialogResult.Cancel;
            }
            else
            {
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
