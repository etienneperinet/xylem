﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
	public partial class FrmCreateSample : Form
	{
		DatabaseController databaseController = new DatabaseController();
		public Sample NewSample { get; set; }
		public FrmCreateSample()
		{
			InitializeComponent();
		}
		public FrmCreateSample(string code)
		{
			InitializeComponent();
			txtCode.Text = code;
		}

		private void btnContinue_Click(object sender, EventArgs e)
		{
			if (ValidComboBox())
			{
				if (txtCode.Text.Trim() != String.Empty && txtPhysiologicalAge.Text.Trim() != String.Empty && txtStage.Text.Trim() != String.Empty && txtWaterPotential.Text.Trim() != String.Empty && !float.IsNaN(ConvertController.ConvertStringToFloat(txtWaterPotential.Text)))
				{
					CreateNewSample();
				}
				else
				{
					MessageBox.Show("Veuillez remplir les zones de textes");
				}
			}
		}

		private void CreateNewSample()
        {
			NewSample = new Sample();
            try
            {
				NewSample = new Sample();
				NewSample.WaterPotential = (float)Convert.ToDouble(txtWaterPotential.Text);
				NewSample.Code = txtCode.Text;
				NewSample.Individual = (Individual)cboIndividual.SelectedItem;
				NewSample.Organ = (Organ)cboOrgan.SelectedItem;
				NewSample.Stress = (Stress)cboStress.SelectedItem;
				NewSample.Stage = txtStage.Text;
				NewSample.PhysiologicalAge = txtPhysiologicalAge.Text;
				NewSample.ID = databaseController.AddSample(NewSample);

				MessageBox.Show("L'échantillon a correctement été ajouté à la base de données");
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
            catch (Exception)
            {
				MessageBox.Show("Veuillez indiquer un nombre pour la zone de textes \"Pontentiel\".");
			}
		}

		private bool ValidComboBox()
        {
			bool valid = true;
			string message = "Veuillez sélectionner ";
            if (cboIndividual.SelectedIndex == -1)
            {
				message += "un individu, ";
				valid = false;
			}
            if (cboOrgan.SelectedIndex == -1)
            {
				message += "un organ, ";
				valid = false;
			}
            if (cboStress.SelectedIndex == -1)
            {
				message += "un stress, ";
				valid = false;
			}

            if (!valid)
            {
				message = message.Remove(message.Length - 2);
				MessageBox.Show(message);
            }
			return valid;

		}

		private void FrmCreateSample_Load(object sender, EventArgs e)
		{
			SetCombobox();
			FillComboBoxIndividual();
			FillComboBoxOrgan();
			FillComboBoxStress();
		}

		private void SetCombobox()
		{
			cboIndividual.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			cboIndividual.AutoCompleteSource = AutoCompleteSource.ListItems;
			cboIndividual.DisplayMember = "Code";
			cboOrgan.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			cboOrgan.AutoCompleteSource = AutoCompleteSource.ListItems;
			cboOrgan.DisplayMember = "Name";
			cboStress.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			cboStress.AutoCompleteSource = AutoCompleteSource.ListItems;
			cboStress.DisplayMember = "Name";
		}

		private void FillComboBoxIndividual()
		{
			cboIndividual.Items.Clear();
			List<Individual> objectsFromBD = databaseController.GetAllIndividuals();
			foreach (Individual objectFromBD in objectsFromBD)
			{
				cboIndividual.Items.Add(objectFromBD);
			}
		}

		private void FillComboBoxOrgan()
		{
			cboOrgan.Items.Clear();
			List<Organ> objectsFromBD = databaseController.GetAllOrgans();
			foreach (Organ objectFromBD in objectsFromBD)
			{
				cboOrgan.Items.Add(objectFromBD);
			}
		}
		private void FillComboBoxStress()
		{
			cboStress.Items.Clear();
			List<Stress> objectsFromBD = databaseController.GetAllStress();
			foreach (Stress objectFromBD in objectsFromBD)
			{
				cboStress.Items.Add(objectFromBD);
			}
		}

		private void cboIndividual_Leave(object sender, EventArgs e)
		{
			if (cboIndividual.SelectedIndex == -1 && cboIndividual.Text.Trim() != String.Empty)
			{
				if (cboIndividual.FindStringExact(cboIndividual.Text) == -1)
				{
					using (var form = new FrmCreateIndividual(cboIndividual.Text))
					{
						if (form.ShowDialog() == DialogResult.OK)
						{
							FillComboBoxIndividual();
							cboIndividual.SelectedIndex = cboIndividual.FindStringExact(form.NewIndividual.Code);
						}
					}
				}
			}
		}

		private void cboOrgan_Leave(object sender, EventArgs e)
        {
			if (cboOrgan.SelectedIndex == -1 && cboOrgan.Text.Trim() != String.Empty)
			{
				if (cboOrgan.FindStringExact(cboOrgan.Text) == -1)
				{
					using (var form = new FrmCreateOrgan(cboOrgan.Text))
					{
						if (form.ShowDialog() == DialogResult.OK)
						{
							FillComboBoxOrgan();
							cboOrgan.SelectedIndex = cboOrgan.FindStringExact(form.NewOrgan.Name);
						}
					}
				}
			}
		}

        private void cboStress_Leave(object sender, EventArgs e)
        {
			if (cboStress.SelectedIndex == -1 && cboStress.Text.Trim() != String.Empty)
			{
				if (cboStress.FindStringExact(cboStress.Text) == -1)
				{
					using (var form = new FrmCreateStress(cboStress.Text))
					{
						if (form.ShowDialog() == DialogResult.OK)
						{
							FillComboBoxStress();
							cboStress.SelectedIndex = cboStress.FindStringExact(form.NewStress.Name);
						}
					}
				}
			}
		}

        private void txtWaterPotential_KeyPress(object sender, KeyPressEventArgs e)
        {
			e.Handled = ConvertController.CanUseKeyPressOnFloat(e, txtWaterPotential.Text);
        }
    }
}
