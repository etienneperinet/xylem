﻿
namespace Xylem
{
	partial class FrmCreateSample
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtPhysiologicalAge = new System.Windows.Forms.TextBox();
            this.lblPhysiologicalAge = new System.Windows.Forms.Label();
            this.txtWaterPotential = new System.Windows.Forms.TextBox();
            this.lblWaterPotential = new System.Windows.Forms.Label();
            this.cboStress = new System.Windows.Forms.ComboBox();
            this.lblStress = new System.Windows.Forms.Label();
            this.lblOrgan = new System.Windows.Forms.Label();
            this.cboOrgan = new System.Windows.Forms.ComboBox();
            this.lblIndividual = new System.Windows.Forms.Label();
            this.cboIndividual = new System.Windows.Forms.ComboBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblStage = new System.Windows.Forms.Label();
            this.txtStage = new System.Windows.Forms.TextBox();
            this.btnContinue = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtPhysiologicalAge
            // 
            this.txtPhysiologicalAge.Location = new System.Drawing.Point(44, 153);
            this.txtPhysiologicalAge.Name = "txtPhysiologicalAge";
            this.txtPhysiologicalAge.Size = new System.Drawing.Size(230, 31);
            this.txtPhysiologicalAge.TabIndex = 0;
            // 
            // lblPhysiologicalAge
            // 
            this.lblPhysiologicalAge.AutoSize = true;
            this.lblPhysiologicalAge.Location = new System.Drawing.Point(44, 115);
            this.lblPhysiologicalAge.Name = "lblPhysiologicalAge";
            this.lblPhysiologicalAge.Size = new System.Drawing.Size(150, 28);
            this.lblPhysiologicalAge.TabIndex = 1;
            this.lblPhysiologicalAge.Text = "Age physiologique";
            // 
            // txtWaterPotential
            // 
            this.txtWaterPotential.Location = new System.Drawing.Point(44, 243);
            this.txtWaterPotential.Name = "txtWaterPotential";
            this.txtWaterPotential.Size = new System.Drawing.Size(230, 31);
            this.txtWaterPotential.TabIndex = 2;
            this.txtWaterPotential.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWaterPotential_KeyPress);
            // 
            // lblWaterPotential
            // 
            this.lblWaterPotential.AutoSize = true;
            this.lblWaterPotential.Location = new System.Drawing.Point(44, 207);
            this.lblWaterPotential.Name = "lblWaterPotential";
            this.lblWaterPotential.Size = new System.Drawing.Size(79, 28);
            this.lblWaterPotential.TabIndex = 3;
            this.lblWaterPotential.Text = "Potentiel";
            // 
            // cboStress
            // 
            this.cboStress.FormattingEnabled = true;
            this.cboStress.Location = new System.Drawing.Point(485, 56);
            this.cboStress.Name = "cboStress";
            this.cboStress.Size = new System.Drawing.Size(230, 34);
            this.cboStress.TabIndex = 11;
            this.cboStress.Leave += new System.EventHandler(this.cboStress_Leave);
            // 
            // lblStress
            // 
            this.lblStress.AutoSize = true;
            this.lblStress.Location = new System.Drawing.Point(485, 13);
            this.lblStress.Name = "lblStress";
            this.lblStress.Size = new System.Drawing.Size(56, 28);
            this.lblStress.TabIndex = 12;
            this.lblStress.Text = "Stress";
            // 
            // lblOrgan
            // 
            this.lblOrgan.AutoSize = true;
            this.lblOrgan.Location = new System.Drawing.Point(485, 110);
            this.lblOrgan.Name = "lblOrgan";
            this.lblOrgan.Size = new System.Drawing.Size(67, 28);
            this.lblOrgan.TabIndex = 14;
            this.lblOrgan.Text = "Organe";
            // 
            // cboOrgan
            // 
            this.cboOrgan.FormattingEnabled = true;
            this.cboOrgan.Location = new System.Drawing.Point(485, 148);
            this.cboOrgan.Name = "cboOrgan";
            this.cboOrgan.Size = new System.Drawing.Size(230, 34);
            this.cboOrgan.TabIndex = 13;
            this.cboOrgan.Leave += new System.EventHandler(this.cboOrgan_Leave);
            // 
            // lblIndividual
            // 
            this.lblIndividual.AutoSize = true;
            this.lblIndividual.Location = new System.Drawing.Point(485, 200);
            this.lblIndividual.Name = "lblIndividual";
            this.lblIndividual.Size = new System.Drawing.Size(75, 28);
            this.lblIndividual.TabIndex = 16;
            this.lblIndividual.Text = "Individu";
            // 
            // cboIndividual
            // 
            this.cboIndividual.FormattingEnabled = true;
            this.cboIndividual.Location = new System.Drawing.Point(485, 238);
            this.cboIndividual.Name = "cboIndividual";
            this.cboIndividual.Size = new System.Drawing.Size(230, 34);
            this.cboIndividual.TabIndex = 15;
            this.cboIndividual.Leave += new System.EventHandler(this.cboIndividual_Leave);
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(44, 56);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(230, 31);
            this.txtCode.TabIndex = 17;
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(44, 18);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(49, 28);
            this.lblCode.TabIndex = 18;
            this.lblCode.Text = "Code";
            // 
            // lblStage
            // 
            this.lblStage.AutoSize = true;
            this.lblStage.Location = new System.Drawing.Point(44, 304);
            this.lblStage.Name = "lblStage";
            this.lblStage.Size = new System.Drawing.Size(53, 28);
            this.lblStage.TabIndex = 19;
            this.lblStage.Text = "Stade";
            // 
            // txtStage
            // 
            this.txtStage.Location = new System.Drawing.Point(44, 342);
            this.txtStage.Name = "txtStage";
            this.txtStage.Size = new System.Drawing.Size(230, 31);
            this.txtStage.TabIndex = 20;
            // 
            // btnContinue
            // 
            this.btnContinue.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btnContinue.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnContinue.FlatAppearance.BorderSize = 0;
            this.btnContinue.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnContinue.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.btnContinue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContinue.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContinue.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnContinue.Location = new System.Drawing.Point(568, 341);
            this.btnContinue.Margin = new System.Windows.Forms.Padding(4);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(160, 58);
            this.btnContinue.TabIndex = 28;
            this.btnContinue.Text = "Continuer";
            this.btnContinue.UseVisualStyleBackColor = false;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // FrmCreateSample
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(741, 412);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.txtStage);
            this.Controls.Add(this.lblStage);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.lblIndividual);
            this.Controls.Add(this.cboIndividual);
            this.Controls.Add(this.lblOrgan);
            this.Controls.Add(this.cboOrgan);
            this.Controls.Add(this.lblStress);
            this.Controls.Add(this.cboStress);
            this.Controls.Add(this.lblWaterPotential);
            this.Controls.Add(this.txtWaterPotential);
            this.Controls.Add(this.lblPhysiologicalAge);
            this.Controls.Add(this.txtPhysiologicalAge);
            this.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "FrmCreateSample";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajout d\'un échantillon";
            this.Load += new System.EventHandler(this.FrmCreateSample_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtPhysiologicalAge;
		private System.Windows.Forms.Label lblPhysiologicalAge;
		private System.Windows.Forms.TextBox txtWaterPotential;
		private System.Windows.Forms.Label lblWaterPotential;
		private System.Windows.Forms.ComboBox cboStress;
		private System.Windows.Forms.Label lblStress;
		private System.Windows.Forms.Label lblOrgan;
		private System.Windows.Forms.ComboBox cboOrgan;
		private System.Windows.Forms.Label lblIndividual;
		private System.Windows.Forms.ComboBox cboIndividual;
		private System.Windows.Forms.TextBox txtCode;
		private System.Windows.Forms.Label lblCode;
		private System.Windows.Forms.Label lblStage;
		private System.Windows.Forms.TextBox txtStage;
		private System.Windows.Forms.Button btnContinue;
	}
}