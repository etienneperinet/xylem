﻿
namespace Xylem
{
	partial class FrmCreateIndividual
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cboExperienceName = new System.Windows.Forms.ComboBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblExperienceName = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.btnContinue = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cboExperienceName
            // 
            this.cboExperienceName.DisplayMember = "Text";
            this.cboExperienceName.FormattingEnabled = true;
            this.cboExperienceName.Location = new System.Drawing.Point(28, 188);
            this.cboExperienceName.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cboExperienceName.Name = "cboExperienceName";
            this.cboExperienceName.Size = new System.Drawing.Size(294, 34);
            this.cboExperienceName.TabIndex = 4;
            this.cboExperienceName.ValueMember = "ID";
            this.cboExperienceName.Leave += new System.EventHandler(this.cboExperienceName_Leave);
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(23, 25);
            this.lblCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(49, 28);
            this.lblCode.TabIndex = 1;
            this.lblCode.Text = "Code";
            // 
            // lblExperienceName
            // 
            this.lblExperienceName.AutoSize = true;
            this.lblExperienceName.Location = new System.Drawing.Point(23, 147);
            this.lblExperienceName.Name = "lblExperienceName";
            this.lblExperienceName.Size = new System.Drawing.Size(165, 28);
            this.lblExperienceName.TabIndex = 2;
            this.lblExperienceName.Text = "Nom de l\'expérience";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(29, 71);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(293, 31);
            this.txtCode.TabIndex = 3;
            // 
            // btnContinue
            // 
            this.btnContinue.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btnContinue.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnContinue.FlatAppearance.BorderSize = 0;
            this.btnContinue.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnContinue.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.btnContinue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContinue.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContinue.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnContinue.Location = new System.Drawing.Point(162, 283);
            this.btnContinue.Margin = new System.Windows.Forms.Padding(4);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(160, 58);
            this.btnContinue.TabIndex = 28;
            this.btnContinue.Text = "Continuer";
            this.btnContinue.UseVisualStyleBackColor = false;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // FrmCreateIndividual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(358, 384);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.lblExperienceName);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.cboExperienceName);
            this.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "FrmCreateIndividual";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajout d\'un individu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmCreateIndividual_FormClosed);
            this.Load += new System.EventHandler(this.FrmCreateIndividual_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox cboExperienceName;
		private System.Windows.Forms.Label lblCode;
		private System.Windows.Forms.Label lblExperienceName;
		private System.Windows.Forms.TextBox txtCode;
		private System.Windows.Forms.Button btnContinue;
	}
}