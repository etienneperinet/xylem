﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
	public partial class FrmCreateMeasurement : Form
	{
        private const string errorMessageSampleCreation = "Vous devez indiquer un échantillon pour pouvoir continuer.";
        private const string errorMessageNotUsedAllTextBox = "Vous devez remplir toutes les zones de texte affichées.";

        List<PerfusionSolution> perfusionSolutions;
        List<PeekTube> peekTubes;
        DatabaseController databaseController = new DatabaseController();
        //public Individual NewIndividual { get; set; }

        private KI_KMAX ki_kmax;
		public FrmCreateMeasurement()
		{
			InitializeComponent();
			databaseController = new DatabaseController();
            if (Session.GetInstance() == null)
            {
                databaseController.CreateSessionTest();
            }
            else
            {
            }
            
            ki_kmax = new KI_KMAX();
            ki_kmax.KMax = null;
        }

        private void FrmCreateMeasurement_Load(object sender, EventArgs e)
        {
            LoadKI_KMAXComboBox();
            LoadKMAXMethodComboBox();
            LoadPeektubes();
            LoadPerfusionSolutions();
            LoadWaterHeightComboBox();
        }

        private void LoadPeektubes()
        {
            peekTubes = databaseController.GetAllActifPeekTubes();
            foreach (PeekTube peekTube in peekTubes)
            {
                cboPeektube.Items.Add(peekTube);
            }
        }
        private void LoadPerfusionSolutions()
        {
            perfusionSolutions = databaseController.GetAllPerfusionSolutions();
            foreach (PerfusionSolution perfusionSolution in perfusionSolutions)
            {
                cboSolutionPerfusion.Items.Add(perfusionSolution);
            }

        }

        private void LoadWaterHeightComboBox()
        {
            for (int i = 0; i < 51; i+=5)
            {
                cboWaterHeight.Items.Add(i);
            }
        }

        private void LoadKI_KMAXComboBox()
        {
            cboKI_KMAX.Items.Add("KMAX");
            cboKI_KMAX.Items.Add("KI");
        }

        private void LoadKMAXMethodComboBox()
        {
            cboKMAXMethod.Items.Clear();
            List<KMAXMethod> kmaxMethods = databaseController.GetAllKMAXMethod();
            foreach (KMAXMethod kmaxMethod in kmaxMethods)
            {
                cboKMAXMethod.Items.Add(kmaxMethod);
            }
        }


        private void DisableEnableKMAX(bool enabled)
        {
            txtKMAXDuration.Enabled = enabled;
            txtKMAXIteration.Enabled = enabled;
            txtKMAXPression.Enabled = enabled;
            cboKMAXMethod.Enabled = enabled;
        }
        

        private bool CanUseSegment()
        {
            bool answer = true;
            if (databaseController.GetSample(txtSampleCode.Text) == null)
            {
                answer = false;
            }
            return answer;
        }

        private void CreateSegment()
        {
            using (var form = new FrmCreateSample(txtSampleCode.Text))
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    ki_kmax.Sample = form.NewSample;
                    askToCreateObjectInformation();
                }
                else
                {
                    MessageBox.Show(errorMessageSampleCreation);
                }
            }
        }

        private MeasurementResult GenerateMeasurementResultWithCurrentInformations()
        {
            MeasurementResult onReturn = new MeasurementResult();

            onReturn.GammePeekTube = ConvertController.ConvertStringToFloat(txtPeekTubeGamme.Text);
            onReturn.WaterHeight = Convert.ToInt32(cboWaterHeight.Text);

           
            return onReturn;
        }

        private void createKMAX()
        {
            if (cboKI_KMAX.Text == "KMAX")
            {
                KMAXMethod method = (KMAXMethod)cboKMAXMethod.SelectedItem;
                ki_kmax.KMax = new KMAX(
                    Convert.ToInt32(txtKMAXDuration.Text),
                    Convert.ToInt32(txtKMAXIteration.Text),
                    Convert.ToInt32(txtKMAXPression.Text),
                    databaseController.GetKMAXMethod(method.ID));
            }
            ki_kmax.MeasurementResult = GenerateMeasurementResultWithCurrentInformations();
        }

        private void CreateKMAXMethod()
        {
            using (var form = new FrmCreateKMaxMethod(cboKMAXMethod.Text))
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    cboKMAXMethod.Items.Add(form.NewKMAXMethod);
                    cboKMAXMethod.SelectedIndex = cboKMAXMethod.Items.Count - 1;
                    askToCreateObjectInformation();
                }
            }
        }
        private bool IsKMAXMethodOk()
        {
            if (cboKI_KMAX.Text == "KMAX" || cboKI_KMAX.Text == "")
            {
                if (cboKMAXMethod.SelectedIndex == -1)
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsPeekTubeOk()
        {
            if (cboPeektube.SelectedIndex == -1)
            {
                foreach (PeekTube item in peekTubes)
                {
                    if (item.Code == cboPeektube.Text)
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        private bool IsPerfusionSolutionOk()
        {
            if (cboSolutionPerfusion.SelectedIndex == -1)
            {
                foreach (PerfusionSolution item in perfusionSolutions)
                {
                    if (item.Name == cboSolutionPerfusion.Text)
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }


        private void CreateSolutionPerfusion()
        {
            using (var form = new FrmCreatePerfusionSolution(cboSolutionPerfusion.Text))
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    cboSolutionPerfusion.Items.Add(form.NewPerfusionSolution);
                    cboSolutionPerfusion.SelectedIndex = cboSolutionPerfusion.Items.Count - 1;
                    askToCreateObjectInformation();
                }
            }
        }

        private bool IsKI_KMAXOk()
        {
            if (cboKI_KMAX.Text == "KMAX")
            {
                return (
                    txtKMAXDuration.Text != "" &&
                    txtKMAXIteration.Text != "" &&
                    txtKMAXPression.Text != "" 
                    );
            }
            else if (cboKI_KMAX.Text == "KI")
            {
                return true;
            }
            return false;
        }

        private bool IsDataNecessaryThere()
        {
            return (IsKI_KMAXOk() &&
                txtSegmentDiameter1.Text != "" &&
                txtSegmentDiameter2.Text != "" &&
                txtSegmentLength.Text != "" &&
                txtSampleCode.Text != "" &&
                txtPeekTubeGamme.Text != "" &&
                cboWaterHeight.Text != "" &&
                txtSegmentDiameter1.Text != "" &&
                txtSegmentDiameter1.Text != "");
        }

        private void CreatePeektube()
        {
             using (var form = new FrmCreatePeekTube(cboPeektube.Text))
             {
                 var result = form.ShowDialog();
                 if (result == DialogResult.OK)
                 {
                     cboPeektube.Items.Add(form.NewPeekTube);
                     cboPeektube.SelectedIndex = cboPeektube.Items.Count - 1;
                    askToCreateObjectInformation();
                 }
             }
        }
        

        private void btnContinue_Click_1(object sender, EventArgs e)
        {
            if (IsDataNecessaryThere())
            {
                if (askToAddInformation())
                {
                    askToCreateObjectInformation();
                }
            }
            else
            {
                MessageBox.Show(errorMessageNotUsedAllTextBox);
            }
            //Create MeasurementResult (give the info needed)
        }
        
        private void updateKI_KMAXTextBoxs()
        {
            if (cboKI_KMAX.Text == "KI")
            {
                DisableEnableKMAX(false);
            }
            else
            {
                DisableEnableKMAX(true);
            }
        }

        private void cboKI_KMAX_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateKI_KMAXTextBoxs();
        }

        private string getMessageToDisplayToUser()
        {
            bool needMessage = false;
            string dialogMessage = "Certaines informations que vous avez donné ne sont pas connu tel que : ";
            if (!IsPeekTubeOk())
            {
                needMessage = true;
                dialogMessage += "\n - Le code de Peek Tube";
            }
            if (!IsPerfusionSolutionOk())
            {
                needMessage = true;
                dialogMessage += "\n - La solution de Perfusion";
            }
            if (!IsKMAXMethodOk())
            {
                needMessage = true;
                dialogMessage += "\n - La méthode KMAX";
            }
            if (!CanUseSegment())
            {
                needMessage = true;
                dialogMessage += "\n - Le code de l'échantillon";
            }

            if (needMessage)
            {
                dialogMessage += "\n\n Voulez-vous les créer?";
            }
            else
            {
                dialogMessage = "";
            }
            
            return dialogMessage;
        }

        private bool askToAddInformation()
        {
            bool askToCreateObject = false;
            string dialogMessage = getMessageToDisplayToUser();
            if (dialogMessage != "")
            {
                DialogResult dialogResult = MessageBox.Show(dialogMessage, "Information inconnu", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    askToCreateObject = true;
                }
            }
            else
            {
                ContinueToRecordingMeasurment();
            }
            return askToCreateObject;
        }

        private void askToCreateObjectInformation()
        {
            if (!IsPeekTubeOk())
            {
                CreatePeektube();
            }
            else if (!IsPerfusionSolutionOk())
            {
                CreateSolutionPerfusion();
            }
            else if (!IsKMAXMethodOk())
            {
                CreateKMAXMethod();
            }
            else if (!CanUseSegment())
            {
                CreateSegment();
            }
            else
            {
                ContinueToRecordingMeasurment();
            }

        }

        private void ContinueToRecordingMeasurment()
        {
            createKMAX();
            this.Hide();
            using (var form = new FrmRecordMeasurement())
            {
                form.ShowDialog();
            }
            this.Show();
        }

        

        private void txtPeekTubeGamme_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ConvertController.CanUseKeyPressOnFloat(e, txtPeekTubeGamme.Text);
        }

        private void txtSegmentDiameter2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ConvertController.CanUseKeyPressOnFloat(e, txtSegmentDiameter2.Text);
        }

        private void txtSegmentDiameter1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ConvertController.CanUseKeyPressOnFloat(e, txtSegmentDiameter1.Text);
        }

        private void txtSegmentLength_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ConvertController.CanUseKeyPressOnFloat(e, txtSegmentLength.Text);
        }

        private void txtKMAXDuration_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ConvertController.CanUseKeyPressOnInt(e);
        }

        private void txtKMAXIteration_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ConvertController.CanUseKeyPressOnInt(e);
        }

        private void txtKMAXPression_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = ConvertController.CanUseKeyPressOnFloat(e, txtKMAXPression.Text);
        }

        private void cboSolutionPerfusion_Leave(object sender, EventArgs e)
        {
            if (!IsPerfusionSolutionOk())
            {
                CreateSolutionPerfusion();
            }
        }

        private void cboKMAXMethod_Leave(object sender, EventArgs e)
        {
            if (!IsKMAXMethodOk())
            {
                CreateKMAXMethod();
            }
        }
    }
}
