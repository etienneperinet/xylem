﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
	public partial class FrmCreateExperience : Form
    {
        DatabaseController databaseController = new DatabaseController();
        public Experience NewExperience { get; set; }

		public FrmCreateExperience()
		{
			InitializeComponent();
		}

		public FrmCreateExperience(string name)
		{
			InitializeComponent();
            txtName.Text = name;
		}

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (txtName.Text != String.Empty)
            {
                NewExperience = new Experience(txtName.Text);
                NewExperience.ID = databaseController.AddExperience(NewExperience);
                this.DialogResult = DialogResult.OK;
                MessageBox.Show("Experience ajouté à la base de données.");
                this.Close();
            }
            else
            {
                MessageBox.Show("La zone de textes ne peut pas être vide.");
            }
        }

        private void FrmCreateExperience_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (NewExperience == null)
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }
    }
}
