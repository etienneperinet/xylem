﻿
namespace Xylem
{
    partial class FrmCreatePeekTube
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreate = new System.Windows.Forms.Button();
            this.pnlPeekTubeColor = new System.Windows.Forms.Panel();
            this.txtPeekTubeColor = new System.Windows.Forms.TextBox();
            this.lblPeekTubeColor = new System.Windows.Forms.Label();
            this.pnlPeekTubeCode = new System.Windows.Forms.Panel();
            this.pnlEmployeeCode = new System.Windows.Forms.Panel();
            this.txtPeekTubeCode = new System.Windows.Forms.TextBox();
            this.lblPeekTubeCode = new System.Windows.Forms.Label();
            this.txtEmployeeCode = new System.Windows.Forms.TextBox();
            this.lblEmployeeCode = new System.Windows.Forms.Label();
            this.pnlR_PeekTube_25 = new System.Windows.Forms.Panel();
            this.txtR_PeekTube_25 = new System.Windows.Forms.TextBox();
            this.lblR_PeekTube_25 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCreate
            // 
            this.btnCreate.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btnCreate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCreate.FlatAppearance.BorderSize = 0;
            this.btnCreate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnCreate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.btnCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreate.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreate.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCreate.Location = new System.Drawing.Point(459, 299);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(120, 47);
            this.btnCreate.TabIndex = 27;
            this.btnCreate.Text = "Ajouter";
            this.btnCreate.UseVisualStyleBackColor = false;
            this.btnCreate.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // pnlPeekTubeColor
            // 
            this.pnlPeekTubeColor.BackColor = System.Drawing.Color.Black;
            this.pnlPeekTubeColor.Location = new System.Drawing.Point(12, 158);
            this.pnlPeekTubeColor.Name = "pnlPeekTubeColor";
            this.pnlPeekTubeColor.Size = new System.Drawing.Size(232, 1);
            this.pnlPeekTubeColor.TabIndex = 25;
            // 
            // txtPeekTubeColor
            // 
            this.txtPeekTubeColor.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPeekTubeColor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPeekTubeColor.Font = new System.Drawing.Font("Sitka Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPeekTubeColor.Location = new System.Drawing.Point(12, 132);
            this.txtPeekTubeColor.Name = "txtPeekTubeColor";
            this.txtPeekTubeColor.Size = new System.Drawing.Size(232, 21);
            this.txtPeekTubeColor.TabIndex = 24;
            // 
            // lblPeekTubeColor
            // 
            this.lblPeekTubeColor.AutoSize = true;
            this.lblPeekTubeColor.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeekTubeColor.Location = new System.Drawing.Point(7, 100);
            this.lblPeekTubeColor.Name = "lblPeekTubeColor";
            this.lblPeekTubeColor.Size = new System.Drawing.Size(72, 28);
            this.lblPeekTubeColor.TabIndex = 23;
            this.lblPeekTubeColor.Text = "Couleur";
            // 
            // pnlPeekTubeCode
            // 
            this.pnlPeekTubeCode.BackColor = System.Drawing.Color.Black;
            this.pnlPeekTubeCode.Location = new System.Drawing.Point(8, 82);
            this.pnlPeekTubeCode.Name = "pnlPeekTubeCode";
            this.pnlPeekTubeCode.Size = new System.Drawing.Size(232, 1);
            this.pnlPeekTubeCode.TabIndex = 20;
            // 
            // pnlEmployeeCode
            // 
            this.pnlEmployeeCode.BackColor = System.Drawing.Color.Black;
            this.pnlEmployeeCode.Location = new System.Drawing.Point(323, 82);
            this.pnlEmployeeCode.Name = "pnlEmployeeCode";
            this.pnlEmployeeCode.Size = new System.Drawing.Size(232, 1);
            this.pnlEmployeeCode.TabIndex = 21;
            // 
            // txtPeekTubeCode
            // 
            this.txtPeekTubeCode.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPeekTubeCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPeekTubeCode.Font = new System.Drawing.Font("Sitka Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPeekTubeCode.Location = new System.Drawing.Point(11, 56);
            this.txtPeekTubeCode.Name = "txtPeekTubeCode";
            this.txtPeekTubeCode.Size = new System.Drawing.Size(232, 21);
            this.txtPeekTubeCode.TabIndex = 22;
            // 
            // lblPeekTubeCode
            // 
            this.lblPeekTubeCode.AutoSize = true;
            this.lblPeekTubeCode.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeekTubeCode.Location = new System.Drawing.Point(7, 17);
            this.lblPeekTubeCode.Name = "lblPeekTubeCode";
            this.lblPeekTubeCode.Size = new System.Drawing.Size(49, 28);
            this.lblPeekTubeCode.TabIndex = 17;
            this.lblPeekTubeCode.Text = "Code";
            // 
            // txtEmployeeCode
            // 
            this.txtEmployeeCode.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtEmployeeCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmployeeCode.Font = new System.Drawing.Font("Sitka Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmployeeCode.Location = new System.Drawing.Point(323, 56);
            this.txtEmployeeCode.Name = "txtEmployeeCode";
            this.txtEmployeeCode.Size = new System.Drawing.Size(232, 21);
            this.txtEmployeeCode.TabIndex = 19;
            // 
            // lblEmployeeCode
            // 
            this.lblEmployeeCode.AutoSize = true;
            this.lblEmployeeCode.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeCode.Location = new System.Drawing.Point(322, 17);
            this.lblEmployeeCode.Name = "lblEmployeeCode";
            this.lblEmployeeCode.Size = new System.Drawing.Size(131, 28);
            this.lblEmployeeCode.TabIndex = 18;
            this.lblEmployeeCode.Text = "Code d\'employé";
            // 
            // pnlR_PeekTube_25
            // 
            this.pnlR_PeekTube_25.BackColor = System.Drawing.Color.Black;
            this.pnlR_PeekTube_25.Location = new System.Drawing.Point(8, 241);
            this.pnlR_PeekTube_25.Name = "pnlR_PeekTube_25";
            this.pnlR_PeekTube_25.Size = new System.Drawing.Size(232, 1);
            this.pnlR_PeekTube_25.TabIndex = 31;
            // 
            // txtR_PeekTube_25
            // 
            this.txtR_PeekTube_25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtR_PeekTube_25.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR_PeekTube_25.Font = new System.Drawing.Font("Sitka Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR_PeekTube_25.Location = new System.Drawing.Point(8, 215);
            this.txtR_PeekTube_25.Name = "txtR_PeekTube_25";
            this.txtR_PeekTube_25.Size = new System.Drawing.Size(232, 21);
            this.txtR_PeekTube_25.TabIndex = 30;
            this.txtR_PeekTube_25.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtR_PeekTube_25_KeyPress);
            // 
            // lblR_PeekTube_25
            // 
            this.lblR_PeekTube_25.AutoSize = true;
            this.lblR_PeekTube_25.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_PeekTube_25.Location = new System.Drawing.Point(7, 176);
            this.lblR_PeekTube_25.Name = "lblR_PeekTube_25";
            this.lblR_PeekTube_25.Size = new System.Drawing.Size(152, 28);
            this.lblR_PeekTube_25.TabIndex = 29;
            this.lblR_PeekTube_25.Text = "Résistance à 25 °C";
            // 
            // FrmCreatePeekTube
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(589, 357);
            this.Controls.Add(this.pnlR_PeekTube_25);
            this.Controls.Add(this.txtR_PeekTube_25);
            this.Controls.Add(this.lblR_PeekTube_25);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.pnlPeekTubeColor);
            this.Controls.Add(this.txtPeekTubeColor);
            this.Controls.Add(this.lblPeekTubeColor);
            this.Controls.Add(this.pnlPeekTubeCode);
            this.Controls.Add(this.pnlEmployeeCode);
            this.Controls.Add(this.txtPeekTubeCode);
            this.Controls.Add(this.lblPeekTubeCode);
            this.Controls.Add(this.txtEmployeeCode);
            this.Controls.Add(this.lblEmployeeCode);
            this.Name = "FrmCreatePeekTube";
            this.Text = "Ajout d\'un PeekTube";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmCreatePeekTube_FormClosed);
            this.Load += new System.EventHandler(this.FrmCreatePeekTube_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Panel pnlPeekTubeColor;
        private System.Windows.Forms.TextBox txtPeekTubeColor;
        private System.Windows.Forms.Label lblPeekTubeColor;
        private System.Windows.Forms.Panel pnlPeekTubeCode;
        private System.Windows.Forms.Panel pnlEmployeeCode;
        private System.Windows.Forms.TextBox txtPeekTubeCode;
        private System.Windows.Forms.Label lblPeekTubeCode;
        private System.Windows.Forms.TextBox txtEmployeeCode;
        private System.Windows.Forms.Label lblEmployeeCode;
        private System.Windows.Forms.Panel pnlR_PeekTube_25;
        private System.Windows.Forms.TextBox txtR_PeekTube_25;
        private System.Windows.Forms.Label lblR_PeekTube_25;
    }
}