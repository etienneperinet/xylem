﻿
namespace Xylem
{
    partial class FrmCreateMeasurement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSegmentDiameter1 = new System.Windows.Forms.Label();
            this.txtSegmentDiameter1 = new System.Windows.Forms.TextBox();
            this.txtSegmentDiameter2 = new System.Windows.Forms.TextBox();
            this.lblSegmentDiameter2 = new System.Windows.Forms.Label();
            this.txtSegmentLength = new System.Windows.Forms.TextBox();
            this.lblSegmentLength = new System.Windows.Forms.Label();
            this.txtPeekTubeGamme = new System.Windows.Forms.TextBox();
            this.lblPeekTubeGamme = new System.Windows.Forms.Label();
            this.lblWaterHeight = new System.Windows.Forms.Label();
            this.cboWaterHeight = new System.Windows.Forms.ComboBox();
            this.lblPeektube = new System.Windows.Forms.Label();
            this.cboPeektube = new System.Windows.Forms.ComboBox();
            this.lblSolutionPerfusion = new System.Windows.Forms.Label();
            this.cboSolutionPerfusion = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSampleCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnContinue = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboKI_KMAX = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtKMAXPression = new System.Windows.Forms.TextBox();
            this.cboKMAXMethod = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKMAXDuration = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtKMAXIteration = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSegmentDiameter1
            // 
            this.lblSegmentDiameter1.AutoSize = true;
            this.lblSegmentDiameter1.Location = new System.Drawing.Point(15, 76);
            this.lblSegmentDiameter1.Name = "lblSegmentDiameter1";
            this.lblSegmentDiameter1.Size = new System.Drawing.Size(143, 28);
            this.lblSegmentDiameter1.TabIndex = 0;
            this.lblSegmentDiameter1.Text = "Diamètre 1 (mm)";
            // 
            // txtSegmentDiameter1
            // 
            this.txtSegmentDiameter1.Location = new System.Drawing.Point(15, 112);
            this.txtSegmentDiameter1.Name = "txtSegmentDiameter1";
            this.txtSegmentDiameter1.Size = new System.Drawing.Size(258, 31);
            this.txtSegmentDiameter1.TabIndex = 1;
            this.txtSegmentDiameter1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSegmentDiameter1_KeyPress);
            // 
            // txtSegmentDiameter2
            // 
            this.txtSegmentDiameter2.Location = new System.Drawing.Point(15, 213);
            this.txtSegmentDiameter2.Name = "txtSegmentDiameter2";
            this.txtSegmentDiameter2.Size = new System.Drawing.Size(258, 31);
            this.txtSegmentDiameter2.TabIndex = 3;
            this.txtSegmentDiameter2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSegmentDiameter2_KeyPress);
            // 
            // lblSegmentDiameter2
            // 
            this.lblSegmentDiameter2.AutoSize = true;
            this.lblSegmentDiameter2.Location = new System.Drawing.Point(15, 177);
            this.lblSegmentDiameter2.Name = "lblSegmentDiameter2";
            this.lblSegmentDiameter2.Size = new System.Drawing.Size(145, 28);
            this.lblSegmentDiameter2.TabIndex = 2;
            this.lblSegmentDiameter2.Text = "Diamètre 2 (mm)";
            // 
            // txtSegmentLength
            // 
            this.txtSegmentLength.Location = new System.Drawing.Point(15, 320);
            this.txtSegmentLength.Name = "txtSegmentLength";
            this.txtSegmentLength.Size = new System.Drawing.Size(258, 31);
            this.txtSegmentLength.TabIndex = 5;
            this.txtSegmentLength.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSegmentLength_KeyPress);
            // 
            // lblSegmentLength
            // 
            this.lblSegmentLength.AutoSize = true;
            this.lblSegmentLength.Location = new System.Drawing.Point(15, 284);
            this.lblSegmentLength.Name = "lblSegmentLength";
            this.lblSegmentLength.Size = new System.Drawing.Size(134, 28);
            this.lblSegmentLength.TabIndex = 4;
            this.lblSegmentLength.Text = "Longueur (mm)";
            // 
            // txtPeekTubeGamme
            // 
            this.txtPeekTubeGamme.Location = new System.Drawing.Point(790, 105);
            this.txtPeekTubeGamme.Name = "txtPeekTubeGamme";
            this.txtPeekTubeGamme.Size = new System.Drawing.Size(258, 31);
            this.txtPeekTubeGamme.TabIndex = 7;
            this.txtPeekTubeGamme.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPeekTubeGamme_KeyPress);
            // 
            // lblPeekTubeGamme
            // 
            this.lblPeekTubeGamme.AutoSize = true;
            this.lblPeekTubeGamme.Location = new System.Drawing.Point(790, 69);
            this.lblPeekTubeGamme.Name = "lblPeekTubeGamme";
            this.lblPeekTubeGamme.Size = new System.Drawing.Size(178, 28);
            this.lblPeekTubeGamme.TabIndex = 6;
            this.lblPeekTubeGamme.Text = "Gamme du Peek Tube";
            // 
            // lblWaterHeight
            // 
            this.lblWaterHeight.AutoSize = true;
            this.lblWaterHeight.Location = new System.Drawing.Point(790, 170);
            this.lblWaterHeight.Name = "lblWaterHeight";
            this.lblWaterHeight.Size = new System.Drawing.Size(180, 28);
            this.lblWaterHeight.TabIndex = 8;
            this.lblWaterHeight.Text = "Hauteur de l\'eau (cm)";
            // 
            // cboWaterHeight
            // 
            this.cboWaterHeight.FormattingEnabled = true;
            this.cboWaterHeight.Location = new System.Drawing.Point(790, 206);
            this.cboWaterHeight.Name = "cboWaterHeight";
            this.cboWaterHeight.Size = new System.Drawing.Size(258, 34);
            this.cboWaterHeight.TabIndex = 9;
            // 
            // lblPeektube
            // 
            this.lblPeektube.AutoSize = true;
            this.lblPeektube.Location = new System.Drawing.Point(791, 270);
            this.lblPeektube.Name = "lblPeektube";
            this.lblPeektube.Size = new System.Drawing.Size(85, 28);
            this.lblPeektube.TabIndex = 10;
            this.lblPeektube.Text = "PeekTube";
            // 
            // cboPeektube
            // 
            this.cboPeektube.FormattingEnabled = true;
            this.cboPeektube.Location = new System.Drawing.Point(790, 306);
            this.cboPeektube.Name = "cboPeektube";
            this.cboPeektube.Size = new System.Drawing.Size(258, 34);
            this.cboPeektube.TabIndex = 11;
            // 
            // lblSolutionPerfusion
            // 
            this.lblSolutionPerfusion.AutoSize = true;
            this.lblSolutionPerfusion.Location = new System.Drawing.Point(791, 368);
            this.lblSolutionPerfusion.Name = "lblSolutionPerfusion";
            this.lblSolutionPerfusion.Size = new System.Drawing.Size(174, 28);
            this.lblSolutionPerfusion.TabIndex = 12;
            this.lblSolutionPerfusion.Text = "Solution de perfusion";
            // 
            // cboSolutionPerfusion
            // 
            this.cboSolutionPerfusion.FormattingEnabled = true;
            this.cboSolutionPerfusion.Location = new System.Drawing.Point(790, 406);
            this.cboSolutionPerfusion.Name = "cboSolutionPerfusion";
            this.cboSolutionPerfusion.Size = new System.Drawing.Size(258, 34);
            this.cboSolutionPerfusion.TabIndex = 13;
            this.cboSolutionPerfusion.Leave += new System.EventHandler(this.cboSolutionPerfusion_Leave);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtSampleCode);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtSegmentDiameter1);
            this.panel1.Controls.Add(this.lblSegmentDiameter1);
            this.panel1.Controls.Add(this.lblSegmentDiameter2);
            this.panel1.Controls.Add(this.txtSegmentDiameter2);
            this.panel1.Controls.Add(this.lblSegmentLength);
            this.panel1.Controls.Add(this.txtSegmentLength);
            this.panel1.Location = new System.Drawing.Point(413, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(292, 506);
            this.panel1.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 396);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 28);
            this.label7.TabIndex = 7;
            this.label7.Text = "Code";
            // 
            // txtSampleCode
            // 
            this.txtSampleCode.Location = new System.Drawing.Point(15, 435);
            this.txtSampleCode.Name = "txtSampleCode";
            this.txtSampleCode.Size = new System.Drawing.Size(258, 31);
            this.txtSampleCode.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Sitka Display", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 32);
            this.label1.TabIndex = 6;
            this.label1.Text = "Échantillon";
            // 
            // btnContinue
            // 
            this.btnContinue.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btnContinue.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnContinue.FlatAppearance.BorderSize = 0;
            this.btnContinue.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnContinue.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.btnContinue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContinue.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContinue.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnContinue.Location = new System.Drawing.Point(889, 484);
            this.btnContinue.Margin = new System.Windows.Forms.Padding(4);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(160, 58);
            this.btnContinue.TabIndex = 28;
            this.btnContinue.Text = "Continuer";
            this.btnContinue.UseVisualStyleBackColor = false;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click_1);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cboKI_KMAX);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtKMAXPression);
            this.panel2.Controls.Add(this.cboKMAXMethod);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtKMAXDuration);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtKMAXIteration);
            this.panel2.Location = new System.Drawing.Point(41, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(311, 518);
            this.panel2.TabIndex = 15;
            // 
            // cboKI_KMAX
            // 
            this.cboKI_KMAX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKI_KMAX.FormattingEnabled = true;
            this.cboKI_KMAX.Location = new System.Drawing.Point(15, 83);
            this.cboKI_KMAX.Name = "cboKI_KMAX";
            this.cboKI_KMAX.Size = new System.Drawing.Size(258, 34);
            this.cboKI_KMAX.TabIndex = 9;
            this.cboKI_KMAX.SelectedIndexChanged += new System.EventHandler(this.cboKI_KMAX_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 344);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 28);
            this.label6.TabIndex = 7;
            this.label6.Text = "Pression";
            // 
            // txtKMAXPression
            // 
            this.txtKMAXPression.Location = new System.Drawing.Point(15, 380);
            this.txtKMAXPression.Name = "txtKMAXPression";
            this.txtKMAXPression.Size = new System.Drawing.Size(258, 31);
            this.txtKMAXPression.TabIndex = 8;
            this.txtKMAXPression.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKMAXPression_KeyPress);
            // 
            // cboKMAXMethod
            // 
            this.cboKMAXMethod.FormattingEnabled = true;
            this.cboKMAXMethod.Location = new System.Drawing.Point(14, 471);
            this.cboKMAXMethod.Name = "cboKMAXMethod";
            this.cboKMAXMethod.Size = new System.Drawing.Size(258, 34);
            this.cboKMAXMethod.TabIndex = 13;
            this.cboKMAXMethod.Leave += new System.EventHandler(this.cboKMAXMethod_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 435);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 28);
            this.label3.TabIndex = 12;
            this.label3.Text = "Méthode KMAX";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Sitka Display", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 32);
            this.label2.TabIndex = 6;
            this.label2.Text = "KI / KMAX";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 28);
            this.label4.TabIndex = 2;
            this.label4.Text = "Durée (min)";
            // 
            // txtKMAXDuration
            // 
            this.txtKMAXDuration.Location = new System.Drawing.Point(15, 175);
            this.txtKMAXDuration.Name = "txtKMAXDuration";
            this.txtKMAXDuration.Size = new System.Drawing.Size(258, 31);
            this.txtKMAXDuration.TabIndex = 3;
            this.txtKMAXDuration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKMAXDuration_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 28);
            this.label5.TabIndex = 4;
            this.label5.Text = "Itération";
            // 
            // txtKMAXIteration
            // 
            this.txtKMAXIteration.AccessibleRole = System.Windows.Forms.AccessibleRole.SplitButton;
            this.txtKMAXIteration.Location = new System.Drawing.Point(15, 282);
            this.txtKMAXIteration.Name = "txtKMAXIteration";
            this.txtKMAXIteration.Size = new System.Drawing.Size(258, 31);
            this.txtKMAXIteration.TabIndex = 5;
            this.txtKMAXIteration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKMAXIteration_KeyPress);
            // 
            // FrmCreateMeasurement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1136, 555);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cboSolutionPerfusion);
            this.Controls.Add(this.lblSolutionPerfusion);
            this.Controls.Add(this.cboPeektube);
            this.Controls.Add(this.lblPeektube);
            this.Controls.Add(this.cboWaterHeight);
            this.Controls.Add(this.lblWaterHeight);
            this.Controls.Add(this.txtPeekTubeGamme);
            this.Controls.Add(this.lblPeekTubeGamme);
            this.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "FrmCreateMeasurement";
            this.Text = "FrmCreateMeasurement";
            this.Load += new System.EventHandler(this.FrmCreateMeasurement_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSegmentDiameter1;
        private System.Windows.Forms.TextBox txtSegmentDiameter1;
        private System.Windows.Forms.TextBox txtSegmentDiameter2;
        private System.Windows.Forms.Label lblSegmentDiameter2;
        private System.Windows.Forms.TextBox txtSegmentLength;
        private System.Windows.Forms.Label lblSegmentLength;
        private System.Windows.Forms.TextBox txtPeekTubeGamme;
        private System.Windows.Forms.Label lblPeekTubeGamme;
        private System.Windows.Forms.Label lblWaterHeight;
        private System.Windows.Forms.ComboBox cboWaterHeight;
        private System.Windows.Forms.Label lblPeektube;
        private System.Windows.Forms.ComboBox cboPeektube;
        private System.Windows.Forms.Label lblSolutionPerfusion;
        private System.Windows.Forms.ComboBox cboSolutionPerfusion;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtKMAXDuration;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtKMAXIteration;
        private System.Windows.Forms.ComboBox cboKI_KMAX;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtKMAXPression;
        private System.Windows.Forms.ComboBox cboKMAXMethod;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSampleCode;
    }
}