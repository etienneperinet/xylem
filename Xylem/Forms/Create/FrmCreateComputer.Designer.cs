﻿
namespace Xylem
{
    partial class FrmCreateComputer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnContinue = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.pnlEmployeeCode = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.txtEmployeeCode = new System.Windows.Forms.TextBox();
			this.lblComputer = new System.Windows.Forms.Label();
			this.comboComputerType = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// btnContinue
			// 
			this.btnContinue.BackColor = System.Drawing.Color.LightSkyBlue;
			this.btnContinue.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
			this.btnContinue.FlatAppearance.BorderSize = 0;
			this.btnContinue.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue;
			this.btnContinue.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
			this.btnContinue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnContinue.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnContinue.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.btnContinue.Location = new System.Drawing.Point(300, 481);
			this.btnContinue.Margin = new System.Windows.Forms.Padding(4);
			this.btnContinue.Name = "btnContinue";
			this.btnContinue.Size = new System.Drawing.Size(160, 58);
			this.btnContinue.TabIndex = 27;
			this.btnContinue.Text = "Continuer";
			this.btnContinue.UseVisualStyleBackColor = false;
			this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Black;
			this.panel1.Location = new System.Drawing.Point(11, 221);
			this.panel1.Margin = new System.Windows.Forms.Padding(4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(448, 1);
			this.panel1.TabIndex = 21;
			// 
			// pnlEmployeeCode
			// 
			this.pnlEmployeeCode.BackColor = System.Drawing.Color.Black;
			this.pnlEmployeeCode.Location = new System.Drawing.Point(11, 103);
			this.pnlEmployeeCode.Margin = new System.Windows.Forms.Padding(4);
			this.pnlEmployeeCode.Name = "pnlEmployeeCode";
			this.pnlEmployeeCode.Size = new System.Drawing.Size(448, 1);
			this.pnlEmployeeCode.TabIndex = 22;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(15, 127);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(183, 35);
			this.label1.TabIndex = 17;
			this.label1.Text = "Type d\'ordinateur";
			// 
			// txtEmployeeCode
			// 
			this.txtEmployeeCode.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.txtEmployeeCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtEmployeeCode.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtEmployeeCode.Location = new System.Drawing.Point(11, 69);
			this.txtEmployeeCode.Margin = new System.Windows.Forms.Padding(4);
			this.txtEmployeeCode.Name = "txtEmployeeCode";
			this.txtEmployeeCode.Size = new System.Drawing.Size(448, 29);
			this.txtEmployeeCode.TabIndex = 20;
			this.txtEmployeeCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmployeeCode_KeyPress);
			// 
			// lblComputer
			// 
			this.lblComputer.AutoSize = true;
			this.lblComputer.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblComputer.Location = new System.Drawing.Point(9, 21);
			this.lblComputer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblComputer.Name = "lblComputer";
			this.lblComputer.Size = new System.Drawing.Size(215, 35);
			this.lblComputer.TabIndex = 18;
			this.lblComputer.Text = "Numéro d\'ordinateur";
			// 
			// comboComputerType
			// 
			this.comboComputerType.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.comboComputerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboComputerType.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.comboComputerType.FormattingEnabled = true;
			this.comboComputerType.Location = new System.Drawing.Point(12, 174);
			this.comboComputerType.Margin = new System.Windows.Forms.Padding(4);
			this.comboComputerType.Name = "comboComputerType";
			this.comboComputerType.Size = new System.Drawing.Size(447, 41);
			this.comboComputerType.TabIndex = 28;
			// 
			// FrmCreateComputer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.ClientSize = new System.Drawing.Size(479, 554);
			this.Controls.Add(this.comboComputerType);
			this.Controls.Add(this.btnContinue);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.pnlEmployeeCode);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtEmployeeCode);
			this.Controls.Add(this.lblComputer);
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "FrmCreateComputer";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Ajout d\'ordinateur";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmCreateComputer_FormClosed);
			this.Load += new System.EventHandler(this.FrmCreateComputer_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlEmployeeCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmployeeCode;
        private System.Windows.Forms.Label lblComputer;
        private System.Windows.Forms.ComboBox comboComputerType;
    }
}