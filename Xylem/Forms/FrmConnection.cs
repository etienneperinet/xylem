﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
    public partial class FrmConnection : Form
    {
        DatabaseController databaseController;
        //Session à la palce
        Operator CurrentOperator { get; set; }
        Computer CurrentComputer { get; set; }

        public FrmConnection()
        {
            InitializeComponent();
            databaseController = new DatabaseController();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            if (!CheckEmptyTextBox())
            {
                GetAllNeededObject();
                if (askToAddInformation())
                {
                    askToCreateObjectInformation();
                }
            }
        }

        private bool CheckEmptyTextBox()
        {
            bool isOneTextBoxEmpty = (txtArduinoId.Text == "" || txtComputerId.Text == "" || txtEmployeeCode.Text == "");
            if (isOneTextBoxEmpty)
            {
                MessageBox.Show("Veuillez remplir toutes les zones de texte");
            }
            return isOneTextBoxEmpty;
        }

        private void GetAllNeededObject()
        {
            CurrentComputer = databaseController.GetComputer(Convert.ToInt32(txtComputerId.Text));
            CurrentOperator = databaseController.GetOperator(txtEmployeeCode.Text);
        }

        private string getMessageToDisplayToUser()
        {
            string dialogMessage = "Certaines informations que vous avez donné ne sont pas connu tel que: ";
            if (CurrentOperator == null)
            {
                dialogMessage += "\n - Code d'employé";
            }
            if (CurrentComputer == null)
            {
                dialogMessage += "\n - Numéro d'ordinateur";
            }
            if ((!databaseController.ArduinoExists(Convert.ToInt32(txtArduinoId.Text))))
            {
                dialogMessage += "\n - Numéro de montage Xylem";
            }
            dialogMessage += "\n\n Voulez-vous les créer?";
            return dialogMessage;
        }

        private bool askToAddInformation()
        {
            bool askToCreateObject = false;
            string dialogMessage = getMessageToDisplayToUser();
            if (dialogMessage != "Certaines informations que vous avez donné ne sont pas connu tel que: " + "\n\n Voulez-vous les créer?")
            {
                DialogResult dialogResult = MessageBox.Show(dialogMessage, "Information inconnu", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    askToCreateObject = true;
                }
            }
            else
            {
                ContinueToRecording();
            }
            return askToCreateObject;
        }

        private void askToCreateObjectInformation()
        {
            if (!databaseController.ArduinoExists(Convert.ToInt32(txtArduinoId.Text)))
            {
                databaseController.AddArduino(Convert.ToInt32(txtArduinoId.Text));
                askToCreateObjectInformation();
            }
            else if (CurrentOperator == null)
            {
                CreateOperator();
            }
            else if (CurrentComputer == null)
            {
                CreateComputer();
            }
            else
            {
                ContinueToRecording();
            }
            
        }

        private void CreateOperator()
        {
            using (var form = new FrmCreateOperator(txtEmployeeCode.Text))
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    CurrentOperator = form.NewOperator;
                    txtEmployeeCode.Text = CurrentOperator.EmployeeCode;
                    askToCreateObjectInformation();
                }
            }
        }

        private void CreateArduino()
        {
            DialogResult dialogResult = MessageBox.Show("Ce numéro de montage Xylem n'existe pas. Voulez-vous le créer?", "Creation d'Arduino", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                databaseController.AddArduino(Convert.ToInt32(txtArduinoId.Text));
                askToCreateObjectInformation();
            }
        }

        private void CreateComputer()
        {
            using (var form = new FrmCreateComputer(txtComputerId.Text))
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    CurrentComputer = form.NewComputer;
                    txtComputerId.Text = CurrentComputer.ID.ToString();
                    askToCreateObjectInformation();
                } 
            }
        }

        private void ContinueToRecording()
        {
            Session.SetInstance(
                    Convert.ToInt32(txtArduinoId.Text),
                    CurrentComputer,
                    CurrentOperator);

            databaseController.AddSession(Session.GetInstance());



            this.Hide();
            using (var form = new FrmRecordCalibration())
            {
                form.ShowDialog();
                
            }
            this.Show();
        }

        private void FrmConnection_Load(object sender, EventArgs e)
        {
            btnLogIn.TabStop = false;
            AcceptButton = btnLogIn;
        }

        private void txtComputerId_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtArduinoId_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
