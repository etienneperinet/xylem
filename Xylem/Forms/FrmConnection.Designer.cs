﻿
namespace Xylem
{
    partial class FrmConnection
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblEmployeeCode = new System.Windows.Forms.Label();
			this.txtEmployeeCode = new System.Windows.Forms.TextBox();
			this.lblComputerId = new System.Windows.Forms.Label();
			this.txtComputerId = new System.Windows.Forms.TextBox();
			this.lblArduinoId = new System.Windows.Forms.Label();
			this.txtArduinoId = new System.Windows.Forms.TextBox();
			this.btnLogIn = new System.Windows.Forms.Button();
			this.pnlEmployeeCode = new System.Windows.Forms.Panel();
			this.pnlComputerId = new System.Windows.Forms.Panel();
			this.pnlArduinoId = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// lblEmployeeCode
			// 
			this.lblEmployeeCode.AutoSize = true;
			this.lblEmployeeCode.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblEmployeeCode.Location = new System.Drawing.Point(824, 101);
			this.lblEmployeeCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblEmployeeCode.Name = "lblEmployeeCode";
			this.lblEmployeeCode.Size = new System.Drawing.Size(164, 35);
			this.lblEmployeeCode.TabIndex = 0;
			this.lblEmployeeCode.Text = "Code d\'employé";
			// 
			// txtEmployeeCode
			// 
			this.txtEmployeeCode.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.txtEmployeeCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtEmployeeCode.Font = new System.Drawing.Font("Sitka Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtEmployeeCode.Location = new System.Drawing.Point(825, 149);
			this.txtEmployeeCode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtEmployeeCode.Name = "txtEmployeeCode";
			this.txtEmployeeCode.Size = new System.Drawing.Size(281, 26);
			this.txtEmployeeCode.TabIndex = 1;
			// 
			// lblComputerId
			// 
			this.lblComputerId.AutoSize = true;
			this.lblComputerId.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblComputerId.Location = new System.Drawing.Point(824, 241);
			this.lblComputerId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblComputerId.Name = "lblComputerId";
			this.lblComputerId.Size = new System.Drawing.Size(215, 35);
			this.lblComputerId.TabIndex = 2;
			this.lblComputerId.Text = "Numéro d\'ordinateur";
			// 
			// txtComputerId
			// 
			this.txtComputerId.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.txtComputerId.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtComputerId.Font = new System.Drawing.Font("Sitka Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtComputerId.Location = new System.Drawing.Point(825, 289);
			this.txtComputerId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtComputerId.Name = "txtComputerId";
			this.txtComputerId.Size = new System.Drawing.Size(281, 26);
			this.txtComputerId.TabIndex = 3;
			this.txtComputerId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtComputerId_KeyPress);
			// 
			// lblArduinoId
			// 
			this.lblArduinoId.AutoSize = true;
			this.lblArduinoId.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblArduinoId.Location = new System.Drawing.Point(824, 388);
			this.lblArduinoId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblArduinoId.Name = "lblArduinoId";
			this.lblArduinoId.Size = new System.Drawing.Size(189, 35);
			this.lblArduinoId.TabIndex = 4;
			this.lblArduinoId.Text = "Numéro d\'Arduino";
			// 
			// txtArduinoId
			// 
			this.txtArduinoId.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.txtArduinoId.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtArduinoId.Font = new System.Drawing.Font("Sitka Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtArduinoId.Location = new System.Drawing.Point(825, 436);
			this.txtArduinoId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtArduinoId.Name = "txtArduinoId";
			this.txtArduinoId.Size = new System.Drawing.Size(281, 26);
			this.txtArduinoId.TabIndex = 5;
			this.txtArduinoId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtArduinoId_KeyPress);
			// 
			// btnLogIn
			// 
			this.btnLogIn.BackColor = System.Drawing.Color.LightSkyBlue;
			this.btnLogIn.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
			this.btnLogIn.FlatAppearance.BorderSize = 0;
			this.btnLogIn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue;
			this.btnLogIn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
			this.btnLogIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLogIn.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnLogIn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.btnLogIn.Location = new System.Drawing.Point(867, 564);
			this.btnLogIn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.btnLogIn.Name = "btnLogIn";
			this.btnLogIn.Size = new System.Drawing.Size(240, 58);
			this.btnLogIn.TabIndex = 6;
			this.btnLogIn.Text = "Se connecter";
			this.btnLogIn.UseVisualStyleBackColor = false;
			this.btnLogIn.Click += new System.EventHandler(this.btnLogIn_Click);
			// 
			// pnlEmployeeCode
			// 
			this.pnlEmployeeCode.BackColor = System.Drawing.Color.Black;
			this.pnlEmployeeCode.Location = new System.Drawing.Point(825, 181);
			this.pnlEmployeeCode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pnlEmployeeCode.Name = "pnlEmployeeCode";
			this.pnlEmployeeCode.Size = new System.Drawing.Size(281, 1);
			this.pnlEmployeeCode.TabIndex = 7;
			// 
			// pnlComputerId
			// 
			this.pnlComputerId.BackColor = System.Drawing.Color.Black;
			this.pnlComputerId.Location = new System.Drawing.Point(825, 321);
			this.pnlComputerId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pnlComputerId.Name = "pnlComputerId";
			this.pnlComputerId.Size = new System.Drawing.Size(281, 1);
			this.pnlComputerId.TabIndex = 8;
			// 
			// pnlArduinoId
			// 
			this.pnlArduinoId.BackColor = System.Drawing.Color.Black;
			this.pnlArduinoId.Location = new System.Drawing.Point(825, 468);
			this.pnlArduinoId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pnlArduinoId.Name = "pnlArduinoId";
			this.pnlArduinoId.Size = new System.Drawing.Size(281, 1);
			this.pnlArduinoId.TabIndex = 9;
			// 
			// FrmConnection
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.ClientSize = new System.Drawing.Size(1176, 681);
			this.Controls.Add(this.pnlArduinoId);
			this.Controls.Add(this.pnlComputerId);
			this.Controls.Add(this.pnlEmployeeCode);
			this.Controls.Add(this.btnLogIn);
			this.Controls.Add(this.txtArduinoId);
			this.Controls.Add(this.lblArduinoId);
			this.Controls.Add(this.txtComputerId);
			this.Controls.Add(this.lblComputerId);
			this.Controls.Add(this.txtEmployeeCode);
			this.Controls.Add(this.lblEmployeeCode);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.MaximizeBox = false;
			this.Name = "FrmConnection";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Connexion";
			this.Load += new System.EventHandler(this.FrmConnection_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEmployeeCode;
        private System.Windows.Forms.TextBox txtEmployeeCode;
        private System.Windows.Forms.Label lblComputerId;
        private System.Windows.Forms.TextBox txtComputerId;
        private System.Windows.Forms.Label lblArduinoId;
        private System.Windows.Forms.TextBox txtArduinoId;
        private System.Windows.Forms.Button btnLogIn;
        private System.Windows.Forms.Panel pnlEmployeeCode;
        private System.Windows.Forms.Panel pnlComputerId;
        private System.Windows.Forms.Panel pnlArduinoId;
    }
}

