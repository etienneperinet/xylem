﻿
namespace Xylem
{
    partial class FrmRecordMeasurement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.dgvMeasurement = new System.Windows.Forms.DataGridView();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Step = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sensor1Pressure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sensor2Pressure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sensor1Temperature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sensor2Temperature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndTemperature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Show = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lblFormulaResults = new System.Windows.Forms.Label();
            this.srlprtArduino = new System.IO.Ports.SerialPort(this.components);
            this.chartBar = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMeasurement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartBar)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMeasurement
            // 
            this.dgvMeasurement.AllowUserToAddRows = false;
            this.dgvMeasurement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMeasurement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Time,
            this.Step,
            this.Sensor1Pressure,
            this.Sensor2Pressure,
            this.Sensor1Temperature,
            this.Sensor2Temperature,
            this.EndTemperature,
            this.ID,
            this.Show});
            this.dgvMeasurement.Location = new System.Drawing.Point(778, 34);
            this.dgvMeasurement.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.dgvMeasurement.Name = "dgvMeasurement";
            this.dgvMeasurement.RowHeadersWidth = 51;
            this.dgvMeasurement.RowTemplate.Height = 42;
            this.dgvMeasurement.Size = new System.Drawing.Size(736, 772);
            this.dgvMeasurement.TabIndex = 25;
            this.dgvMeasurement.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMeasurement_CellValueChanged);
            this.dgvMeasurement.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvMeasurement_UserDeletingRow);
            // 
            // Time
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Time.DefaultCellStyle = dataGridViewCellStyle1;
            this.Time.HeaderText = "Time";
            this.Time.MinimumWidth = 6;
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.Width = 60;
            // 
            // Step
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Step.DefaultCellStyle = dataGridViewCellStyle2;
            this.Step.HeaderText = "Step";
            this.Step.MinimumWidth = 6;
            this.Step.Name = "Step";
            this.Step.Width = 50;
            // 
            // Sensor1Pressure
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Sensor1Pressure.DefaultCellStyle = dataGridViewCellStyle3;
            this.Sensor1Pressure.HeaderText = "S1P";
            this.Sensor1Pressure.MinimumWidth = 6;
            this.Sensor1Pressure.Name = "Sensor1Pressure";
            this.Sensor1Pressure.ReadOnly = true;
            this.Sensor1Pressure.Width = 90;
            // 
            // Sensor2Pressure
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Sensor2Pressure.DefaultCellStyle = dataGridViewCellStyle4;
            this.Sensor2Pressure.HeaderText = "S2P";
            this.Sensor2Pressure.MinimumWidth = 6;
            this.Sensor2Pressure.Name = "Sensor2Pressure";
            this.Sensor2Pressure.ReadOnly = true;
            this.Sensor2Pressure.Width = 90;
            // 
            // Sensor1Temperature
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Sensor1Temperature.DefaultCellStyle = dataGridViewCellStyle5;
            this.Sensor1Temperature.HeaderText = "S1T";
            this.Sensor1Temperature.MinimumWidth = 6;
            this.Sensor1Temperature.Name = "Sensor1Temperature";
            this.Sensor1Temperature.ReadOnly = true;
            this.Sensor1Temperature.Width = 90;
            // 
            // Sensor2Temperature
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Sensor2Temperature.DefaultCellStyle = dataGridViewCellStyle6;
            this.Sensor2Temperature.HeaderText = "S2T";
            this.Sensor2Temperature.MinimumWidth = 6;
            this.Sensor2Temperature.Name = "Sensor2Temperature";
            this.Sensor2Temperature.ReadOnly = true;
            this.Sensor2Temperature.Width = 90;
            // 
            // EndTemperature
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.EndTemperature.DefaultCellStyle = dataGridViewCellStyle7;
            this.EndTemperature.HeaderText = "ET";
            this.EndTemperature.MinimumWidth = 6;
            this.EndTemperature.Name = "EndTemperature";
            this.EndTemperature.ReadOnly = true;
            this.EndTemperature.Width = 90;
            // 
            // ID
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ID.DefaultCellStyle = dataGridViewCellStyle8;
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            this.ID.Visible = false;
            this.ID.Width = 80;
            // 
            // Show
            // 
            this.Show.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.NullValue = false;
            this.Show.DefaultCellStyle = dataGridViewCellStyle9;
            this.Show.HeaderText = "Afficher";
            this.Show.MinimumWidth = 6;
            this.Show.Name = "Show";
            this.Show.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Show.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Show.Width = 97;
            // 
            // lblFormulaResults
            // 
            this.lblFormulaResults.AutoSize = true;
            this.lblFormulaResults.Location = new System.Drawing.Point(12, 17);
            this.lblFormulaResults.Name = "lblFormulaResults";
            this.lblFormulaResults.Size = new System.Drawing.Size(71, 28);
            this.lblFormulaResults.TabIndex = 28;
            this.lblFormulaResults.Text = "Results:";
            // 
            // srlprtArduino
            // 
            this.srlprtArduino.PortName = "COM3";
            this.srlprtArduino.RtsEnable = true;
            this.srlprtArduino.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.srlprtArduino_DataReceived);
            // 
            // chartBar
            // 
            chartArea1.AxisX.Interval = 100D;
            chartArea1.AxisX.Minimum = 0D;
            chartArea1.AxisX.Title = "Total elapsed time (Sec)";
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Sitka Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisY.Interval = 0.005D;
            chartArea1.AxisY.Minimum = 0D;
            chartArea1.AxisY.Title = "Pressure (Bar)";
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Sitka Display", 12F);
            chartArea1.Name = "ChartArea1";
            this.chartBar.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartBar.Legends.Add(legend1);
            this.chartBar.Location = new System.Drawing.Point(280, 50);
            this.chartBar.Name = "chartBar";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series1.Font = new System.Drawing.Font("Sitka Display", 12F);
            series1.Legend = "Legend1";
            series1.Name = "Sensor1";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series2.Font = new System.Drawing.Font("Sitka Display", 12F);
            series2.Legend = "Legend1";
            series2.Name = "Sensor2";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.chartBar.Series.Add(series1);
            this.chartBar.Series.Add(series2);
            this.chartBar.Size = new System.Drawing.Size(494, 254);
            this.chartBar.TabIndex = 29;
            this.chartBar.Text = "chartBar";
            title1.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title1.Name = "Step2Log";
            title1.Text = "View for Log of Step 2";
            this.chartBar.Titles.Add(title1);
            // 
            // FrmRecordMeasurement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1539, 806);
            this.Controls.Add(this.chartBar);
            this.Controls.Add(this.lblFormulaResults);
            this.Controls.Add(this.dgvMeasurement);
            this.Font = new System.Drawing.Font("Sitka Display", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.Name = "FrmRecordMeasurement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mesure";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmRecordMeasure_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMeasurement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvMeasurement;
		private System.Windows.Forms.Label lblFormulaResults;
		private System.IO.Ports.SerialPort srlprtArduino;
		private System.Windows.Forms.DataVisualization.Charting.Chart chartBar;
		private System.Windows.Forms.DataGridViewTextBoxColumn Time;
		private System.Windows.Forms.DataGridViewTextBoxColumn Step;
		private System.Windows.Forms.DataGridViewTextBoxColumn Sensor1Pressure;
		private System.Windows.Forms.DataGridViewTextBoxColumn Sensor2Pressure;
		private System.Windows.Forms.DataGridViewTextBoxColumn Sensor1Temperature;
		private System.Windows.Forms.DataGridViewTextBoxColumn Sensor2Temperature;
		private System.Windows.Forms.DataGridViewTextBoxColumn EndTemperature;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Show;
	}
}