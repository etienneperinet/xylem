﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Xylem
{
    public partial class FrmRecordMeasurement : Form
    {
        private SessionRecord sessionRecord;
        private DatabaseController databaseController;
        private List<ArduinoData> arduinoData = new List<ArduinoData>();

        delegate void testFormulasSerialCallback();
        delegate void serialCallback(ArduinoData arduinoData);

        private bool startingToChangeForm = false;

        //Test data
        List<ArduinoData> arduinoDataFormulaTest = new List<ArduinoData>();
        List<double> prUnitsSensor1 = new List<double>();
        List<double> barSensor1 = new List<double>();
        List<double> prUnitsSensor2 = new List<double>();
        List<double> barSensor2 = new List<double>();
        double slopeSensor1;
        double interceptSensor1;
        double slopeSensor2;
        double interceptSensor2;
        double peekTubeResistance;
        double stemDiameter;
        double pithDiameter;
        double stemLength;
        double leafArea;
        double minBar = 0.2;
        double maxBar = 0.8;

        public FrmRecordMeasurement()
        {
            InitializeComponent();
            databaseController = new DatabaseController();
            if (Session.GetInstance() == null)
            {
                databaseController.CreateSessionTest();
                sessionRecord = databaseController.CreateSessionRecordTest(Session.GetInstance());
            }
            else
            {
                sessionRecord = new SessionRecord(Session.GetInstance());
                sessionRecord.ID = databaseController.AddSessionRecord(sessionRecord);
            }
           /* SerialPortCommunicator.SerialPort.PortName = "COM3";
            SerialPortCommunicator.SerialPort.RtsEnable = true;
            SerialPortCommunicator.SerialPort.DtrEnable = true;*/
            SerialPortCommunicator.SerialPort.DataReceived += srlprtArduino_DataReceived;
        }

        private void FrmRecordMeasure_Load(object sender, EventArgs e)
        {
            UseRightPort();
            if (!srlprtArduino.IsOpen)
            {
                try
                {
                    srlprtArduino.Open();
                }
                catch (Exception exception)
                {
                    //MessageBox.Show(exception.Message);
                }
            }
            InitializeSlopeInterceptMorgan();
            InitializeOthersMorgan();
            ShowResults();
            //InitializeArduinoDataFormulaTestMorgan();
            //ShowTestFormulas();
            //UpdateChartTest();
        }

		#region Three

		private void InitializeOthers3()
        {
            peekTubeResistance = 0.193;
            stemDiameter = 3.23;
            pithDiameter = 0;
            stemLength = 41;
            leafArea = 1;
        }

        private void InitializeSlopeIntercept3()
        {
            prUnitsSensor1.Add(379);
            prUnitsSensor1.Add(1163);
            prUnitsSensor1.Add(1929);
            prUnitsSensor1.Add(2670);
            prUnitsSensor1.Add(3480);
            prUnitsSensor1.Add(4226);
            prUnitsSensor1.Add(5008);
            prUnitsSensor1.Add(5765);
            prUnitsSensor1.Add(6567);
            prUnitsSensor1.Add(7342);

            barSensor1.Add(0.005);
            barSensor1.Add(0.01);
            barSensor1.Add(0.015);
            barSensor1.Add(0.02);
            barSensor1.Add(0.025);
            barSensor1.Add(0.03);
            barSensor1.Add(0.035);
            barSensor1.Add(0.04);
            barSensor1.Add(0.045);
            barSensor1.Add(0.05);

            slopeSensor1 = FormulaController.GetSlope(prUnitsSensor1, barSensor1);
            interceptSensor1 = FormulaController.GetIntercept(prUnitsSensor1, barSensor1);

            prUnitsSensor2.Add(659);
            prUnitsSensor2.Add(1432);
            prUnitsSensor2.Add(2191);
            prUnitsSensor2.Add(2910);
            prUnitsSensor2.Add(3699);
            prUnitsSensor2.Add(4434);
            prUnitsSensor2.Add(5215);
            prUnitsSensor2.Add(5959);
            prUnitsSensor2.Add(6760);
            prUnitsSensor2.Add(7535);

            barSensor2.Add(0.005);
            barSensor2.Add(0.01);
            barSensor2.Add(0.015);
            barSensor2.Add(0.02);
            barSensor2.Add(0.025);
            barSensor2.Add(0.03);
            barSensor2.Add(0.035);
            barSensor2.Add(0.04);
            barSensor2.Add(0.045);
            barSensor2.Add(0.05);

            slopeSensor2 = FormulaController.GetSlope(prUnitsSensor2, barSensor2);
            interceptSensor2 = FormulaController.GetIntercept(prUnitsSensor2, barSensor2);
        }

        private void InitializeArduinoDataFormulaTest3()
        {
            arduinoDataFormulaTest.Add(new ArduinoData(39, 1, 7396, 7617, 21.12f, 21.31f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(45, 1, 7408, 7617, 21.12f, 21.31f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(50, 1, 7397, 7618, 21.12f, 21.31f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(56, 1, 7397, 7617, 21.12f, 21.31f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(61, 1, 7429, 7589, 21.19f, 21.31f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(67, 1, 7398, 7619, 21.12f, 21.31f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(72, 1, 7394, 7615, 21.19f, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(78, 1, 7400, 7612, 21.12f, 21.31f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(84, 1, 7400, 7616, 21.12f, 21.31f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(89, 1, 7398, 7617, 21.12f, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(354, 2, 7387, 2537, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(360, 2, 7388, 2584, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(365, 2, 7387, 2601, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(371, 2, 7385, 2601, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(377, 2, 7386, 2604, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(382, 2, 7387, 2610, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(388, 2, 7386, 2610, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(393, 2, 7384, 2618, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(399, 2, 7385, 2610, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(405, 2, 7385, 2606, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(410, 2, 7384, 2608, 20.94f, 21.25f, 18.31f));
            arduinoDataFormulaTest.Add(new ArduinoData(416, 2, 7383, 2610, 21, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(421, 2, 7384, 2600, 21, 21.19f, 18.31f));
            arduinoDataFormulaTest.Add(new ArduinoData(427, 2, 7384, 2595, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(432, 2, 7382, 2595, 20.94f, 21.25f, 18.31f));
            arduinoDataFormulaTest.Add(new ArduinoData(438, 2, 7384, 2591, 21, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(444, 2, 7383, 2600, 21, 21.19f, 18.31f));
            arduinoDataFormulaTest.Add(new ArduinoData(449, 2, 7382, 2591, 20.94f, 21.25f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(455, 2, 7384, 2578, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(460, 2, 7384, 2585, 21, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(466, 2, 7382, 2580, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(472, 2, 7381, 2581, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(477, 2, 7380, 2580, 20.94f, 21.19f, 18.31f));
            arduinoDataFormulaTest.Add(new ArduinoData(483, 2, 7380, 2579, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(488, 2, 7382, 2578, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(494, 2, 7381, 2580, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(500, 2, 7381, 2574, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(505, 2, 7379, 2577, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(511, 2, 7380, 2570, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(516, 2, 7379, 2571, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(522, 2, 7378, 2568, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(527, 2, 7380, 2571, 20.94f, 21.19f, 18.31f));
            arduinoDataFormulaTest.Add(new ArduinoData(533, 2, 7378, 2572, 20.94f, 21.19f, 18.31f));
            arduinoDataFormulaTest.Add(new ArduinoData(539, 2, 7378, 2570, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(544, 2, 7378, 2570, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(550, 2, 7378, 2568, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(555, 2, 7379, 2568, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(561, 2, 7378, 2569, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(567, 2, 7378, 2570, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(572, 2, 7379, 2569, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(578, 2, 7378, 2568, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(583, 2, 7378, 2567, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(589, 2, 7377, 2568, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(595, 2, 7376, 2566, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(600, 2, 7375, 2564, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(606, 2, 7377, 2569, 20.94f, 21.12f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(611, 2, 7375, 2568, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(617, 2, 7370, 2562, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(622, 2, 7376, 2570, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(628, 2, 7377, 2553, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(634, 2, 7375, 2561, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(639, 2, 7374, 2565, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(645, 2, 7374, 2556, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(650, 2, 7375, 2560, 20.94f, 21.12f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(656, 2, 7375, 2560, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(662, 2, 7374, 2560, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(667, 2, 7374, 2564, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(673, 2, 7372, 2563, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(678, 2, 7376, 2645, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(684, 2, 7368, 2521, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(689, 2, 7374, 2571, 20.94f, 21.12f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(695, 2, 7372, 2576, 20.87f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(701, 2, 7372, 2570, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(706, 2, 7373, 2569, 20.87f, 21.12f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(712, 2, 7371, 2569, 20.87f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(717, 2, 7372, 2560, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(723, 2, 7371, 2560, 20.87f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(729, 2, 7371, 2564, 20.87f, 21.12f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(734, 2, 7370, 2564, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(740, 2, 7371, 2569, 20.94f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(745, 2, 7371, 2564, 20.87f, 21.19f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(751, 2, 7371, 2564, 20.94f, 21.12f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(757, 2, 7370, 2560, 20.87f, 21.12f, 18.25f));
            arduinoDataFormulaTest.Add(new ArduinoData(762, 2, 7369, 2563, 20.94f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(768, 2, 7370, 2560, 20.87f, 21.12f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(773, 2, 7371, 2568, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(779, 2, 7370, 2569, 20.87f, 21.12f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(784, 2, 7369, 2561, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(790, 2, 7369, 2569, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(796, 2, 7370, 2563, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(801, 2, 7369, 2571, 20.87f, 21.12f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(807, 2, 7369, 2561, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(812, 2, 7369, 2563, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(818, 2, 7369, 2566, 20.87f, 21.12f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(824, 2, 7368, 2561, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(829, 2, 7377, 2560, 20.87f, 21.12f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(835, 2, 7365, 2563, 20.87f, 21.12f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(840, 2, 7365, 2565, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(846, 2, 7367, 2568, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(852, 2, 7369, 2569, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(857, 2, 7370, 2559, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(863, 2, 7367, 2559, 20.87f, 21.12f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(868, 2, 7368, 2570, 20.87f, 21.12f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(874, 2, 7367, 2562, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(879, 2, 7367, 2578, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(885, 2, 7368, 2574, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(891, 2, 7368, 2578, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(896, 2, 7365, 2568, 20.87f, 21.19f, 18.19f));
            arduinoDataFormulaTest.Add(new ArduinoData(1020, 3, 7366, 173, 20.87f, 21.12f, 18.12f));
            arduinoDataFormulaTest.Add(new ArduinoData(1026, 3, 7366, 170, 20.87f, 21.12f, 18.12f));
            arduinoDataFormulaTest.Add(new ArduinoData(1031, 3, 7367, 170, 20.87f, 21.12f, 18.12f));
            arduinoDataFormulaTest.Add(new ArduinoData(1037, 3, 7366, 170, 20.87f, 21.19f, 18.12f));
            arduinoDataFormulaTest.Add(new ArduinoData(1043, 3, 7367, 170, 20.87f, 21.12f, 18.12f));
            arduinoDataFormulaTest.Add(new ArduinoData(1048, 3, 7366, 169, 20.87f, 21.12f, 18.12f));
            arduinoDataFormulaTest.Add(new ArduinoData(1054, 3, 7367, 169, 20.87f, 21.12f, 18.12f));
            arduinoDataFormulaTest.Add(new ArduinoData(1059, 3, 7367, 169, 20.87f, 21.19f, 18.12f));
            arduinoDataFormulaTest.Add(new ArduinoData(1065, 3, 7367, 169, 20.87f, 21.12f, 18.12f));
        }

		#endregion

		#region Morgan

		private void InitializeOthersMorgan()
		{
            peekTubeResistance = 0.252;
            stemDiameter = 3.925;
            pithDiameter = 0;
            stemLength = 42;
            leafArea = 1;
        }

        private void InitializeSlopeInterceptMorgan()
        {
            prUnitsSensor1.Add(250);
            prUnitsSensor1.Add(988 );
            prUnitsSensor1.Add(1795);
            prUnitsSensor1.Add(2584);
            prUnitsSensor1.Add(3350);
            prUnitsSensor1.Add(4155);
            prUnitsSensor1.Add(4928);
            prUnitsSensor1.Add(5735);
            prUnitsSensor1.Add(6507);
            prUnitsSensor1.Add(7294);

            barSensor1.Add(0.005);
            barSensor1.Add(0.01 );
            barSensor1.Add(0.015);
            barSensor1.Add(0.02 );
            barSensor1.Add(0.025);
            barSensor1.Add(0.03 );
            barSensor1.Add(0.035);
            barSensor1.Add(0.04 );
            barSensor1.Add(0.045);
            barSensor1.Add(0.05);

            slopeSensor1 = FormulaController.GetSlope(prUnitsSensor1, barSensor1);
            interceptSensor1 = FormulaController.GetIntercept(prUnitsSensor1, barSensor1);

            prUnitsSensor2.Add(720);
            prUnitsSensor2.Add(1478);
            prUnitsSensor2.Add(2273);
            prUnitsSensor2.Add(3050);
            prUnitsSensor2.Add(3808);
            prUnitsSensor2.Add(4598);
            prUnitsSensor2.Add(5352);
            prUnitsSensor2.Add(6149);
            prUnitsSensor2.Add(6900);
            prUnitsSensor2.Add(7676);

            barSensor2.Add(0.005);
            barSensor2.Add(0.01 );
            barSensor2.Add(0.015);
            barSensor2.Add(0.02 );
            barSensor2.Add(0.025);
            barSensor2.Add(0.03 );
            barSensor2.Add(0.035);
            barSensor2.Add(0.04 );
            barSensor2.Add(0.045);
            barSensor2.Add(0.05);

            slopeSensor2 = FormulaController.GetSlope(prUnitsSensor2, barSensor2);
            interceptSensor2 = FormulaController.GetIntercept(prUnitsSensor2, barSensor2);
        }

        private void InitializeArduinoDataFormulaTestMorgan()
        {
            arduinoDataFormulaTest.Add(new ArduinoData(5  , 1,   3346,    3718,    23,      22.87f,   22.56f));
            arduinoDataFormulaTest.Add(new ArduinoData(11 , 1,   3347,    3713,    23,      22.87f,   22.56f));
            arduinoDataFormulaTest.Add(new ArduinoData(16 , 1,   3347,    3704,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(22 , 1,   3347,    3728,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(28 , 1,   3346,    3723,    23,      22.81f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(39 , 2,   3344,    1062,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(45 , 2,   3345,    1107,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(50 , 2,   3346,    1172,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(56 , 2,   3346,    1174,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(62 , 2,   3346,    1177,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(67 , 2,   3345,    1180,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(73 , 2,   3345,    1181,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(78 , 2,   3344,    1186,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(84 , 2,   3344,    1193,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(90 , 2,   3346,    1189,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(95 , 2,   3346,    1178,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(101, 2,   3345,    1185,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(106, 2,   3346,    1185,    23,      22.87f,   22.56f));
            arduinoDataFormulaTest.Add(new ArduinoData(112, 2,   3345,    1196,    22.94f,  22.81f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(117, 2,   3344,    1157,    22.94f,  22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(123, 2,   3346,    1158,    22.94f,  22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(129, 2,   3343,    1142,    22.94f,  22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(134, 2,   3345,    1141,    22.94f,  22.81f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(140, 2,   3345,    1143,    22.94f,  22.81f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(145, 2,   3343,    1141,    22.94f,  22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(151, 2,   3345,    1142,    22.94f,  22.81f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(157, 2,   3344,    1146,    22.94f,  22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(162, 2,   3345,    1140,    22.94f,  22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(168, 2,   3344,    1140,    22.94f,  22.81f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(173, 2,   3344,    1151,    22.94f,  22.81f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(179, 2,   3345,    1161,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(185, 2,   3344,    1155,    23,      22.81f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(190, 2,   3344,    1164,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(196, 2,   3344,    1158,    22.94f,  22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(201, 2,   3345,    1171,    22.94f,  22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(207, 2,   3342,    1170,    22.94f,  22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(212, 2,   3344,    1173,    22.94f,  22.81f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(218, 2,   3344,    1169,    22.94f,  22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(224, 2,   3343,    1160,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(229, 2,   3343,    1152,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(235, 2,   3344,    1153,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(240, 2,   3344,    1174,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(246, 2,   3344,    1157,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(252, 2,   3344,    1152,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(257, 2,   3344,    1162,    23,      22.87f,   22.5f));
            arduinoDataFormulaTest.Add(new ArduinoData(263, 2,   3343,    1157,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(268, 2,   3344,    1157,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(274, 2,   3345,    1162,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(279, 2,   3344,    1167,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(285, 2,   3344,    1168,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(291, 2,   3344,    1169,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(296, 2,   3346,    1186,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(302, 2,   3345,    1162,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(307, 2,   3345,    1174,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(313, 2,   3344,    1180,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(319, 2,   3344,    1174,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(324, 2,   3346,    1162,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(330, 2,   3343,    1156,    23,      22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(335, 2,   3344,    1157,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(341, 2,   3342,    1150,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(347, 2,   3344,    1174,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(352, 2,   3343,    1184,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(358, 2,   3343,    1179,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(363, 2,   3342,    1181,    22.87f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(369, 2,   3343,    1176,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(374, 2,   3344,    1167,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(380, 2,   3343,    1173,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(386, 2,   3347,    1173,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(391, 2,   3342,    1165,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(397, 2,   3343,    1163,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(402, 2,   3341,    1169,    23,      22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(408, 2,   3342,    1174,    22.94f,  22.87f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(414, 2,   3343,    1166,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(422, 3,   3343,    273 ,    22.94f,  22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(428, 3,   3343,    157 ,    22.94f,  22.81f,   22.37f));
            arduinoDataFormulaTest.Add(new ArduinoData(433, 3,   3345,    154 ,    23,      22.81f,   22.44f));
            arduinoDataFormulaTest.Add(new ArduinoData(439, 3,   3344,    154 ,    23,      22.87f,   22.44f));
        }

		#endregion

		private void ShowTestFormulas()
        {
            lblFormulaResults.Text = string.Empty;
            lblFormulaResults.Text += "Results:\n";
            lblFormulaResults.Text += "(D6) :" + FormulaController.OptimalPeekTubeChoice(arduinoDataFormulaTest, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2, minBar, maxBar) + "\n";
            lblFormulaResults.Text += "(C7) :" + FormulaController.CalibratedPressureSensor(arduinoDataFormulaTest, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2) + "\n";
            lblFormulaResults.Text += "(D8) :" + FormulaController.SufficientTime(arduinoDataFormulaTest) + "\n";
            lblFormulaResults.Text += "(D9) :" + FormulaController.ValidStability(arduinoDataFormulaTest, peekTubeResistance, slopeSensor1, interceptSensor1) + "\n";
            lblFormulaResults.Text += "(D24) :" + FormulaController.GetAverageStep2Sensor1BarLast300Seconds(arduinoDataFormulaTest, slopeSensor1, interceptSensor1) + "\n";
            lblFormulaResults.Text += "(D25) :" + FormulaController.GetAverageStep2Sensor2BarLast300Seconds(arduinoDataFormulaTest, slopeSensor2, interceptSensor2) + "\n";
            lblFormulaResults.Text += "(D26) :" + FormulaController.GetMmolKRoughLast300Seconds(arduinoDataFormulaTest, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2) + "\n";
            lblFormulaResults.Text += "(D27) :" + FormulaController.GetKGKRough(FormulaController.GetMmolKRoughLast300Seconds(arduinoDataFormulaTest, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2)) + "\n";
            lblFormulaResults.Text += "(D28) :" + FormulaController.GetKCoefficientCorrelation(arduinoDataFormulaTest, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2) + "\n";
            lblFormulaResults.Text += "(D30) :" + FormulaController.GetMinimumStep3Sensor2Bar(arduinoDataFormulaTest, slopeSensor2, interceptSensor2) + "\n";
            lblFormulaResults.Text += "(D31) :" + FormulaController.GetAverageStep3Sensor1Temperature(arduinoDataFormulaTest) + "\n";
            lblFormulaResults.Text += "(D32) :" + FormulaController.GetAverageSensor2Step3Temperature(arduinoDataFormulaTest) + "\n";
            lblFormulaResults.Text += "(D33) :" + FormulaController.GetAverageStep3Temperature(arduinoDataFormulaTest) + "\n";
            lblFormulaResults.Text += "(D34) :" + FormulaController.GetPeektubeResistanceActualTemperature(arduinoDataFormulaTest, peekTubeResistance) + "\n";
            lblFormulaResults.Text += "(D36) :" + FormulaController.GetAverageEndTemperatureStep3(arduinoDataFormulaTest) + "\n";
            lblFormulaResults.Text += "(D38) :" + FormulaController.GetMeasurementTemperatureCorrectedP3K(arduinoDataFormulaTest, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2) + "\n";
            lblFormulaResults.Text += "(D39) :" + FormulaController.Get25oCCorrectedP3K(arduinoDataFormulaTest, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2) + "\n";
            lblFormulaResults.Text += "(D43) :" + FormulaController.GetStemCrossSectionalArea(stemDiameter, pithDiameter) + "\n";
            lblFormulaResults.Text += "(D47) :" + FormulaController.GetConductivity(arduinoDataFormulaTest, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2, stemLength) + "\n";
            lblFormulaResults.Text += "(D48) :" + FormulaController.GetStemAreaSpecificConductivity(arduinoDataFormulaTest, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2, stemLength, stemDiameter, pithDiameter) + "\n";
            lblFormulaResults.Text += "(D49) :" + FormulaController.GetHuberValue(stemDiameter, leafArea) + "\n";
            lblFormulaResults.Text += "(D50) :" + FormulaController.GetLeafAreaSpecificConductivity(arduinoDataFormulaTest, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2, stemLength, leafArea) + "\n";
        }
		private void ShowResults()
        {
            if (lblFormulaResults.InvokeRequired)
            {
                testFormulasSerialCallback serialCallback = new testFormulasSerialCallback(ShowResults);
                Invoke(serialCallback);
            } else
            {
                lblFormulaResults.Text = string.Empty;
                lblFormulaResults.Text += "Results:\n";
                lblFormulaResults.Text += "(D6) :" + FormulaController.OptimalPeekTubeChoice(arduinoData, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2, minBar, maxBar) + "\n";
                lblFormulaResults.Text += "(C7) :" + FormulaController.CalibratedPressureSensor(arduinoData, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2) + "\n";
                lblFormulaResults.Text += "(D8) :" + FormulaController.SufficientTime(arduinoData) + "\n";
                lblFormulaResults.Text += "(D9) :" + FormulaController.ValidStability(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1) + "\n";
                lblFormulaResults.Text += "(D24) :" + FormulaController.GetAverageStep2Sensor1BarLast300Seconds(arduinoData, slopeSensor1, interceptSensor1) + "\n";
                lblFormulaResults.Text += "(D25) :" + FormulaController.GetAverageStep2Sensor2BarLast300Seconds(arduinoData, slopeSensor2, interceptSensor2) + "\n";
                lblFormulaResults.Text += "(D26) :" + FormulaController.GetMmolKRoughLast300Seconds(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2) + "\n";
                lblFormulaResults.Text += "(D27) :" + FormulaController.GetKGKRough(FormulaController.GetMmolKRoughLast300Seconds(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2)) + "\n";
                lblFormulaResults.Text += "(D28) :" + FormulaController.GetKCoefficientCorrelation(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2) + "\n";
                lblFormulaResults.Text += "(D30) :" + FormulaController.GetMinimumStep3Sensor2Bar(arduinoData, slopeSensor2, interceptSensor2) + "\n";
                lblFormulaResults.Text += "(D31) :" + FormulaController.GetAverageStep3Sensor1Temperature(arduinoData) + "\n";
                lblFormulaResults.Text += "(D32) :" + FormulaController.GetAverageSensor2Step3Temperature(arduinoData) + "\n";
                lblFormulaResults.Text += "(D33) :" + FormulaController.GetAverageStep3Temperature(arduinoData) + "\n";
                lblFormulaResults.Text += "(D34) :" + FormulaController.GetPeektubeResistanceActualTemperature(arduinoData, peekTubeResistance) + "\n";
                lblFormulaResults.Text += "(D36) :" + FormulaController.GetAverageEndTemperatureStep3(arduinoData) + "\n";
                lblFormulaResults.Text += "(D38) :" + FormulaController.GetMeasurementTemperatureCorrectedP3K(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2) + "\n";
                lblFormulaResults.Text += "(D39) :" + FormulaController.Get25oCCorrectedP3K(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2) + "\n";
                lblFormulaResults.Text += "(D43) :" + FormulaController.GetStemCrossSectionalArea(stemDiameter, pithDiameter) + "\n";
                lblFormulaResults.Text += "(D47) :" + FormulaController.GetConductivity(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2, stemLength) + "\n";
                lblFormulaResults.Text += "(D48) :" + FormulaController.GetStemAreaSpecificConductivity(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2, stemLength, stemDiameter, pithDiameter) + "\n";
                lblFormulaResults.Text += "(D49) :" + FormulaController.GetHuberValue(stemDiameter, leafArea) + "\n";
                lblFormulaResults.Text += "(D50) :" + FormulaController.GetLeafAreaSpecificConductivity(arduinoData, peekTubeResistance, slopeSensor1, interceptSensor1, slopeSensor2, interceptSensor2, stemLength, leafArea) + "\n";
            }
        }

        private void UseRightPort()
        {
            if (!ArduinoPortFound())
            {
                MessageBox.Show("Le port série du montage Xylem n'a pas été trouvé.");
            }
        }

        private bool ArduinoPortFound()
        {
            bool arduinoFound = false;
            ManagementScope connectionScope = new ManagementScope();
            SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);
            try
            {
                foreach (ManagementObject item in searcher.Get())
                {
                    string desc = item["Description"].ToString();
                    string deviceId = item["DeviceID"].ToString();
                    if (desc.Contains("Périphérique série USB") || desc.Contains("USB Serial Device"))
                    {
                        arduinoFound = true;
                        srlprtArduino.PortName = deviceId;
                    }
                }
            }
            catch (ManagementException e)
            {
                /* Do Nothing */
            }
            return arduinoFound;
        }

        private void srlprtArduino_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string dataReceived = srlprtArduino.ReadLine();
            dataReceived = dataReceived.Replace("\r", "");
            string[] data = dataReceived.Split('\t');
            try
            {
                if (data.Length >= 7)
                {
                    ArduinoData arduinoData = new ArduinoData();
                    arduinoData.Time = Int32.Parse(data[0]);
                    arduinoData.Step = Int32.Parse(data[1]);
                    arduinoData.CurrentSessionRecord = sessionRecord;

                    Sensor sensor1 = new Sensor();
                    sensor1.Pressure = Int32.Parse(data[2]);
                    sensor1.Temperature = float.Parse(data[4], CultureInfo.InvariantCulture);
                    arduinoData.Sensor1 = sensor1;

                    Sensor sensor2 = new Sensor();
                    sensor2.Pressure = Int32.Parse(data[3]);
                    sensor2.Temperature = float.Parse(data[5], CultureInfo.InvariantCulture);
                    arduinoData.Sensor2 = sensor2;

                    arduinoData.EndTemperature = float.Parse(data[6], CultureInfo.InvariantCulture);

                    arduinoData.ID = databaseController.AddArduinoData(arduinoData);
                    this.arduinoData.Add(arduinoData);

                    SetDataGridView(arduinoData);
                    ShowResults();
                    UpdateChart();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Une donnée provenant du Arduino n'est pas lisible par le programme.");
            }
        }

        private void SetDataGridView(ArduinoData arduinoData)
        {
            if (dgvMeasurement.InvokeRequired)
            {
                serialCallback serialCallback = new serialCallback(SetDataGridView);
                Invoke(serialCallback, arduinoData);
            }
            else
            {
                if (!startingToChangeForm)
                {
                    int index = dgvMeasurement.Rows.Add();
                    dgvMeasurement.Rows[index].Cells[0].Value = arduinoData.Time;
                    dgvMeasurement.Rows[index].Cells[1].Value = arduinoData.Step;
                    dgvMeasurement.Rows[index].Cells[2].Value = arduinoData.Sensor1.Pressure;
                    dgvMeasurement.Rows[index].Cells[3].Value = arduinoData.Sensor2.Pressure;
                    dgvMeasurement.Rows[index].Cells[4].Value = arduinoData.Sensor1.Temperature.ToString("0.00");
                    dgvMeasurement.Rows[index].Cells[5].Value = arduinoData.Sensor2.Temperature.ToString("0.00");
                    dgvMeasurement.Rows[index].Cells[6].Value = arduinoData.EndTemperature.ToString("0.00");
                    dgvMeasurement.Rows[index].Cells[7].Value = arduinoData.ID;
                    dgvMeasurement.Rows[index].Cells[8].Value = true;
                    if (dgvMeasurement.Rows.Count >= 2 && dgvMeasurement.Rows[dgvMeasurement.Rows.Count - 2].Displayed)
                    {
                        dgvMeasurement.FirstDisplayedScrollingRowIndex = dgvMeasurement.RowCount - 1;
                    }
                }
            }
        }

        private void UpdateChart()
        {
            if (chartBar.InvokeRequired)
            {
                testFormulasSerialCallback serialCallback = new testFormulasSerialCallback(UpdateChart);
                Invoke(serialCallback);
            }
            else
            {
                List<int> xValuesSensor1 = FormulaController.GetArduinoDataTime(GetShowArduinoData());
                List<double> yValuesSensor1 = FormulaController.GetSensor1BarChart(GetShowArduinoData(), slopeSensor1, interceptSensor1);
                chartBar.Series["Sensor1"].Points.DataBindXY(xValuesSensor1, yValuesSensor1);
                List<int> xValuesSensor2 = FormulaController.GetArduinoDataTime(GetShowArduinoData());
                List<double> yValuesSensor2 = FormulaController.GetSensor2BarChart(GetShowArduinoData(), slopeSensor2, interceptSensor2);
                chartBar.Series["Sensor1"].Points.DataBindXY(xValuesSensor2, yValuesSensor2);
            }
        }

        private void UpdateChartTest()
        {
            if (chartBar.InvokeRequired)
            {
                testFormulasSerialCallback serialCallback = new testFormulasSerialCallback(UpdateChartTest);
                Invoke(serialCallback);
            }
            else
            {
                List<int> xValuesSensor1 = FormulaController.GetArduinoDataTime(arduinoDataFormulaTest);
                List<double> yValuesSensor1 = FormulaController.GetSensor1BarChart(arduinoDataFormulaTest, slopeSensor1, interceptSensor1);
                chartBar.Series["Sensor1"].Points.DataBindXY(xValuesSensor1, yValuesSensor1);
                List<int> xValuesSensor2 = FormulaController.GetArduinoDataTime(arduinoDataFormulaTest);
                List<double> yValuesSensor2 = FormulaController.GetSensor2BarChart(arduinoDataFormulaTest, slopeSensor2, interceptSensor2);
                chartBar.Series["Sensor2"].Points.DataBindXY(xValuesSensor2, yValuesSensor2);
            }
        }

		private void dgvMeasurement_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0 && dgvMeasurement[e.ColumnIndex, e.RowIndex].Selected == true && dgvMeasurement.RowCount >= 1)
            {
                if (e.ColumnIndex == 8)
                {
                    ChangeCheckBox(e.RowIndex);
                }
                else
                {
                    UpdateArduinoDataStep(e.RowIndex, e.ColumnIndex);
                }
            }
        }

        private void ChangeCheckBox(int rowIndex)
        {
            DataGridViewCheckBoxCell checkBox = (DataGridViewCheckBoxCell)dgvMeasurement[8, rowIndex];
            arduinoData[rowIndex].Show = Convert.ToBoolean(checkBox.Value);
            UpdateChart();
        }

        private void UpdateArduinoDataStep(int rowIndex, int columnIndex)
        {
            int selectedCellCount =
                    dgvMeasurement.GetCellCount(DataGridViewElementStates.Selected);
            if (selectedCellCount > 0)
            {
                for (int i = 0; i < selectedCellCount; i++)
                {
                    UpdateArduinoData(Convert.ToInt32(dgvMeasurement[7, dgvMeasurement.SelectedCells[i].RowIndex].Value), Convert.ToInt32(dgvMeasurement.Rows[rowIndex].Cells[columnIndex].Value));
                    dgvMeasurement[1, dgvMeasurement.SelectedCells[i].RowIndex].Value = dgvMeasurement.Rows[rowIndex].Cells[columnIndex].Value;
                    UpdateChart();
                }
            }
        }

        private void UpdateArduinoData(int id, int step)
        {
            ArduinoData newArduinoData = arduinoData.Where(ad => ad.ID == id).FirstOrDefault();
            if (newArduinoData != null)
            {
                int arduinoDataIndex = arduinoData.IndexOf(newArduinoData);
                arduinoData[arduinoDataIndex].Step = step;
                databaseController.UpdateArduinoDataStep(arduinoData[arduinoDataIndex]);
            }
        }

		private void dgvMeasurement_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            int arduinoDataID = Int32.Parse(e.Row.Cells[7].Value.ToString());
            DeleteArduinoData(arduinoDataID);
            databaseController.DeleteArduinoData(arduinoDataID);
            UpdateChart();
        }

        private void DeleteArduinoData(long arduinoDataID)
        {
            ArduinoData arduinoDataFound = arduinoData.Find(ad => ad.ID == arduinoDataID);
            if (arduinoDataFound != null)
            {
                arduinoData.RemoveAt(arduinoData.IndexOf(arduinoDataFound));
            }
            databaseController.DeleteArduinoData(arduinoDataID);
        }

        private List<ArduinoData> GetShowArduinoData()
        {
            return arduinoData.Where(data => data.Show == true).ToList();
        }
    }
}
