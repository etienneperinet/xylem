﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Xylem
{
    public partial class FrmRecordCalibration : Form
    {
        private SessionRecord sessionRecord;
        private DatabaseController databaseController;
        private List<ArduinoData> arduinoData = new List<ArduinoData>();

        delegate void serialCallback(ArduinoData arduinoData);
        delegate void listBoxSerialCallback();
        delegate void chartSerialCallback();

        private int heightIncrementation;

        // for test
        public FrmRecordCalibration()
        {
            InitializeComponent();
            databaseController = new DatabaseController();
            if (Session.GetInstance() == null)
            {
                databaseController.CreateSessionTest();
                sessionRecord = databaseController.CreateSessionRecordTest(Session.GetInstance());
            }
            else
            {
                sessionRecord = new SessionRecord(Session.GetInstance());
                sessionRecord.ID = databaseController.AddSessionRecord(sessionRecord);
            }
            SerialPortCommunicator.SerialPort.DataReceived += serialPortArduino_DataReceived;
        }

        private void FrmRecord_Load(object sender, EventArgs e)
        {
            /*
            if (!srlprtArduino.IsOpen)
            {
                try
                {
                    srlprtArduino.Open();
                }
                catch (Exception exception)
                {
                    //MessageBox.Show(exception.Message);
                }
            }*/
            heightIncrementation = Int32.Parse(txtHeightIncrementation.Text);
        }

        private bool IsDataUsable(string[] data)
        {
            return 
                data[1].Equals("0") ||
                data[2].Equals("0") ||
                data[3].Equals("0");
        }


        private void serialPortArduino_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string dataReceived = SerialPortCommunicator.SerialPort.ReadLine();
            dataReceived = dataReceived.Replace("\r", "");
            string[] data = dataReceived.Split('\t');
            try
            {
                
                if (data.Length >= 7)
                {
                   // if (IsDataUsable(data))
                    //{
                        ArduinoData arduinoData = new ArduinoData();
                        arduinoData.Time = Int32.Parse(data[0]);
                        arduinoData.Step = Int32.Parse(data[1]);
                        arduinoData.CurrentSessionRecord = sessionRecord;

                        Sensor sensor1 = new Sensor();
                        sensor1.Pressure = Int32.Parse(data[2]);
                        sensor1.Temperature = float.Parse(data[4], CultureInfo.InvariantCulture);
                        arduinoData.Sensor1 = sensor1;

                        Sensor sensor2 = new Sensor();
                        sensor2.Pressure = Int32.Parse(data[3]);
                        sensor2.Temperature = float.Parse(data[5], CultureInfo.InvariantCulture);
                        arduinoData.Sensor2 = sensor2;

                        arduinoData.EndTemperature = float.Parse(data[6], CultureInfo.InvariantCulture);

                        arduinoData.ID = databaseController.AddArduinoData(arduinoData);
                        this.arduinoData.Add(arduinoData);

                        SetDataGridView(arduinoData);
                        UpdateCharts();
                        UpdateSquaredPearson();
                    /*}
                    else
                    {
                        MessageBox.Show("Une donnée provenant du Arduino n'est pas utilisable par le programme.");
                    }*/
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Une donnée provenant du Arduino n'est pas lisible par le programme.");
            }
            
        }

        private List<ArduinoData> GetShowArduinoData()
        {
            return arduinoData.Where(data => data.Show == true).ToList();
        }

        private void UpdateSquaredPearson()
        {
            if (txtPearsonSensor1.InvokeRequired)
            {
                chartSerialCallback serialCallback = new chartSerialCallback(UpdatePearsonSensor1);
                Invoke(serialCallback);
            } else
			{
                UpdatePearsonSensor1();
			}

            if (txtPearsonSensor2.InvokeRequired)
            {
                chartSerialCallback serialCallback = new chartSerialCallback(UpdatePearsonSensor2);
                Invoke(serialCallback);
            } else
			{
                UpdatePearsonSensor2();
			}
        }

        private void UpdatePearsonSensor1()
        {
            txtPearsonSensor1.Text = FormulaController.GetSquaredPearson(FormulaController.GetSensor1PressureAveragePerStep(GetShowArduinoData()).Select(avg => (double)avg).ToList(), FormulaController.GetBars(FormulaController.GetHeights(GetShowArduinoData(), heightIncrementation))).ToString();
        }

        private void UpdatePearsonSensor2()
        {
            txtPearsonSensor2.Text = FormulaController.GetSquaredPearson(FormulaController.GetSensor2PressureAveragePerStep(GetShowArduinoData()).Select(avg => (double)avg).ToList(), FormulaController.GetBars(FormulaController.GetHeights(GetShowArduinoData(), heightIncrementation))).ToString();
        }

        private void SetDataGridView(ArduinoData arduinoData)
        {
            if (dgvCalibration.InvokeRequired)
            {
                serialCallback serialCallback = new serialCallback(SetDataGridView);
                Invoke(serialCallback, arduinoData);
            }
            else
            {
                int index = dgvCalibration.Rows.Add();
                dgvCalibration.Rows[index].Cells[0].Value = arduinoData.Time;
                dgvCalibration.Rows[index].Cells[1].Value = arduinoData.Step;
                dgvCalibration.Rows[index].Cells[2].Value = arduinoData.Sensor1.Pressure;
                dgvCalibration.Rows[index].Cells[3].Value = arduinoData.Sensor2.Pressure;
                dgvCalibration.Rows[index].Cells[4].Value = arduinoData.Sensor1.Temperature.ToString("0.00");
                dgvCalibration.Rows[index].Cells[5].Value = arduinoData.Sensor2.Temperature.ToString("0.00");
                dgvCalibration.Rows[index].Cells[6].Value = arduinoData.EndTemperature.ToString("0.00");
                dgvCalibration.Rows[index].Cells[7].Value = arduinoData.ID;
                dgvCalibration.Rows[index].Cells[8].Value = true;
                if (dgvCalibration.Rows.Count >= 2 && dgvCalibration.Rows[dgvCalibration.Rows.Count - 2].Displayed)
                {
                    dgvCalibration.FirstDisplayedScrollingRowIndex = dgvCalibration.RowCount - 1;
                }
            }
        }

        private void UpdateCharts()
        {
            if (chartSensor1.InvokeRequired)
            {
                chartSerialCallback serialCallback = new chartSerialCallback(UpdateChart1);
                Invoke(serialCallback);
            } else
			{
                UpdateChart1();
			}
            if (chartSensor2.InvokeRequired)
            {
                chartSerialCallback serialCallback = new chartSerialCallback(UpdateChart2);
                Invoke(serialCallback);
            } else
			{
                UpdateChart2();
			}
        }
        
        private void UpdateChart1()
        {
            List<int> xValuesSensor1 = FormulaController.GetSensor1PressureAveragePerStep(GetShowArduinoData());
            List<double> yValuesSensor1 = FormulaController.GetBars(FormulaController.GetHeights(GetShowArduinoData(), heightIncrementation));
            chartSensor1.Series["Steps"].Points.DataBindXY(xValuesSensor1, yValuesSensor1);
            chartSensor1.Series["Regression"].Points.Clear();
            if (xValuesSensor1.Count > 1)
            {
                SetChart1RegressionLine(xValuesSensor1.Select(value => (double)value).ToList(), yValuesSensor1.ToList());
            }
        }
        private void UpdateChart2()
        {
            List<int> xValuesSensor2 = FormulaController.GetSensor2PressureAveragePerStep(GetShowArduinoData());
            List<double> yValuesSensor2 = FormulaController.GetBars(FormulaController.GetHeights(GetShowArduinoData(), heightIncrementation));
            chartSensor2.Series["Steps"].Points.DataBindXY(xValuesSensor2, yValuesSensor2);
            chartSensor2.Series["Regression"].Points.Clear();
            if (xValuesSensor2.Count > 1)
            {
                SetChart2RegressionLine(xValuesSensor2.Select(value => (double)value).ToList(), yValuesSensor2.ToList());
            }
        }

        private void SetChart1RegressionLine(List<double> prUnits, List<double> bar)
        {
            double slope = FormulaController.GetSlope(prUnits, bar);
            double intercept = FormulaController.GetIntercept(prUnits, bar);
            List<int> xRegressionSensor1 = new List<int>();
            List<double> yRegressionSensor1 = new List<double>();
            for (int index = 0; index < prUnits.Count; index++)
            {
                xRegressionSensor1.Add(Convert.ToInt32(prUnits[index]));
                yRegressionSensor1.Add((slope * prUnits[index]) + intercept);
            }
            chartSensor1.Series["Regression"].Points.DataBindXY(xRegressionSensor1, yRegressionSensor1);
        }

        private void SetChart2RegressionLine(List<double> prUnits, List<double> bar)
        {
            double slope = FormulaController.GetSlope(prUnits, bar);
            double intercept = FormulaController.GetIntercept(prUnits, bar);
            List<int> xRegressionSensor2 = new List<int>();
            List<double> yRegressionSensor2 = new List<double>();
            for (int index = 0; index < prUnits.Count; index++)
            {
                xRegressionSensor2.Add(Convert.ToInt32(prUnits[index]));
                yRegressionSensor2.Add((slope * prUnits[index]) + intercept);
            }
            chartSensor2.Series["Regression"].Points.DataBindXY(xRegressionSensor2, yRegressionSensor2);
        }

        private void txtHeightIncrementation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }

        private void txtHeightIncrementation_Leave(object sender, EventArgs e)
        {
            if (txtHeightIncrementation.Text != null && txtHeightIncrementation.Text != String.Empty)
            {
                heightIncrementation = Int32.Parse(txtHeightIncrementation.Text);
                UpdateCharts();
                UpdateSquaredPearson();
            }
        }

        private void DeleteArduinoData(long arduinoDataID)
        {
            ArduinoData arduinoDataFound = arduinoData.Find(ad => ad.ID == arduinoDataID);
            if (arduinoDataFound != null)
            {
                arduinoData.RemoveAt(arduinoData.IndexOf(arduinoDataFound));
            }
            databaseController.DeleteArduinoData(arduinoDataID);
        }

        private void DeleteArduinoDataGridView(long arduinoDataID)
		{
            for (int rowIndex = 0; rowIndex < dgvCalibration.Rows.Count; rowIndex++)
			{
                if (Convert.ToInt32(dgvCalibration[7, rowIndex].Value) == arduinoDataID)
				{
                    dgvCalibration.Rows.RemoveAt(rowIndex);
				}
			}
        }

        private void ChangeCheckBox(int rowIndex)
        {
            DataGridViewCheckBoxCell checkBox = (DataGridViewCheckBoxCell)dgvCalibration[8, rowIndex];
            arduinoData[rowIndex].Show = Convert.ToBoolean(checkBox.Value);
            UpdateCharts();
            UpdateSquaredPearson();
        }

        private void UpdateArduinoDataStep(int rowIndex, int columnIndex)
        {
            int selectedCellCount =
                    dgvCalibration.GetCellCount(DataGridViewElementStates.Selected);
            if (selectedCellCount > 0)
            {
                for (int i = 0; i < selectedCellCount; i++)
                {
                    UpdateArduinoData(Convert.ToInt32(dgvCalibration[7, dgvCalibration.SelectedCells[i].RowIndex].Value), Convert.ToInt32(dgvCalibration.Rows[rowIndex].Cells[columnIndex].Value));
                    dgvCalibration[1, dgvCalibration.SelectedCells[i].RowIndex].Value = dgvCalibration.Rows[rowIndex].Cells[columnIndex].Value;
                    UpdateCharts();
                    UpdateSquaredPearson();
                }
            }
        }

        private void UpdateArduinoData(int id, int step)
        {
            ArduinoData newArduinoData = arduinoData.Where(ad => ad.ID == id).FirstOrDefault();
            if (newArduinoData != null)
			{
                int arduinoDataIndex = arduinoData.IndexOf(newArduinoData);
                arduinoData[arduinoDataIndex].Step = step;
                databaseController.UpdateArduinoDataStep(arduinoData[arduinoDataIndex]);
            }
        }
       

        private void dgvCalibration_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            int arduinoDataID = Int32.Parse(e.Row.Cells[7].Value.ToString());
            DeleteArduinoData(arduinoDataID);
            databaseController.DeleteArduinoData(arduinoDataID);
            UpdateCharts();
            UpdateSquaredPearson();
        }

		private void dgvCalibration_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0 && dgvCalibration[e.ColumnIndex, e.RowIndex].Selected == true && dgvCalibration.RowCount >= 1)
            {
                if (e.ColumnIndex == 8)
                {
                    ChangeCheckBox(e.RowIndex);
                }
                else
                {
                    UpdateArduinoDataStep(e.RowIndex, e.ColumnIndex);
                }
            }
        }

        private bool EndCalibration()
        {
            DialogResult dialogResult = MessageBox.Show("Voulez-vous terminer la calibration?", "Continuer", MessageBoxButtons.YesNo);
            return (dialogResult == DialogResult.Yes);
        }

        private bool DeleteHiddenArduinoData()
        {
            DialogResult dialogResult = MessageBox.Show("Voulez-vous effacer les données non visible?", "Effacement de données invisible", MessageBoxButtons.YesNo);
            return (dialogResult == DialogResult.Yes);
        }

        private void ContinueToMeasurements()
        {
            double slope1 = FormulaController.GetSlope(FormulaController.GetSensor1PressureAveragePerStep(arduinoData).Select(avg => (double)avg).ToList(), FormulaController.GetBars(FormulaController.GetHeights(GetShowArduinoData(), heightIncrementation)));
            double slope2 = FormulaController.GetSlope(FormulaController.GetSensor2PressureAveragePerStep(arduinoData).Select(avg => (double)avg).ToList(), FormulaController.GetBars(FormulaController.GetHeights(GetShowArduinoData(), heightIncrementation)));
            double intercept1 = FormulaController.GetIntercept(FormulaController.GetSensor1PressureAveragePerStep(arduinoData).Select(avg => (double)avg).ToList(), FormulaController.GetBars(FormulaController.GetHeights(GetShowArduinoData(), heightIncrementation)));
            double intercept2 = FormulaController.GetIntercept(FormulaController.GetSensor2PressureAveragePerStep(arduinoData).Select(avg => (double)avg).ToList(), FormulaController.GetBars(FormulaController.GetHeights(GetShowArduinoData(), heightIncrementation)));
            double pearson1 = FormulaController.GetSquaredPearson(FormulaController.GetSensor1PressureAveragePerStep(arduinoData).Select(avg => (double)avg).ToList(), FormulaController.GetBars(FormulaController.GetHeights(GetShowArduinoData(), heightIncrementation)));
            double pearson2 = FormulaController.GetSquaredPearson(FormulaController.GetSensor2PressureAveragePerStep(arduinoData).Select(avg => (double)avg).ToList(), FormulaController.GetBars(FormulaController.GetHeights(GetShowArduinoData(), heightIncrementation)));
            string note = String.Empty;
            CalibrationResult calibrationResult = new CalibrationResult(intercept1, intercept2, slope1, slope2, pearson1, pearson2, sessionRecord, note); ;
            databaseController.AddCalibrationResult(calibrationResult);
            this.Hide();
            FrmCreateMeasurement frmCreateMeasurement = new FrmCreateMeasurement();
            frmCreateMeasurement.ShowDialog();
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            if (EndCalibration())
            {
                if (arduinoData.Where(ad => ad.Show == false).Any() && DeleteHiddenArduinoData())
                {
                    foreach (ArduinoData data in arduinoData.Where(ad => ad.Show == false).ToList())
                    {
                        DeleteArduinoDataGridView(data.ID);
                        DeleteArduinoData(data.ID);
                        databaseController.DeleteArduinoData(data.ID);
                        UpdateCharts();
                        UpdateSquaredPearson();
                    }
                }
                ContinueToMeasurements();
                //SerialPortCommunicator.SerialPort.Par = null; 
            }
        }
    }
}
