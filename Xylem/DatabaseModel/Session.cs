﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class Session
    {
        #region instance
        static Session instance;
        public static Session GetInstance()
        {
            return instance;
        }
        public static void SetInstance(int arduinoNumber, Computer currentComputer, Operator currentOperator)
        {
            instance = new Session(arduinoNumber, currentComputer, currentOperator);
        }
        #endregion

        public const int SessionTypeCalibrationID = 1;
        public const int SessionTypeMeasurementID = 2;

        public int ID { set; get; }
        public int Year { get; }
        public int Month { get; }
        public int Day { get; }
        public Computer CurrentComputer { set; get; }
        public int ArduinoNumber { set; get; }
        public Operator CurrentOperator { set; get; }

        public Session(int arduinoNumber, Computer currentComputer, Operator currentOperator)
        {
            DateTime localDate = DateTime.Now;
            Year = localDate.Year;
            Month = localDate.Month;
            Day = localDate.Day;
            ArduinoNumber = arduinoNumber;
            CurrentComputer = currentComputer;
            CurrentOperator = currentOperator;
        }

        public Session(int id, int arduinoNumber, Computer currentComputer, Operator currentOperator)
        {
            ID = id;
            DateTime localDate = DateTime.Now;
            Year = localDate.Year;
            Month = localDate.Month;
            Day = localDate.Day;
            ArduinoNumber = arduinoNumber;
            CurrentComputer = currentComputer;
            CurrentOperator = currentOperator;
        }

        public Session(int id, int year, int month, int day, int arduinoNumber, Computer currentComputer, Operator currentOperator)
        {
            ID = id;
            Year = year;
            Month = month;
            Day = day;
            ArduinoNumber = arduinoNumber;
            CurrentComputer = currentComputer;
            CurrentOperator = currentOperator;
        }

        public Session(int id)
        {
            DateTime localDate = DateTime.Now;
            Year = localDate.Year;
            Month = localDate.Month;
            Day = localDate.Day;
        }

    }
}