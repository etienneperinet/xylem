﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class PerfusionSolution
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public PerfusionSolution(int id, string name)
        {
            ID = id;
            Name = name;
        }
        public PerfusionSolution(string name)
        {
            ID = 0;
            Name = name;
        }

        public PerfusionSolution()
        {
            ID = 0;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
