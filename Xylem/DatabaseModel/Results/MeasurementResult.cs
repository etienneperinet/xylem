﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
	public class MeasurementResult
    {
		public int ID { get; set; }
		public int WaterHeight { get; set; }
		public bool ValidPeekTube { get; set; }
		public bool SufficientTime { get; set; }
		public bool Stable { get; set; }
		public bool ValidSensorPressure { get; set; }
		public float P1 { get; set; }
		public float P2 { get; set; }
		public float Mmol_k_rough { get; set; }
		public float Kg_k_rough { get; set; }
		public float CV_K { get; set; }
		public float P3 { get; set; }
		public float T1 { get; set; }
		public float T_AV { get; set; }
		public float R_peektube_t { get; set; }
		public float T3 { get; set; }
		public float K_T { get; set; }
		public float K_25 { get; set; }
		public float KH { get; set; }
		public float KS { get; set; }
		public float HV { get; set; }
		public float KL { get; set; }
		public float GammePeekTube { get; set; }
		public float Diameter1 { get; set; }
		public float Diameter2 { get; set; }
		public float Lenght { get; set; }
		public PerfusionSolution SolutionPerfusion { get; set; }
		public PeekTube PeekTube { get; set; }
		public SessionRecord SessionRecord { get; set; }
		public string Note { get; set; }
		
		public MeasurementResult()
        {

        }

		public MeasurementResult(int id, int waterHeight,
			bool validPeekTube, bool stable, bool validSensorPressure, bool sufficientTime,
			float p1, float p2, float mmol_k_rough, float kg_k_rough, float cv_k, 
			float p3, float t1, float t_av, float r_peektube_t, float t3,
			float k_t, float k_25, float kh, float ks, float hv, 
			float kl, float gammePeekTube,
			float diameter1, float diameter2, float lenght,
			PerfusionSolution solutionPerfusion, PeekTube peekTube, SessionRecord sessionRecord,
			string note)
        {
			ID = id;
			WaterHeight = waterHeight;

			ValidPeekTube = validPeekTube;
			Stable = stable;
			ValidSensorPressure = validSensorPressure;
			SufficientTime = sufficientTime;

			P1 = p1;
			P2 = p2;
			Mmol_k_rough = mmol_k_rough;
			Kg_k_rough = kg_k_rough;
			CV_K = cv_k;

			P3 = p3;
			T1 = t1;
			T_AV = t_av;
			R_peektube_t = r_peektube_t;
			T3 = t3;

			K_T = k_t;
			K_25 = k_25;
			KH = kh;
			KS = ks;
			HV = hv;

			KL = kl;
			GammePeekTube = gammePeekTube;

			Diameter1 = diameter1;
			Diameter2 = diameter2;
			Lenght = lenght;

			SolutionPerfusion = solutionPerfusion;
			PeekTube = peekTube;
			SessionRecord = sessionRecord;

			Note = note;
		}
		
		public MeasurementResult(int waterHeight,
			bool validPeekTube, bool stable, bool validSensorPressure, bool sufficientTime,
			float p1, float p2, float mmol_k_rough, float kg_k_rough, float cv_k, 
			float p3, float t1, float t_av, float r_peektube_t, float t3,
			float k_t, float k_25, float kh, float ks, float hv, 
			float kl, float gammePeekTube,
			float diameter1, float diameter2, float lenght,
			PerfusionSolution solutionPerfusion, PeekTube peekTube, SessionRecord sessionRecord,
			string note)
        {
			WaterHeight = waterHeight;

			ValidPeekTube = validPeekTube;
			Stable = stable;
			ValidSensorPressure = validSensorPressure;
			SufficientTime = sufficientTime;

			P1 = p1;
			P2 = p2;
			Mmol_k_rough = mmol_k_rough;
			Kg_k_rough = kg_k_rough;
			CV_K = cv_k;

			P3 = p3;
			T1 = t1;
			T_AV = t_av;
			R_peektube_t = r_peektube_t;
			T3 = t3;

			K_T = k_t;
			K_25 = k_25;
			KH = kh;
			KS = ks;
			HV = hv;

			KL = kl;
			GammePeekTube = gammePeekTube;

			SolutionPerfusion = solutionPerfusion;
			PeekTube = peekTube;
			SessionRecord = sessionRecord;

			Diameter1 = diameter1;
			Diameter2 = diameter2;
			Lenght = lenght;

			Note = note;
		}
	}
}
