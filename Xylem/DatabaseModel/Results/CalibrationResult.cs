﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class CalibrationResult
    {
        public int ID { get; set; }
        public double Slope1 { get; set; }
        public double Slope2 { get; set; }
        public double Intercept1 { get; set; }
        public double Intercept2 { get; set; }
        public double Pearson1 { get; set; }
        public double Pearson2 { get; set; }
        public SessionRecord SessionRecord { get; set; }
        public string Note { get; set; }

        public CalibrationResult()
        {
            ID = -1;
        }

        public CalibrationResult(int id, double intercept1, double intercept2, double slope1, double slope2,
            double pearson1, double pearson2, SessionRecord sessionRecord, string note)
        {
            ID = id;
            Intercept1 = intercept1;
            Intercept2 = intercept2;
            Slope1 = slope1;
            Slope2 = slope2;
            Pearson1 = pearson1;
            Pearson2 = pearson2;
            SessionRecord = sessionRecord;
            Note = note;
        }

        public CalibrationResult(double intercept1, double intercept2, double slope1, double slope2,
            double pearson1, double pearson2, SessionRecord sessionRecord, string note)
        {
            ID = -1;
            Intercept1 = intercept1;
            Intercept2 = intercept2;
            Slope1 = slope1;
            Slope2 = slope2;
            Pearson1 = pearson1;
            Pearson2 = pearson2;
            SessionRecord = sessionRecord;
            Note = note;
        }
    }
}
