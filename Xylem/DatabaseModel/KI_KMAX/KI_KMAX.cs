﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class KI_KMAX
    {
        //If KMAX == null, KI_KMAX = KI
        public int ID { get; set; }
        public KMAX KMax { get; set; }
        public MeasurementResult MeasurementResult { get; set; }
        public Sample Sample { get; set; }

        public KI_KMAX()
        {
            ID = -1;
            KMax = null;
        }

        public KI_KMAX(KMAX kmax, MeasurementResult measurementResult, Sample sample)
        {
            ID = -1;
            KMax = kmax;
            MeasurementResult = measurementResult;
            Sample = sample;
        }

        public KI_KMAX(int id, KMAX kmax, MeasurementResult measurementResult, Sample sample)
        {
            ID = id;
            KMax = kmax;
            MeasurementResult = measurementResult;
            Sample = sample;
        }

        public KI_KMAX(int id)
        {
            ID = id;
            KMax = null;
        }

    }
}