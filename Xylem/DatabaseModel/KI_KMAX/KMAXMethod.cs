﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class KMAXMethod
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public KMAXMethod()
        {
            ID = -1;
            Name = "";
        }

        public KMAXMethod(int id, string name)
        {
            ID = id;
            Name = name;
        }

        public KMAXMethod(int id)
        {
            ID = id;
        }

        public KMAXMethod(string name)
        {
            ID = -1;
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
