﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class KMAX
    {
        public int ID { get; set; }
        public int KMaxDurationMin { get; set; }
        public int KMaxIteration { get; set; }
        public int KMaxPression { get; set; }
        public KMAXMethod KMAXMethode { get; set; }

        public KMAX(int kmaxDurationMin, int kmaxIteration, int kmaxPression,
            KMAXMethod kmaxMethode)
        {
            KMaxDurationMin = kmaxDurationMin;
            KMaxIteration = kmaxIteration;
            KMaxPression = kmaxPression;
            KMAXMethode = kmaxMethode;
        }

        public KMAX(int id, int kmaxDurationMin, int kmaxIteration, int kmaxPression, KMAXMethod kmaxMethode)
        {
            ID = id;
            KMaxDurationMin = kmaxDurationMin;
            KMaxIteration = kmaxIteration;
            KMaxPression = kmaxPression;
            KMAXMethode = kmaxMethode;
        } 
        public KMAX(int id)
        {
            ID = id;
        } 
    }
}
