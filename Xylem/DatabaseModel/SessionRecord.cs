﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class SessionRecord
    {
        public int ID { get; set; }
        public Session Session { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }

        public SessionRecord()
        {

        }

        public SessionRecord(int id, Session session, int hour, int minute)
        {
            ID = id;
            Hour = hour;
            Minute = minute;
            Session = session;
        }

        public SessionRecord(int id, Session session)
        {
            ID = id;
            DateTime localDate = DateTime.Now;
            Hour = localDate.Hour;
            Minute = localDate.Minute;
            Session = session;
        }
        
        public SessionRecord(Session session)
        {
            ID = -1;
            DateTime localDate = DateTime.Now;
            Hour = localDate.Hour;
            Minute = localDate.Minute;
            Session = session;
        }
    }
}
