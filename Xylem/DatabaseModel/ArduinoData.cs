﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class ArduinoData
    {
        public long ID { get; set; }
        public int Time { get; set; }
        public int Step { get; set; }
        public float EndTemperature { get; set; }
        public Sensor Sensor1 { get; set; }
        public Sensor Sensor2 { get; set; }
        public SessionRecord CurrentSessionRecord { get; set; }
        public bool Show { get; set; }

        public ArduinoData()
        {
            Show = true;
        }

        public ArduinoData(int time, int step, Sensor sensor1, Sensor sensor2, float endTemperature)
        {
            Time = time;
            Step = step;
            EndTemperature = endTemperature;
            Sensor1 = sensor1;
            Sensor2 = sensor2;
        }
        public ArduinoData(int id, int time, int step, Sensor sensor1, Sensor sensor2, float endTemperature)
        {
            ID = id;
            Time = time;
            Step = step;
            EndTemperature = endTemperature;
            Sensor1 = sensor1;
            Sensor2 = sensor2;
        }

        public ArduinoData(int time, int step, int pressure1, int pressure2, float temperature1, float temperature2,
            float endTemperature)
        {
            Time = time;
            Step = step;
            EndTemperature = endTemperature;
            Sensor1 = new Sensor(pressure1, temperature1);
            Sensor2 = new Sensor(pressure2, temperature2);
        }
    }
}
