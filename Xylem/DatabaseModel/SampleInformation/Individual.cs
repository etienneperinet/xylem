﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class Individual
    {
        public string Code { get; set; }
        public Experience Experience { get; set; }
        public Individual()
        {
           
        }

        public Individual(string code, Experience experience)
        {
            Code = code;
            Experience = experience;
        }
        public Individual(string code)
        {
            Code = code;
        }
    }
}
