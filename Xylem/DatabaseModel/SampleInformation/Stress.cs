﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class Stress
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public Stress()
        {
            ID = -1;
            Name = "";
        }
        
        public Stress(int id, string name)
        {
            ID = id;
            Name = name;
        }
        
        public Stress(int id)
        {
            ID = id;
        }
        
        public Stress( string name)
        {
            Name = name;
        }
    }
}
