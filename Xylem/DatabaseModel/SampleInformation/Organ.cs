﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class Organ
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Organ()
        {
            ID = -1;
            Name = "";
        }

        public Organ(int id)
        {
            ID = id;
        }

        public Organ(string name)
        {
            Name = name;
        }

        public Organ(int id, string name)
        {
            ID = id;
            Name = name;
        }
    }
}
