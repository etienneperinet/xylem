﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class Computer
    {
        public int ID { set; get; }
        public bool Standardized { set; get; }

        public Computer(int id, bool standardized)
        {
            ID = id;
            Standardized = standardized;
        }
        public Computer()
        {
            ID = 0;
            Standardized = false;
        }

        public Computer(int id, int standardized)
        {
            ID = id;
            if (standardized == 0)
            {
                Standardized = false;
            }
            else
            {
                Standardized = true;
            }
        }

        public int GetStandardizedID()
        {
            int id;
            if (Standardized)
            {
                id = 1;
            }
            else
            {
                id = 0;
            }
            return id;
        }

        public int SetStanderdizedWithID(int id)
        {
            if (id == 0)
            {
                Standardized = false;
            }
            else
            {
                Standardized = true;
            }
            return id;
        }
    }
}
