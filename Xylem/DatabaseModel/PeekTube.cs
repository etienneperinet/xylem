﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class PeekTube
    {
        public string Code { get; set; }
        public string Color { get; set; }
        public float R_PeekTube_25 { get; set; }
        public int Year { get; }
        public int Month { get; }
        public int Day { get; }
        public bool Actif { get; set; }
        public string OperatorCreatorCode { get; set; }

        public PeekTube()
        {

        }
        public PeekTube(string code, string color, float r_PeekTube_25, string employeCode, int year, int month, int day, bool actif)
        {
            Code = code;
            Color = color;
            R_PeekTube_25 = r_PeekTube_25;
            Year = year;
            Month = month;
            Day = day;
            Actif = actif;
            OperatorCreatorCode = employeCode;
        }
        
        public PeekTube(string code, string color, float r_PeekTube_25, string employeCode)
        {
            Code = code;
            Color = color;
            R_PeekTube_25 = r_PeekTube_25;
            DateTime localDate = DateTime.Now;
            Year = localDate.Year;
            Month = localDate.Month;
            Day = localDate.Day;
            Actif = true;
            OperatorCreatorCode = employeCode;
        }
        public override string ToString()
        {
            return Code;
        }
    }
}
