﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class Operator
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string EmployeeCode { set; get; }

        public Operator(string firstName, string lastName, string employeeCode)
        {
            FirstName = firstName;
            LastName = lastName; 
            EmployeeCode = employeeCode; 
        }
    }
}
