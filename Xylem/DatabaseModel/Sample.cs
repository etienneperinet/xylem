﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class Sample
    {
        //Need to figure out if it is us that de
        public int ID { get; set; }
        public string Code { get; set; }
        public Stress Stress { get; set; }
        public Organ Organ { get; set; }
        public Individual Individual { get; set; }
        public string PhysiologicalAge { get; set; }
        public string Stage { get; set; }
        public float WaterPotential { get; set; }

        public Sample()
        {

        }

        public Sample(int id, string code, Stress stress, Organ organ, Individual individual,
            string physiologicalAge, string stage, float waterPotential)
        {
            ID = id;
            Code = code.ToUpper();
            Stress = stress;
            Organ = organ;
            Stage = stage;
            Individual = individual;
            PhysiologicalAge = physiologicalAge;
            WaterPotential = waterPotential;
        } 
        
        public Sample(string code, Stress stress, Organ organ, Individual individual,
            string physiologicalAge, string stage, float waterPotential)
        {
            ID = -1;
            Code = code.ToUpper();
            Stress = stress;
            Organ = organ;
            Individual = individual;
            PhysiologicalAge = physiologicalAge;
            Stage = stage;
            WaterPotential = waterPotential;
        }
    }
}
