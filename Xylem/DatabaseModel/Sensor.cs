﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class Sensor
    {
        public int ID { get; set; }
        public int Pressure { get; set; }
        public float Temperature { get; set; }

        public Sensor()
        {

        }

        public Sensor(int pressure, float temperature)
        {
            Pressure = pressure;
            Temperature = temperature;
        }
        public Sensor(int id, int pressure, int temperature)
        {
            ID = id;
            Pressure = pressure;
            Temperature = temperature;
        }
    }
}
