﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xylem
{
    public class Experience
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public Experience()
        {

        }

        public Experience(int id, string name)
        {
            ID = id;
            Name = name;
        }
        public Experience(int id)
        {
            ID = id;
        }

        public Experience(string name)
        {
            ID = 0;
            Name = name;
        }
    }
}
