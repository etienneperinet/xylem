﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;
using System.Globalization;
using System.Management;
using System.ComponentModel;

namespace Xylem
{
    public static class SerialPortCommunicator
    {
        
        private static SerialPort serialPort = new SerialPort();

        public static SerialPort SerialPort
        {
            get { return serialPort; }
            set { serialPort = value; }
        }

        public static void Initialise()
        {
            UseRightPort();
            SerialPort.RtsEnable = true;
            SerialPort.DtrEnable = true;
        }
        private static void UseRightPort()
        {
            if (!ArduinoPortFound())
            {
                MessageBox.Show("Le port série du montage Xylem n'a pas été trouvé.");
            }
        }

        private static bool ArduinoPortFound()
        {
            bool arduinoFound = false;
            ManagementScope connectionScope = new ManagementScope();
            SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);
            try
            {
                foreach (ManagementObject item in searcher.Get())
                {
                    string desc = item["Description"].ToString();
                    string deviceId = item["DeviceID"].ToString();
                    if (desc.Contains("Périphérique série USB") || desc.Contains("USB Serial Device"))
                    {
                        arduinoFound = true;
                        serialPort.PortName = deviceId;
                    }
                }
            }
            catch (ManagementException e)
            {
                /*Do Nothing*/
            }
            return arduinoFound;
        }
    }
    /*class CostumSerialPortClass : SerialPort
    {
        private static CostumSerialPortClass instance;
        public event EventHandler<ArduinoDataEventArgs> ArduinoDataReceptionEvent;


        private CostumSerialPortClass(IContainer container) : base(container)
        {
            PortName = "COM3";
            RtsEnable = true;
            DtrEnable = true;
            DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(EventDataReceived);
            UseRightPort();
        }

        public static CostumSerialPortClass createInstance(IContainer container)
        {
            if (instance == null)
            {
                instance = new CostumSerialPortClass(container);
            }
            else
            {
                instance = new CostumSerialPortClass(container);
            }

            return instance;
        }
        public static CostumSerialPortClass getInstance()
        {
            return instance;
        }

        private void UseRightPort()
        {
            if (!ArduinoPortFound())
            {
                MessageBox.Show("Le port série du montage Xylem n'a pas été trouvé.");
            }
        }

        private bool ArduinoPortFound()
        {
            bool arduinoFound = false;
            ManagementScope connectionScope = new ManagementScope();
            SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);
            try
            {
                foreach (ManagementObject item in searcher.Get())
                {
                    string desc = item["Description"].ToString();
                    string deviceId = item["DeviceID"].ToString();
                    if (desc.Contains("Périphérique série USB") || desc.Contains("USB Serial Device"))
                    {
                        arduinoFound = true;
                        PortName = deviceId;
                    }
                }
            }
            catch (ManagementException e)
            {
                 Do Nothing 
            }
            return arduinoFound;
        }

        private void EventDataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string dataReceived = ReadLine();
            dataReceived = dataReceived.Replace("\r", "");
            string[] data = dataReceived.Split('\t');
            try
            {
                if (data.Length >= 7)
                {
                    ArduinoData arduinoData = new ArduinoData();
                    arduinoData.Time = Int32.Parse(data[0]);
                    arduinoData.Step = Int32.Parse(data[1]);

                    Sensor sensor1 = new Sensor();
                    sensor1.Pressure = Int32.Parse(data[2]);
                    sensor1.Temperature = float.Parse(data[4], CultureInfo.InvariantCulture);
                    arduinoData.Sensor1 = sensor1;

                    Sensor sensor2 = new Sensor();
                    sensor2.Pressure = Int32.Parse(data[3]);
                    sensor2.Temperature = float.Parse(data[5], CultureInfo.InvariantCulture);
                    arduinoData.Sensor2 = sensor2;

                    arduinoData.EndTemperature = float.Parse(data[6], CultureInfo.InvariantCulture);

                    SendDataToForm(arduinoData);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Une donnée provenant du Arduino n'est pas lisible par le programme.");
            }
        }

        private void SendDataToForm(ArduinoData arduinoData)
        {
            if (ArduinoDataReceptionEvent != null)
            {
                ArduinoDataReceptionEvent(this, new ArduinoDataEventArgs(arduinoData));
            }
        }
    }
    class ArduinoDataEventArgs : EventArgs
    {
        public ArduinoData ArduinoData { get; }
        public ArduinoDataEventArgs(ArduinoData arduinoData)
        {
            ArduinoData = arduinoData;
        }
    }*/
}