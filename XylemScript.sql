CREATE TABLE "operator"(
	"employee_code" TEXT,
	"first_name" TEXT NULL,
	"last_name" TEXT NULL,
	
	PRIMARY KEY ("employee_code")
);

CREATE TABLE "arduino"(
	"id" INTEGER,
	PRIMARY KEY ("id")
);

CREATE TABLE "computer_type"(
	"id" INTEGER,
	"type" TEXT NOT NULL,
	PRIMARY KEY ("id" AUTOINCREMENT)
);


CREATE TABLE "computer"(
	"id" INTEGER,
	"computer_type_id" INTEGER NOT NULL,
	FOREIGN KEY ("computer_type_id")  REFERENCES "computer_type"("id"),
	PRIMARY KEY ("id")
);

CREATE TABLE "session"(
	"id" INTEGER,
	"computer_id" INTEGER NOT NULL,
	"arduino_id" INTEGER NOT NULL,
	"employee_code" INTEGER NOT NULL,
	/*"session_type_id" INTEGER NOT NULL,*/
	"year" INTEGER NOT NULL,
	"month" INTEGER NOT NULL,
	"day" INTEGER NOT NULL,
	FOREIGN KEY ("computer_id")  REFERENCES "computer"("id"),
	FOREIGN KEY ("arduino_id")  REFERENCES "arduino"("id"),
	/*FOREIGN KEY ("session_type_id")  REFERENCES "session_type"("id"),*/
	FOREIGN KEY ("employee_code")  REFERENCES "operator"("employee_code"),
	PRIMARY KEY ("id" AUTOINCREMENT)
);


CREATE TABLE "arduino_data"(
	"id" INTEGER,
	"time" INTEGER NOT NULL,
	"step" INTEGER NOT NULL,
	"end_temperature" REAL NOT NULL,
	"session_id" INTEGER, 
	"sensor1_id" INTEGER NOT NULL,
	"sensor2_id" INTEGER NOT NULL,
	FOREIGN KEY ("session_id") REFERENCES "session"("id"),
	FOREIGN KEY ("sensor1_id") REFERENCES "sensor"("id"),
	FOREIGN KEY ("sensor2_id") REFERENCES "sensor"("id"),
	PRIMARY KEY ("id" AUTOINCREMENT)
);

CREATE TABLE "sensor"(
	"id" INTEGER,
	"pressure" REAL NOT NULL,
	"temperature" REAL NOT NULL,
	PRIMARY KEY ("id" AUTOINCREMENT)
);



			   
INSERT INTO computer_type(id, type)
	VALUES(0 ,'Non standarisé');

INSERT INTO computer_type(id, type)
	VALUES(1 ,'Standarisé par le SAC');


/*
New code : 
*/


CREATE TABLE "experience"(
	"id" INTEGER,
	"name" TEXT NOT NULL,
	
	PRIMARY KEY ("id" AUTOINCREMENT)
);


CREATE TABLE "individual"(
	"code" TEXT,
	"experience_id" TEXT NOT NULL,
	
	FOREIGN KEY ("experience_id") REFERENCES "experience"("id"),
	PRIMARY KEY ("code")
);


CREATE TABLE "organ"(
	"id" INTEGER,
	"name" TEXT NOT NULL,
	
	PRIMARY KEY ("id" AUTOINCREMENT)
);


CREATE TABLE "stress"(
	"id" INTEGER,
	"name" TEXT NOT NULL,
	
	PRIMARY KEY ("id" AUTOINCREMENT)
);



CREATE TABLE "sample"(
	"id" INTEGER,
	"code" TEXT NOT NULL, 
	"stage" TEXT NOT NULL,
	"physiological_age" TEXT NOT NULL,
	"water_potential" REAL NOT NULL,
	
	"organ_id" INTEGER NOT NULL,
	"stress_id" INTEGER NOT NULL,
	"individual_code" TEXT NOT NULL,
	
	FOREIGN KEY ("individual_code") REFERENCES "individual"("code"),
	FOREIGN KEY ("stress_id") REFERENCES "stress"("id"),
	FOREIGN KEY ("organ_id") REFERENCES "organ"("id"),
	UNIQUE("code"/*, "individual_code"*/),
	PRIMARY KEY ("id" AUTOINCREMENT)
);



CREATE TABLE "perfusion_solution"(
	"id" INTEGER,
	"name" TEXT NOT NULL,
	
	PRIMARY KEY ("id" AUTOINCREMENT)
);


CREATE TABLE "peektube"(
	"code" TEXT,
	"color" TEXT NOT NULL,
	"r_peektube_25" REAL NOT NULL,
	"year" INTEGER NOT NULL,
	"month" INTEGER NOT NULL,
	"day" INTEGER NOT NULL,
	"actif" INTEGER NOT NULL,
	"employee_code" TEXT NOT NULL,
	FOREIGN KEY ("employee_code") REFERENCES "operator"("employee_code"),
	PRIMARY KEY ("code")
);

CREATE TABLE "session_record"(
	"id" INTEGER,
	"hour" INTEGER NOT NULL,
	"minute" INTEGER NOT NULL,
	"session_id" REAL NOT NULL,
	FOREIGN KEY ("session_id") REFERENCES "session"("id"),
	PRIMARY KEY ("id" AUTOINCREMENT)
);

CREATE TABLE "measurement_result"(
	"id" INTEGER,
	"water_height" INTEGER NOT NULL,
	"perfusion_solution_id" TEXT NOT NULL,
	"valid_peektube" INTEGER NOT NULL,
	"sufficient_time" INTEGER NOT NULL,
	"stable" INTEGER NOT NULL,
	"valid_sensor_pressure" INTEGER NOT NULL,
	"p1" REAL NOT NULL,
	"p2" REAL NOT NULL,
	"mmol_k_rough" REAL NOT NULL,
	"kg_k_rough" REAL NOT NULL,
	"cv_k" REAL NOT NULL,
	"p3" REAL NOT NULL,
	"t1" REAL NOT NULL,
	"t_av" REAL NOT NULL,
	"r_peektube_t" REAL NOT NULL,
	"t3" REAL NOT NULL,
	"k_t" REAL NOT NULL,
	"k_25" REAL NOT NULL,
	"kh" REAL NOT NULL,
	"ks" REAL NOT NULL,
	"hv" REAL NOT NULL,
	"kl" REAL NOT NULL,
	"gamme_peektube" REAL NOT NULL,
	
	"peektube_code" TEXT NOT NULL,
	"session_record_id" INTEGER NOT NULL,
	"note" TEXT NOT NULL,
	
	"diameter1" REAL NOT NULL,
	"diameter2" REAL NOT NULL,
	"length" REAL NOT NULL,
	
	FOREIGN KEY ("perfusion_solution_id") REFERENCES "perfusion_solution"("id"),
	FOREIGN KEY ("session_record_id") REFERENCES "session_record"("id"),
	FOREIGN KEY ("peektube_code") REFERENCES "peektube"("code"),
	PRIMARY KEY ("id" AUTOINCREMENT)
);

CREATE TABLE "calibration_result"(
	"id" INTEGER,
	"slope1" REAL NOT NULL,
	"slope2" REAL NOT NULL,
	"intercept1" REAL NOT NULL,
	"intercept2" REAL NOT NULL,
	"pearson1" REAL NOT NULL,
	"pearson2" REAL NOT NULL,
	"session_record_id" INTEGER NOT NULL,
	"note" TEXT NOT NULL,
	
	FOREIGN KEY ("session_record_id") REFERENCES "session_record"("id"),
	PRIMARY KEY ("id" AUTOINCREMENT)
);

/*
CREATE TABLE "ki_kmax_type"(
	"id" INTEGER,
	"type" TEXT NOT NULL,
	
	PRIMARY KEY ("id" AUTOINCREMENT)
);

INSERT INTO ki_kmax_type(id, type)
	VALUES(1 ,'KI');
INSERT INTO ki_kmax_type(id, type)
	VALUES(2 ,'KMAX');
*/

CREATE TABLE "kmax_method"(
	"id" INTEGER,
	"name" TEXT NOT NULL,
	
	PRIMARY KEY ("id" AUTOINCREMENT)
);


CREATE TABLE "kmax"(
	"id" INTEGER,
	
	"kmax_duration_min" INTEGER NOT NULL,
	"kmax_iteration" INTEGER NOT NULL,
	"kmax_pression" REAL NOT NULL,
	
	"kmax_method_id" INTEGER NOT NULL,
	FOREIGN KEY ("kmax_method_id") REFERENCES "kmax_method"("id"),
	PRIMARY KEY ("id" AUTOINCREMENT)
);

CREATE TABLE "ki_kmax"(
	"id" INTEGER,
	"kmax_id" INTEGER,
	"measurement_result_id" INTEGER NOT NULL,
	"sample_id" INTEGER NOT NULL,
	
	
	FOREIGN KEY ("kmax_id") REFERENCES "kmax"("id"),
	FOREIGN KEY ("measurement_result_id") REFERENCES "measurement_result"("id"),
	FOREIGN KEY ("sample_id") REFERENCES "sample"("id"),
	/*
	"ki_kmax_type_id" INTEGER NOT NULL,
	FOREIGN KEY ("ki_kmax_type_id") REFERENCES "ki_kmax_type"("id"),
	*/
	
	PRIMARY KEY ("id" AUTOINCREMENT)
);




